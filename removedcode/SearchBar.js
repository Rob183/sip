import React from "react";
import "./Search.css";
import SearchIcon from "@material-ui/icons/Search";

class SearchBar extends React.Component {
  constructor(props) {
    super(props);
    this.state = {
      query: "",
      results: {},
      loading: false,
      message: "",
    };
  }
  render() {
    return (
      <div className="container">
        {/*Search Input*/}
        <label className="search-label" htmlFor="search-input">
          <input type="text" id="search-input" placeholder="search..." />
          <SearchIcon></SearchIcon>
        </label>
      </div>
    );
  }
}
export default SearchBar;
