#imported Libraries

from bs4 import BeautifulSoup
import requests
import json
import os
import signal
import sys
import urllib.parse
import re
import urllib
from urlmatch import urlmatch
from requests.exceptions import MissingSchema
from urllib.parse import *

#get first and last name of the professor.
vor_name = input("Vorname: ")
fam_name = input("Name: ")

#capitalize the first letters
prof=fam_name.capitalize()+', '+vor_name.capitalize()
vorcap = vor_name.capitalize()
famcap = fam_name.capitalize()

first_letter = prof[0]
 
#Get and set the URL
#get first letter from name and insert between buchstabe and question mark

url = 'https://www.htwk-leipzig.de/hochschule/kontakt/htwk-telefonverzeichnis/buchstabe/'+first_letter+'/'

parsed = urlsplit(url)

page = requests.get(url).text
soup = BeautifulSoup(page, "lxml")

url_parsed=parsed.scheme+"://"+parsed.netloc+parsed.path+parsed.query+parsed.fragment
url = url_parsed

#Get name from telephone directory in specific letter section
find_string = soup.body.findAll(text=re.compile(prof), limit=1)

find_vorname = soup.body.findAll(text=re.compile(vorcap), limit=1)

find_famname = soup.body.findAll(text=re.compile(famcap), limit=1)


for findev in find_string:
    findev = findev.strip()
    
    #convert list to string
    findstring = ' '.join(map(str, findev.split(',')))
   
    string1 = findstring.split(' ', 1)[0]
    string1 = string1.strip()
    string2 = findstring.split(' ', 2)[2]
    string2 = string2.strip()
       
    findstringstrip = findstring.strip(',')
      
    findev = findev.split(',')
               
    if string1 != famcap:
        print("Nachname nicht gefunden.")
        sys.exit()
    else:
        print("Nachname gefunden")
  
    if string2 != vorcap:
        print("Vorname nicht gefunden")
        sys.exit()
    else:
        print("Vorname gefunden")
      
for strings in find_string:
    strings = strings.strip()
    
    #Get all Links in "a href" from given name
    result_link = soup.findAll('a', href=True)
    result_link = list(filter(lambda x: x.text.strip() == prof, result_link))
    try:
        res = result_link[0]['href']
        parsed = urlsplit(res)
                    
        if not parsed.scheme and not parsed.netloc:
            result_link[0]['href'] = 'https://www.htwk-leipzig.de' + result_link[0]['href']
    except:
        result_link[0]['href'] = 'https://www.htwk-leipzig.de' + result_link[0]['href']
        
    for link in result_link:
        res_link = link['href']
        page = requests.get(res_link).text
        soup = BeautifulSoup(page, "lxml")
        result_link_sub = soup.findAll(href=True, text=prof)
                                 
        for link_sub in result_link_sub: 
            print("res_link: ", link_sub['href'])

        result_name = soup.findAll('div', attrs={"class" : "name"})
        result_images = soup.find_all('img', {'src':re.compile('.jpg')})
        result_title = soup.findAll('div', attrs={"class" : "name title"})
        result_position = soup.findAll('div', attrs={"class" : "position"})
        result_department = soup.findAll('div', attrs={"class" : "department content"})
        result_telephone = soup.findAll('div', attrs={"class" : "number content"})
        result_email = soup.findAll('div', attrs={"class" : "address content"})
        result_room = soup.findAll('div', attrs={"class" : "object content"})

        #print each result in a result list 
        for name in result_name:
            print('Name:',name.text.strip())

        for title in result_title:
            print('Titel:',title.text.strip())

        for department in result_department:
            print('Departement:',department.text.strip())

        for telephone in result_telephone:
            print('Telefon:',telephone.text.strip())

        for email in result_email:
            print('Email: ',email.text.strip())

        for room in result_room:
            building,build_room = room.text.split('\t', 1)
            building.strip()
            build_room.strip()
 
        for image in result_images: 
            print('Bild:',image['src'])
     
        dictionary = {
            "image": image['src'],
            "name": name.text.strip(),
            "title": title.text.strip(),
           #"department": department.text.strip(),
            "raum": build_room.strip(),
            "telefon": telephone.text.strip(),
          # "Email": email.text.strip()
            }

         # sorting result in asscending order by keys:
        json_object = json.dumps(dictionary, indent=4, ensure_ascii=False)

         #Open File and write object in json file
        try:
            with open ('info.json', 'w') as outfile:
                outfile.write(json_object)
                print("Datensatz erfolgreich in die Datei geschrieben.")
        except IOError:
            print("Datensatz nicht erfolgreich geschrieben.")

