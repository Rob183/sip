const fs = require("fs");
const readline = require("readline");

const view = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

view.question(
  "Bitte den Mapbox Access Token eingeben. Anleitung in Abschnitt 3.2.5 des Benutzerhandbuches:  ",
  (AccessToken) => {
    let Token = AccessToken;
    fs.writeFile(
      "./src/components/Karte/Token.js",
      'export const Token = "' + Token + '";\n',
      function (error) {
        if (error) {
          return console.log(
            "Setzen des Access Tokens Fehlgeschlagen: " + error
          );
        }
        console.log("Access Token successfully changed.");
      }
    );
    view.close();
  }
);
