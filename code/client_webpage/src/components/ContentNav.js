import React, { Component } from "react";
import { Router } from "@reach/router";
import { Link } from "@reach/router";
import Button from "@material-ui/core/Button";
import getContentMarkdown from "../utilities/contentResolver.js";
import Loader from "./MarkdownLoader";
import Karte from "../components/Karte/karte.js";

function pathFind(path = "", obj) {
  path = path.split("/").map((i) => decodeURIComponent(i));

  let content = obj;
  path.forEach((i) => {
    const index = content.findIndex((el) => el.name === i);
    content = index === -1 ? content : content[index];

    if (content.hasOwnProperty("children")) {
      content = content.children;
    } else {
    }
  });
  return content;
}

const ContentView = (props) => {
  const text = pathFind(props["*"], props.entries).text;

  //Wenn sich der Text als JSON Parsen lässt
  try {
    JSON.parse(text);
    return <Karte data={text} />;
  } catch (err) {
    return <Loader text={text} />;
  }
};

const NavList = (props) => {
  const contentList = pathFind(props["*"], props.entries);
  const linkPrefix = props["*"] !== "" ? props["*"] + "/" : "";

  const pathList = props["*"].split("/");
  const pathTitle = pathList[pathList.length - 1];

  return (
    <>
      <h1 style={{ padding: "0.5em" }}>{pathTitle || "Informationen"}</h1>
      <div className="AppButton">
        {contentList.map((el, i) => {
          if (el.hasOwnProperty("children")) {
            // Untermenü
            return (
              <Link to={linkPrefix + encodeURIComponent(el.name)} key={i}>
                <Button variant="contained" color="primary">
                  {el.name}
                </Button>
              </Link>
            );
          } else {
            // Artikel
            return (
              <Link
                to={"artikel/" + linkPrefix + encodeURIComponent(el.name)}
                key={i}
              >
                <Button variant="contained" color="primary">
                  {el.titel}
                </Button>
              </Link>
            );
          }
        })}
      </div>
    </>
  );
};

class ContentNav extends Component {
  state = {
    loaded: false,
  };

  content = {};

  async componentWillMount() {
    // Load content and cd to root path
    this.content = (await getContentMarkdown())[0].children;
    this.setState({
      loaded: true,
    });
  }

  render() {
    if (this.state.loaded) {
      return (
        <Router>
          <ContentView path="/artikel/*" entries={this.content} />
          <NavList path="/*" entries={this.content} />
        </Router>
      );
    } else {
      return <p>Lädt...</p>;
    }
  }
}

export default ContentNav;
