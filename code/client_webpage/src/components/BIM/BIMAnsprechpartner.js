import React, { Component } from "react";
import jsonAnsprechpartner from "./BIMAnsprechpartner.json";
import digiTransSachs from "./digiTransSachs.json";
import BIMAnsprechpartnerBox from "./BIMAnsprechpartnerBox";
import "./BIMAnsprechpartner.css";

class BIMAnsprechpartner extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  render() {
    return (
      <div>
        <h2>BIM Ansprechpartner</h2>
        <div className="siteWrapper">
          {jsonAnsprechpartner.map((item, i) => {
            return <BIMAnsprechpartnerBox key={i} data={item} />;
          })}
        </div>
        <h2>DigiTransSachs Ansprechpartner</h2>
        <div className="siteWrapper">
          {digiTransSachs.map((item, i) => {
            return <BIMAnsprechpartnerBox key={i} data={item} />;
          })}
        </div>
      </div>
    );
  }
}
export default BIMAnsprechpartner;
