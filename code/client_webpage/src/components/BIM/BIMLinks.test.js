import React from "react";
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import BIMLinks from "./BIMLinks.js";

describe("Dashboard component", () => {
  it("Find Element", () => {
    const wrapper = shallow(<BIMLinks />);
    const el = wrapper.find("QRCode");
    expect(el).toBeDefined();
  });

  it("renders without crashing", () => {
    shallow(<BIMLinks />);
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMLinks />);
    const el = wrapper.find("h1").text();
    const text = "BIM-Links";
    expect(el).toEqual(text);
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMLinks />);
    const el = wrapper.find("p").at(0).text();
    const text =
      "Das BIM ist ein sehr großer Themenbereich, der immer mehr an Bedeutung" +
      " gewinnt. Hier findest du Links zu wichtigen Infos, die dir helfen dich" +
      " zurecht zu finden. Von offiziellen Leitfäden vom Bundesministerium für" +
      " Verkehr und digitale Infrastruktur, über die Architektenkammer Sachsen" +
      " und ihre Projekte zum BIM, bis zu nützlichen Webseiten die dir das" +
      " Einfinden und Einarbeiten erleichtern.";
    expect(el).toEqual(text);
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMLinks />);
    const el = wrapper.find("p").at(1).text();

    expect(el.length).not.toEqual(0);
    console.log(el.length);
  });
});
