import React, { Component } from "react";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel
} from "react-accessible-accordion";
import BIMStatischeInfo from "./BIMStatischeInfo";
import "./BIMStudium.css";

class BIMStudium extends Component {
  dateien = [
    {
      title: "MA 8700 [AR]",
      pageid: "MA8700"
    },
    {
      title: "BA 2200 [AR]",
      pageid: "BA2200"
    },
    {
      title: "MA 3240 [BI]",
      pageid: "MA3240"
    },
    {
      title: "MA 9020 [FING]",
      pageid: "MA9020"
    }
  ];
  render() {
    return (
      <>
        <div>
          <h2>Lehrveranstaltungen</h2>
          <Accordion className="accordion" allowZeroExpanded="true">
            {this.dateien.map((item, i) => {
              return (
                <AccordionItem>
                  <AccordionItemHeading>
                    <AccordionItemButton className="heading">
                      {item.title}
                    </AccordionItemButton>
                  </AccordionItemHeading>
                  <AccordionItemPanel className="text">
                    <BIMStatischeInfo pageid={item.pageid} />
                  </AccordionItemPanel>
                </AccordionItem>
              );
            })}
          </Accordion>
          <h2>Begriffe</h2>
          <div id="begriffe">
            <BIMStatischeInfo pageid="Begriffe" />
          </div>
        </div>
      </>
    );
  }
}

export default BIMStudium;
