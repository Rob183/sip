import React, { Component } from "react";
import { Link } from "@reach/router";
import "./BIMDashboard.css";

class BIMInformationenWidget extends Component {
  /*Richtige Seiten verlinken*/
  apps = [
    {
      name: "Curriculum",
      link: "artikel/Curriculum",
      preview:
        "Wie ist BIM aufgebaut? Was gibt es zu wissen? Hier findest du einen Überblick über das Thema, der dir hilft dich zu orientieren."
    },
    {
      name: "BIM-Werkzeuge",
      link: "artikel/Werkzeuge",
      preview:
        "Du willst nichtnur lesen, sondern auch selber mit BIM arbeiten? Hier findest du Werkzeuge die du nutzen kannst."
    },
    {
      name: "Zertifizierungen",
      link: "artikel/Zertifizierungen",
      preview:
        "Du möchtest dein Wissen über BIM offiziell machen? Hier findest du Infos zum Erwerb einer offiziellen Qualifikation."
    },
    {
      name: "Ansprechpartner",
      link: "Ansprechpartner",
      preview:
        "Fragen zu BIM? Hier finden sich Experten, die deine Fragen beantworten können. Schreib einfach deine Fragen oder ruf an."
    },
    {
      name: "Studium",
      link: "Studium",
      preview:
        "Wo man BIM nutzt? Hier findest du Module in denen du alles über BIM erfahren wirst und es praktisch nutzt."
    },
    {
      name: "Abschlussarbeiten",
      link: "wissenswertes/Abschlussarbeiten",
      preview:
        "Du interessierst dich für BIM und möchtest deine Abschlussarbeit darüber schreiben? Hier kannst du lesen was andere zu BIM erarbeitet haben."
    },
    {
      name: "Forschung",
      link: "wissenswertes/Forschung",
      preview:
        "BIM ist kein abgeschlossenes System. Hier kannst du nachlesen welche Projekte es gibt um BIM weiterzuentwickeln."
    },
    {
      name: "Links",
      link: "Link-Liste",
      preview:
        "Du möchtest noch mehr über BIM erfahren? Zück dein Smartphone und durchstöber diese Sammlung zu dem Thema."
    }
  ];

  render() {
    return (
      <>
        <div className="widgetkapselung2">
          <div className="widgetheadline">Informationen zu BIM</div>
          <div className="Scrollkapselung">
            <div className="widget">
              {//Erzeugung der Buttons mit den Eigenschaften aus vorherigem Array
              this.apps.map((item, i) => {
                if (!item.disabled) {
                  //Abfrage zur Sichtbarkeit
                  return (
                    <Link
                      to={item.link}
                      key={i}
                      style={{ textDecoration: "none" }}
                    >
                      <div className="BIMWidgetItem">
                        <div className="BIMWidgetItemtitle">{item.name}</div>
                        <div className="BIMWidgetItempreview">
                          {item.preview}
                        </div>
                      </div>
                    </Link>
                  );
                } else {
                  return null;
                }
              })}
            </div>
          </div>
        </div>
        <div className="fade" />
      </>
    );
  }
}
export default BIMInformationenWidget;
