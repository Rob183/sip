import React, { Component } from "react";
import {
  Accordion,
  AccordionItem,
  AccordionItemHeading,
  AccordionItemButton,
  AccordionItemPanel
} from "react-accessible-accordion";
import QRCode from "qrcode-react";

import "../../components/text.css";
import jsonLinks from "./links.json";
import "./BIMStudium.css";
import "../../components/text.css";

class BIMLinks extends Component {
  //von Richard: Ueberschrift BIM Links auf h2 statt h1 damit einheitlich mit md Dateien und Einrueckung dann funktioniert
  //Inhalt der json Datei wird dann durch text.css formatiert
  render() {
    return (
      <div>
        <h1 className="BIMHeadline h1margin">BIM-Links</h1>
        <p>
          Das BIM ist ein sehr großer Themenbereich, der immer mehr an Bedeutung
          gewinnt. Hier findest du Links zu wichtigen Infos, die dir helfen dich
          zurecht zu finden. Von offiziellen Leitfäden vom Bundesministerium für
          Verkehr und digitale Infrastruktur, über die Architektenkammer Sachsen
          und ihre Projekte zum BIM, bis zu nützlichen Webseiten die dir das
          Einfinden und Einarbeiten erleichtern.
        </p>
        <Accordion className="">
          {jsonLinks.map((sections, i) => {
            return (
              <AccordionItem key={i}>
                <AccordionItemHeading>
                  <AccordionItemButton className="heading">
                    {sections.thema}
                  </AccordionItemButton>
                </AccordionItemHeading>
                <div className="text">
                  <AccordionItemPanel>
                    <table className="tabTable">
                      {" "}
                      {/*Klasse in stundenplan.css*/}
                      <tbody>
                        {sections.links.map((links, index) => {
                          return (
                            <tr key={index}>
                              <td style={{ width: "100%" }}>
                                <p>{links.titel}</p>
                              </td>
                              <td>
                                <QRCode value={links.url} />
                              </td>
                            </tr>
                          );
                        })}
                      </tbody>
                    </table>
                  </AccordionItemPanel>
                </div>
              </AccordionItem>
            );
          })}
        </Accordion>
      </div>
    );
  }
}

export default BIMLinks;
