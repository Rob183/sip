import React from "react";
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import BIMInformationenWidget from "./BIMInformationenWidget.js";

describe("Dashboard component", () => {
  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    const el = wrapper.find("Link");
    expect(el).toBeDefined();
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    expect(wrapper.find("Link").at(0).props().to).toBe("artikel/Curriculum");
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    expect(wrapper.find("Link").at(1).props().to).toBe("artikel/Werkzeuge");
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    expect(wrapper.find("Link").at(2).props().to).toBe(
      "artikel/Zertifizierungen"
    );
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    expect(wrapper.find("Link").at(3).props().to).toBe("Ansprechpartner");
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    expect(wrapper.find("Link").at(4).props().to).toBe("Studium");
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    expect(wrapper.find("Link").at(5).props().to).toBe(
      "wissenswertes/Abschlussarbeiten"
    );
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    expect(wrapper.find("Link").at(6).props().to).toBe(
      "wissenswertes/Forschung"
    );
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMInformationenWidget />);
    expect(wrapper.find("Link").at(7).props().to).toBe("Link-Liste");
  });
});
