import React, { Component } from "react";
import "./BIMAnsprechpartner.css";
import QRCode from "qrcode-react";

class BIMAnsprechpartnerBox extends Component {
  render() {
    return (
      <div className="box">
        <div className="bildInfo">
          <div>
            <img src={this.props.data.image} alt="Ladefehler" />
          </div>
          <div className="infoWrapper">
            <QRCode value={this.props.data.link} />
          </div>
        </div>
        <div className="contactWrapper">
          <div>{this.props.data.title}</div>
          <div id="name">{this.props.data.name}</div>
          <div>{this.props.data.bereich}</div>
          <div>{this.props.data.raum}</div>
          <div>{this.props.data.telefon}</div>
          <div>{this.props.data.email}</div>
        </div>
      </div>
    );
  }
}
export default BIMAnsprechpartnerBox;
