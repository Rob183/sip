import React from "react";
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import BIMStudium from "./BIMStudium.js";

describe("Dashboard component", () => {
  it("Find Element", () => {
    const wrapper = shallow(<BIMStudium />);
    const el = wrapper.findWhere((x) => x.text === "MA 8700 [AR]").first();
    expect(el).toBeDefined();
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMStudium />);
    const el = wrapper.findWhere((x) => x.text === "BA 2200 [AR]").first();
    expect(el).toBeDefined();
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMStudium />);
    const el = wrapper.findWhere((x) => x.text === "MA 3240 [BI]").first();
    expect(el).toBeDefined();
  });

  it("Find Element", () => {
    const wrapper = shallow(<BIMStudium />);
    const el = wrapper.findWhere((x) => x.text === "MA 9020 [FING]").first();
    expect(el).toBeDefined();
  });
});
