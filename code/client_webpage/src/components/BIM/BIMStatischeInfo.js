import React, { useEffect, useState } from "react";
import Loader from "../MarkdownLoader";

export default function BIMStatischeInfo(props) {
  const [text, setText] = useState("");
  console.log(props);

  useEffect(() => {
    async function getText() {
      setText(
        await (await fetch("/bim-content/" + props.pageid + ".md")).text()
      );
      //Pfad in fetch muss so angepasst werden, das es als markdown datei erkannt wird und in den ordner artikel gelangt
    }
    getText();
  }, []);

  return <Loader text={text} />;
}
