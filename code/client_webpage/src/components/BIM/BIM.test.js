import React from "react";
import BIM from "./BIM.js";
import renderer from "react-test-renderer";

describe("Router test", () => {
  it("matches the snapshot", () => {
    const tree = renderer.create(<BIM />).toJSON();
    expect(tree).toMatchSnapshot();
  });
});
