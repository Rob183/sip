import React from "react";
import renderer from "react-test-renderer";
import { shallow } from "enzyme";
import BIMDashboard from "./BIMDashboard.js";

describe("Dashboard component", () => {
  it("heading test", () => {
    const wrapper = shallow(<BIMDashboard />);
    const text = wrapper.find("h1").text();
    expect(text).toEqual("Building Information Modelling (BIM)");
  });

  it("text test", () => {
    const wrapper = shallow(<BIMDashboard />);
    const testText =
      "Digitalisierung, ein Schlagwort das heute überall zu finden ist." +
      " Auch im Bau hinterlässt die digitale Welt ihre Spuren und als" +
      " moderne Hochschule sieht sich die HTWK als Vorreiter für neue" +
      " Technologien und Methoden. Mit dem Building Information Moddeling" +
      " geht die Branche mit der Zeit und revolutioniert dabei den" +
      " Arbeitsalltag.";
    const text = wrapper.find("p").first().text();
    expect(text).toEqual(testText);
  });

  it("text test", () => {
    const wrapper = shallow(<BIMDashboard />);
    const testText =
      "Kooperieren bei Planung und Bau, Komprimieren und Konzentrieren" +
      " aller Informationen – Das BIM ermöglicht es diese für jedes" +
      " Projekt entscheidenden Aspekte zu verfolgen. Alle relevanten" +
      " Informationen, von Architekturentscheidungen über Kosten bis zu" +
      " Schaltplänen, können an einem Ort gebündelt und von allen" +
      " Teammitgliedern genutzt werden, vorbei sind die Zeiten in denen" +
      " umständlich Akten gewälztes werden mussten, mit dem BIM kann" +
      " übersichtlich, effizient und schnell gearbeitet werden.";
    const text = wrapper.find("p").at(1).text();
    expect(text).toEqual(testText);
  });

  it("text test", () => {
    const wrapper = shallow(<BIMDashboard />);
    const text = wrapper.find("p").at(2).text();
    expect(text).toBe("");
  });
});
