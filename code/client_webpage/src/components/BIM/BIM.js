/* Router für BIM*/
/*sollte ähnlich ContentNav sein*/

import React, { Component } from "react";
import { Router } from "@reach/router";

import BIMDashboard from "./BIMDashboard.js";
import BIMAnsprechpartner from "./BIMAnsprechpartner.js";
import BIMLinks from "./BIMLinks.js";
import BIMStatischeInfo from "./BIMStatischeInfo.js";
import BIMStudium from "./BIMStudium.js";
import BIMTabellenSeite from "./BIMTabellenSeite.js";

class BIM extends Component {
  render() {
    return (
      <Router>
        <BIMDashboard path="/" />
        <BIMAnsprechpartner path="Ansprechpartner/" />
        <BIMLinks path="Link-Liste/" />
        <BIMStatischeInfo path="artikel/:pageid" />
        <BIMStudium path="Studium/" />
        <BIMTabellenSeite path="wissenswertes/:pageid" />
      </Router>
    );
  }
}

export default BIM;
