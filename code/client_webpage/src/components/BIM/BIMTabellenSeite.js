import React, { Component } from "react";
import jsonAbschlussarbeiten from "./bimabschlussarbeiten.json";
import jsonForschung from "./bimforschung.json";
import "../../components/text.css";
import "./BIMTabellenSeite.css";

class BIMTabellenSeite extends Component {
  constructor(props) {
    super(props);
    this.state = { data: [] };
  }

  componentDidMount() {
    switch (this.props.pageid) {
      case "Abschlussarbeiten":
        this.setState({ data: jsonAbschlussarbeiten });
        break;

      case "Forschung":
        this.setState({ data: jsonForschung });
        break;
      //default: navigate("/BIM/"); --> ToDo: klappt noch nicht, soll zum BIM Dashboard zurück Routen

      default:
        this.setState({ data: jsonAbschlussarbeiten });
        break;
    }
  }
  render() {
    return (
      <>
        <div className="BIMHead">
          <h1 className="BIMHeadline h1margin">{this.props.pageid}</h1>
          {this.state.data === jsonAbschlussarbeiten && (
            <p className="BIMIntrotext">
              {" "}
              Viele Studierende der Fakultät Bau beschäftigen sich mit, oder
              haben sich mit, BIM beschäftigt. Hier finen sich die Themen, die
              bereits einmal zum BIM erarbeitet wurden. Bei Fragen und
              Interesse, wende dich an einen der Ansprechpartner zu BIM. Viele
              der Arbeiten bieten Anregungen für Ergänzungen, Erweiterungen der
              bereits erarbeiteten Themen oder Ideen für eigene Arbeiten.
            </p>
          )}
          {this.state.data === jsonForschung && (
            <p className="BIMIntrotext">
              Neben den Studierenden der Fakultät beschäftigen sich auch
              Unternehmen und Stiftungen mit dem BIM. Hier findest du
              Institutionen die die Forschung zum BIM vorranbringen und deren
              Projekte. Viele dieser Projekte bieten Anregungen für Bachelor-
              oder Masterarbeiten oder Projekte innerhalb der Fakultät, um die
              Arbeit mit BIM weiter zu verbessern und noch vielfältigere
              Anwendungsgebiete und Arbeitsweisen zu erarbeiten.
            </p>
          )}
        </div>
        <div className="BimTabelle">
          <div>
            <table>
              <tbody>
                {this.state.data.map((item, index) => {
                  return (
                    <tr key={index}>
                      {Object.keys(item).map((item2, index2) => {
                        return <td key={index2}>{item[item2]}</td>;
                      })}
                    </tr>
                  );
                })}
              </tbody>
            </table>
          </div>
        </div>
      </>
    );
  }
}

export default BIMTabellenSeite;
