import React, { Component } from "react";
import FeedList from "../news/FeedList";
import BIMInformationenWidget from "./BIMInformationenWidget";

class BIMDashboard extends Component {
  componentDidMount = () => {
    console.log("here");
    // setzt Breadcrumb auf "BIM"
  };

  render() {
    return (
      <>
        <div>
          <div>
            <h1 className="BIMHeadline h1margin">
              Building Information Modelling (BIM)
            </h1>
            <p>
              Digitalisierung, ein Schlagwort das heute überall zu finden ist.
              Auch im Bau hinterlässt die digitale Welt ihre Spuren und als
              moderne Hochschule sieht sich die HTWK als Vorreiter für neue
              Technologien und Methoden. Mit dem Building Information Moddeling
              geht die Branche mit der Zeit und revolutioniert dabei den
              Arbeitsalltag.
            </p>
            <img
              className="BIMPic"
              width="60%"
              src={require("./BIMDashboard.png")}
              alt="Ladefehler"
            />
            <p>
              Kooperieren bei Planung und Bau, Komprimieren und Konzentrieren
              aller Informationen – Das BIM ermöglicht es diese für jedes
              Projekt entscheidenden Aspekte zu verfolgen. Alle relevanten
              Informationen, von Architekturentscheidungen über Kosten bis zu
              Schaltplänen, können an einem Ort gebündelt und von allen
              Teammitgliedern genutzt werden, vorbei sind die Zeiten in denen
              umständlich Akten gewälztes werden mussten, mit dem BIM kann
              übersichtlich, effizient und schnell gearbeitet werden.
            </p>
            <p />
          </div>

          <div className="widgetkapselungBIM">
            <FeedList
              title="Meldungen zu BIM"
              url="http://localhost:5001/api/bim/bim-startseite/meldungen/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=77385"
              alias="BIM-Meldungen"
            />

            <BIMInformationenWidget />
          </div>
        </div>
      </>
    );
  }
}

export default BIMDashboard;
