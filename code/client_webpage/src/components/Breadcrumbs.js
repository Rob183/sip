import React, { Fragment } from "react";
import { Location } from "@reach/router";
import { Link } from "@reach/router";

import "./Breadcrumbs.css";

export default function Breadcrumbs(props) {
  return (
    <>
      <Location>
        {(props) => {
          // "Artikel" wird aus den Pfadnamen entfernt
          const pathename = props.location.pathname
            .split("/")
            .filter((item) => item !== item.toLowerCase() || item === "")
            .map((item) => decodeURIComponent(item)); // Zeichen die in der URL angezeigt werden, verschwinden
          const fertigerPathname = pathename.splice(1, pathename.length - 1); // Entferne das erste Element aus dem Array (pathename.length - 2 dann auch das letzte wurde aber von Marcel geändert)
          // Ein Array mit dem ganzen aktuellem Path
          const fullPath = fertigerPathname; // NEU

          // ------- Anpassen der .md-Datein Breadcrumbs ----------
          for (let i = 0; i < fertigerPathname.length; i++) {
            // Geht alle Elemente des fertigen Paths durch
            let regExp = new RegExp(".md"); // Überprüfe ob ".md" enthalten
            let regExpGeo = new RegExp(".geojson"); // Überprüfe ob ".geojson" enthalten
            if (regExp.test(fertigerPathname[i])) {
              // wenn ".md" enthalten ja
              fertigerPathname[i] = fertigerPathname[i].replace(regExp, ""); // Dann entferne dieses
            }
            if (regExpGeo.test(fertigerPathname[i])) {
              // wenn ".geojson" ja
              fertigerPathname[i] = fertigerPathname[i].replace(regExpGeo, ""); // Dann entferne dieses
            }
            let regExp2 = new RegExp("_"); // Prüft ob "_"s im Dateinamen enthalten sind
            while (regExp2.test(fertigerPathname[i])) {
              // wenn ja
              fertigerPathname[i] = fertigerPathname[i].replace(regExp2, " "); // Dann ersetze diese mit Leerzeichen
            }
          }

          switch (fertigerPathname[0]) {
            case "": // Wenn auf Hauptseite soll Hephaistos angezeigt werden
              // fertigerPathname.length === 0 geändert von Marcel damit nur auf der Startseite "Hephaistos" angezeigt wird.. ansonsten wurde nichts angezeigt oder "Hephaistos" wurde auch bei Informationen, Stundenplan, Absch... ect. angezeigt
              fertigerPathname[0] = "Hephaistos";
              break;

            //Cases für alle NewsWidgets damit "Artikel" angezeigt wird
            case "BIM-Meldungen":
              fertigerPathname[0] = "Artikel";
              break;
            case "News":
              fertigerPathname[0] = "Artikel";
              break;
            case "Veranstaltungen":
              fertigerPathname[0] = "Artikel";
              break;
            default:
          }

          // Layout: Transparenz der Schrift wird ab dem Anfang um 0.2 erhöht
          let prevOp = 1.3;
          const opacties = [...fertigerPathname]
            .reverse()
            .map((item, index) => {
              prevOp = prevOp - 0.3;
              return prevOp;
            })
            .reverse();

          /*
          Funktion zum handeln der Breadcrumb-Links
          @param(item) Es wird das angeklickte Breadcrumb übergeben
          */
          function jumpTo(item) {
            let jumpPath = "";
            // Wenn das Breadcrumb "Informationen" angeklickt wurde
            if (item === "Informationen") {
              // Springe zu der entsprechenden Seite
              jumpPath = "Informationen/";
            } else {
              // Ansonsten erstelle den Path der Seite zu der gesprungen werden soll
              let add = true;
              // Gehe den gesamten Path der aktuell aufgerufenen Seite durch
              for (let j = 0; j < fullPath.length; j++) {
                // Und erstelle ihn nach bis zu dem angeklickten Breadcrumb
                if (add) jumpPath += encodeURIComponent(fullPath[j]) + "/";
                if (fullPath[j] === item) add = false;
              }
            }
            // Am Schluss wird der Path der anzusteuernden Seite zurückgegeben
            return jumpPath;
          }

          // Ausgabe des Breadcrumbs
          return (
            <div>
              {fertigerPathname.map((item, index) => {
                // Was legt das fest für den Style?
                const opacityStyle = { opacity: opacties[index] };
                if (index !== fertigerPathname.length - 1) {
                  return (
                    <Fragment key={index}>
                      <Link to={jumpTo(item)} key={index}>
                        <span className="crumb" style={opacityStyle}>
                          {item}
                        </span>
                      </Link>

                      <span className="dash" style={opacityStyle}>
                        —
                      </span>
                    </Fragment>
                  );
                } else {
                  return (
                    <span key={index} className="crumb" style={opacityStyle}>
                      {item}
                    </span>
                  );
                }
              })}
            </div>
          );
        }}
      </Location>
    </>
  );
}
