import React, { Component } from "react";
import PdfLoader from "../PdfLoader/PdfLoader";

class AbschlussPoster extends Component {
  render() {
    var string1 = "/abschlussarbeiten/Poster";
    var string2 = this.props.year;
    var string3 = ".pdf";
    var string = string1 + string2 + string3;
    return (
      <div>
        <PdfLoader file={string} />
      </div>
    );
  }
}
export default AbschlussPoster;
