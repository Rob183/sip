import React, { Component } from "react";
import { Link } from "@reach/router";
import { VerticalTimelineElement } from "react-vertical-timeline-component";
import "./timeline.css"; //Styling von TimeLineELement, Grund fuer diesen import siehe timeline.css

//Klasse zum rendern der Infobox im Zeitstrahl
class TimeLineElement extends Component {
  render() {
    return (
      //jedes TimeLineElement ist gleichzeitig Link
      //Auswaehlen des Links fuehrt zur Komponente AbschlussPoster
      //dort wird mithilfe der Komponente PdfLoader die Pdf Datei des entsprechenden Jahres gerendert
      <Link to={this.props.year} style={{ textDecoration: "none" }}>
        <VerticalTimelineElement
          className="vertical-timeline-element--work"
          iconStyle={{
            color: "#004699"
          }}
          icon={this.props.icon}
        >
          <h2 className="vertical-timeline-element-title">
            {this.props.title}
          </h2>
          <h4 className="vertical-timeline-element-subtitle">
            {this.props.author}
          </h4>
          <p>{this.props.summary}</p>
        </VerticalTimelineElement>
      </Link>
    );
  }
}

export default TimeLineElement;
