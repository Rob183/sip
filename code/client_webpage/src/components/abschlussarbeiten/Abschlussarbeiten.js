import React, { Component } from "react";
import { Router } from "@reach/router";

import AbschlussZeitstrahl from "./AbschlussZeitstrahl.js";
import AbschlussPoster from "./AbschlussPoster.js";

class Abschlussarbeiten extends Component {
  render() {
    return (
      <Router>
        <AbschlussZeitstrahl path="/" />
        <AbschlussPoster path="/:year" />
      </Router>
    );
  }
}

export default Abschlussarbeiten;
