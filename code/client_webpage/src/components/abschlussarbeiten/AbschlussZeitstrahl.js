import React, { Component } from "react";
import { VerticalTimeline } from "react-vertical-timeline-component";
import abschlussarbeiten from "./abschlussarbeiten.json";
import TimeLineElement from "./TimeLineElement";

//import der icons
import {
  FaArchway,
  FaBalanceScale,
  FaHammer,
  FaGavel,
  FaLaptop,
  FaCity,
  FaUniversity,
  FaToriiGate
} from "react-icons/fa";

//Array aus 9 moeglichen icons, className icon1 und icon2 zur wechselseitigen Anordnung
var data = [
  <FaHammer className={"icon1"} />,
  <FaArchway className={"icon2"} />,
  <FaBalanceScale className={"icon1"} />,
  <FaCity className={"icon2"} />,
  <FaArchway className={"icon1"} />,
  <FaLaptop className={"icon2"} />,
  <FaGavel className={"icon1"} />,
  <FaUniversity className3={"icon2"} />,
  <FaToriiGate className={"icon1"} />
];

//Klasse zum rendern des Zeitstrahls
class AbschlussZeitstrahl extends Component {
  //render Methode iteriert abschlussarbeiten.json mithifle von map durch
  //bei jeder Iteration wird JSX ELement TimeLineElement erstellt
  //Attribute von TimeLineElement werden initialisert mit Werten der Schluessel aus abschlussarbeiten.json
  render() {
    var iconNumber = -1;
    return (
      <VerticalTimeline className="vertical-timeline">
        {abschlussarbeiten.map((element, index) => {
          //wenn alle icons aus data[] vollstaendig durchlaufen, dann starte wieder beim 1. icon
          if (index === 9) {
            iconNumber = -1;
          }

          //fuer wechselseitige Positionierung des Jahres
          var stringCl;

          if (index % 2 === 0) {
            stringCl = "year";
          } else {
            stringCl = "year2";
          }

          iconNumber = iconNumber + 1;
          //return TimeLineElement mit initialisierten Attributen
          return (
            <div className="box3">
              <p className={stringCl}>{element.year}</p>
              <TimeLineElement
                key={index}
                year={element.year}
                title={element.title}
                author={element.author}
                summary={element.summary}
                icon={data[iconNumber]}
              />
            </div>
          );
        })}
      </VerticalTimeline>
    );
  }
}
export default AbschlussZeitstrahl;
