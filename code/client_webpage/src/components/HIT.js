import React, { useEffect, useState } from "react";
import Loader from "./MarkdownLoader";

export default function Hilfe(props) {
  const [text, setText] = useState("");

  useEffect(() => {
    async function getText() {
      setText(await (await fetch("/HIT.md")).text());
    }
    getText();
  }, []);

  return <Loader text={text} />;
}
