//Komponente zum Auslesen von markdown Dateien
import React from "react";
import ReactMarkdown from "react-markdown";
//Gestaltung der Bilder und des Textes
import "../components/formats.css";
import "../components/text.css";
import QRCode from "qrcode-react";
import PdfLoader from "./PdfLoader/PdfLoader";
import { sizeQr } from "./qrConfig.js";
import { backgroundcolour } from "./qrConfig.js";
import { colour } from "./qrConfig.js";

import VideoLoader from "./VideoLoader.js";

// Zusätzliche Renderer, basierend auf Bilder-Tags
function customTypeRenderers(props) {
  switch (props.alt) {
    // PDF
    case "pdf":
      var scale = 1.0;
      var anchors = props.src.split("#");
      anchors.shift();
      anchors.forEach((anchor) => {
        if (anchor.indexOf("s") === 0) scale = anchor.substr(2);
      });
      return <PdfLoader scale={scale} file={props.src} classList={anchors} />;

    case "mp4":
      return <VideoLoader src={props.src} />;

    // Normale Bilder
    default:
      return <img {...props} />;
  }
}

const MarkdownLoader = (props) => {
  // Links als QR-Code
  const RENDERERS = {
    //wende für jeden Link in der md Datei Funktion auf Arg props an (link:) (wäre es text: alle Textelemente wären als QR Code dargestellt)
    //Komponente QRCode benoetigt als Attribut value die Url des Links (=props.href)
    link: (props) => (
      <QRCode
        value={props.href}
        size={sizeQr}
        fgColor={colour}
        bgColor={backgroundcolour}
      />
    ),
    // definiert zusätzliche Typen, basierend auf Bilder-Tags
    image: customTypeRenderers,
  };
  // Komponente ReactMarkdown liest md Datei von uebergebener Url aus
  return <ReactMarkdown source={props.text} renderers={RENDERERS} />;
};

export default MarkdownLoader;
