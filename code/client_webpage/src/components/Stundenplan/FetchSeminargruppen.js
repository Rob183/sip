// eslint-disable-next-line no-unused-vars
const semester = getSemester();

// Berechnung des aktuellen Semesters
// Aktueller Monat < 3 (April) oder > 8 (September) -> Wintersemester
// sonst: Sommersemester
function getSemester() {
  var now = new Date();
  var monthNumber = now.getMonth();
  if (monthNumber < 3 || monthNumber > 8) {
    return "ws";
  } else {
    return "ss";
  }
}

class StundenplanFetcher {
  semester = getSemester();

  getSemester() {
    var now = new Date();
    var monthNumber = now.getMonth();
    if (monthNumber < 3 || monthNumber > 8) {
      return "ws";
    } else {
      return "ss";
    }
  }

  // Proof of Concept: Stundenplandaten via CORS-Proxy laden
  fetchData() {
    return new Promise((resolve) => {
      fetch(
        `http://localhost:5001/api/stundenplan/stundenplan/xml/public/semgrp_${this.semester}.xml`
      ) // via lokalem CORS Proxy und nur fuer Sommersemester
        .catch((err) => {
          console.log(err);
        })
        .then((response) => response.text())
        .catch((err) => {
          console.log(err);
        })
        .then((contents) => {
          const parser = new DOMParser();
          const doc = parser.parseFromString(contents, "application/xml");
          resolve(doc);
        });
    });
  }

  async getFakultaeten() {
    var doc = await this.fetchData();
    const itemsFak = doc.getElementsByTagName("fakultaet");
    var newData = [];
    for (let itemFak of itemsFak) {
      var data = {
        label: itemFak.getAttribute("name"),
        value: itemFak.getAttribute("id"),
      };
      newData.push(data);
    }
    return newData;
  }

  async getStudiengaenge(id) {
    var doc = await this.fetchData();
    const object = doc.getElementById(id);
    const itemsStud = object.getElementsByTagName("studiengang");
    var newData = [];
    for (let itemStud of itemsStud) {
      var data = {
        label: itemStud.getAttribute("name"),
        value: itemStud.getAttribute("id"),
      };
      newData.push(data);
    }
    return newData;
  }

  async getSeminargruppen(id) {
    var doc = await this.fetchData();
    const object = doc.getElementById(id);
    const itemsSem = object.getElementsByTagName("semgrp");
    var newData = [];
    for (let itemSem of itemsSem) {
      var data = {
        label: itemSem.getAttribute("name"),
        value: itemSem.getAttribute("id"),
      };
      newData.push(data);
    }
    return newData;
  }
}

export function getFetcher() {
  // Erstelle neue NewsFetcher Instanz und speichere sie
  const newFetcher = new StundenplanFetcher();
  return newFetcher;
}
