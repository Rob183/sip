import React, { Component } from "react";
import StundenplanEingabe from "./StundenplanEingabe";

class Stundenplan extends Component {
  /* aktuell leerer Stundenplan Component */
  render() {
    return (
      <div>
        <div>
          <h1 className="h1margin">Stundenplan</h1>
        </div>
        <StundenplanEingabe />
      </div>
    );
  }
}
export default Stundenplan;
