import React, { Component } from "react";
import StundenplanAusgabe from "./StundenplanAusgabe";
import "./stundenplan.css";
import Select from "react-select";
import { getFetcher } from "./FetchSeminargruppen";

class StundenplanEingabe extends Component {
  /* Übernahme aus: "https://github.com/JedWatson/react-select" */
  constructor(props) {
    super(props);
    this.state = {
      selectedFakultaet: null,
      selectedStudiengang: null,
      selectedSemgrp: null,
      semester: "",
      fakultaeten: [],
      studiengaenge: [],
      semgrp: [],
      finalSelection: ""
    };
  }

  //Fetcher für Objekte der Fakultäten, Studiengänge und Seminargruppen
  async fetchFakultaeten() {
    this.setState({
      semester: await getFetcher().getSemester(),
      fakultaeten: await getFetcher().getFakultaeten()
    });
  }

  async fetchStudiengaenge(select) {
    this.setState({
      selectedFakultaet: select,
      studiengaenge: await getFetcher().getStudiengaenge(select.value),
      selectedStudiengang: null,
      selectedSemgrp: null,
      finalSelection: ""
    });
  }

  async fetchSeminargruppen(select) {
    this.setState({
      selectedStudiengang: select,
      semgrp: await getFetcher().getSeminargruppen(select.value),
      selectedSemgrp: null,
      finalSelection: ""
    });
  }

  setSelectedSemgrp(select) {
    this.setState({
      selectedSemgrp: select
    });
  }

  async componentDidMount() {
    await this.fetchFakultaeten();
    this.handleChangeFakultaet({ label: "Bauwesen", value: "FB" });
  }

  //Handles für die Dropdown-Menüs
  handleChangeFakultaet = select => {
    this.fetchStudiengaenge(select);
    console.log(`Option selected:`, select);
  };

  handleChangeStudiengang = select => {
    this.fetchSeminargruppen(select);
    console.log(`Option selected:`, select);
  };

  handleChangeSemgrp = select => {
    this.setSelectedSemgrp(select);
    console.log(`Option selected:`, select);
  };

  handleClick = () => {
    if (this.state.selectedSemgrp && this.state.selectedSemgrp.value) {
      this.setState({
        finalSelection: this.state.selectedSemgrp.value
      });
    }
  };

  render() {
    /* Übernahme aus: "https://github.com/JedWatson/react-select" */
    return (
      <div>
        <div className="containerEingabe">
          <div className="ausgabeFeld">
            <div className="afFakultät">
              <h3>Fakultät:</h3>
              {/* Übernahme aus: "https://github.com/JedWatson/react-select" */}
              <Select
                value={this.state.selectedFakultaet}
                onChange={this.handleChangeFakultaet}
                options={this.state.fakultaeten}
              />
            </div>

            <div className="afStudiengang">
              <h3>Studiengang:</h3>
              <Select
                value={this.selectedStudiengang}
                onChange={this.handleChangeStudiengang}
                options={this.state.studiengaenge}
              />
            </div>

            <div className="afSeminargruppe">
              <h3>Seminargruppe:</h3>
              <Select
                value={this.selectedSemgrp}
                onChange={this.handleChangeSemgrp}
                options={this.state.semgrp}
              />
            </div>

            <button className="buttonAbschicken" onClick={this.handleClick}>
              "Zeig's mir!"
            </button>
          </div>
        </div>

        {this.state.finalSelection ? (
          (console.log(this.state.finalSelection),
          (
            <StundenplanAusgabe
              semester={this.state.semester}
              semgrp={this.state.finalSelection}
              key={this.state.semester + this.state.finalSelection}
            />
          ))
        ) : (
          <p className="tabText" style={{ marginLeft: "5.2rem" }}>
            Bitte korrekte Auswahl treffen.
          </p>
        )}
      </div>
    );
  }
}
export default StundenplanEingabe;
