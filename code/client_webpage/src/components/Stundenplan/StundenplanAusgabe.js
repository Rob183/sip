import React, { Component } from "react";
/*https://github.com/reactjs/react-tabs*/
import { Tab, Tabs, TabList, TabPanel } from "react-tabs";
import "react-tabs/style/react-tabs.css";

import "./stundenplan.css";

class StundenplanAusgabe extends Component {
  wochentage = [
    "Montag",
    "Dienstag",
    "Mittwoch",
    "Donnerstag",
    "Freitag",
    "Sa",
    "So"
  ];

  constructor(props) {
    super(props);
    this.state = {
      semester: "ss",
      semgrp: "18ARB-4",
      loaded: "pending",
      dateLoaded: ""
    };
  }
  //https://stundenplan.htwk-leipzig.de/ss/Berichte/Text-Listen;Studenten-Sets;name;18ARB-4?template=sws_semgrp_js&weeks=_25


  getWeek() {
    let now = new Date();
    let onejan = new Date(now.getFullYear(), 0, 1);
    let calendarWeek = Math.ceil(
      ((now.getTime() - onejan.getTime()) / 86400000 + onejan.getDay() + 1) / 7
    );
    if (now.getMonth() > 2){
      return calendarWeek;
    } else{
      return 52 + calendarWeek;
    }
  }
  /**
   * Gibt ein Array (Wochentage) eines Arrays (Veranstaltungen) von Objekten (Eigenschaften der Veranstaltung) zurueck.
   * stundenplan[i] mit i = 0..6 meint Mo..So
   * stundenplan[i][j] mit j = 0 .. n Veranstaltungen an diesem Wochentag
   * stundenplan[i][j].beginn oder .xyz ist dann das Attribut dieser Veranstaltung
   */
  fetchStundenplan() {
    return new Promise(resolve => {
      let week = this.getWeek();
      const urlPrefix = "http://localhost:5001/api/stundenplan/";
      const url =
        urlPrefix +
        this.props.semester +
        "/Berichte/Text-Listen;Studenten-Sets;name;" +
        this.props.semgrp +
        "?template=UNEinzelGru&weeks=" +
        week;
      fetch(url)
        .catch(error => {
          var newData = false;
          console.log(error);
          resolve(newData);
        })

        // Ändere das Encoding von ISO-8859-1 to UTF-8
        .then(response => response.arrayBuffer())
        .then(buffer => {
          let decoder = new TextDecoder("iso-8859-1");
          let text = decoder.decode(buffer);
          return text;
        })
        //.then(response => response.text())
        .then(contents => {
          var newData = [];
          const parser = new DOMParser();
          const doc = parser.parseFromString(contents, "text/html");
          const tage = doc.querySelectorAll("table.spreadsheet"); // selektiere alle Tabellen/Wochentage
          for (let tag of tage) {
            var tagData = [];
            const veranstaltungen = tag.querySelectorAll(
              "tr:not(.columnTitles)"
            ); // selektiere alle Zeilen ausser der Kopfzeile
            for (let veranstaltung of veranstaltungen) {
              const spalten = veranstaltung.querySelectorAll("td"); // selektiere alle Spalten
              // erwartete Spaltenreihenfolge: Planungswochen 	Anfang 	Ende 	Raum 	Veranstaltung 	Art 	Dozent 	Bemerkungen 	gebucht am
              if (spalten.length === 9) {
                var data = {
                  planungswochen: spalten.item(0).textContent,
                  beginn: spalten.item(1).textContent,
                  ende: spalten.item(2).textContent,
                  raum: spalten.item(3).textContent,
                  veranstaltung: spalten.item(4).textContent,
                  art: spalten.item(5).textContent,
                  dozent: spalten.item(6).textContent
                };
                tagData.push(data); // fuege Veranstaltung zum Array dieses Tags hinzu
              }
            }
            newData.push(tagData); // fuege Tag und dessen Veranstaltungen hinzu
          }
          resolve(newData);
        });
    });
  }

  /**
   * Laedt den Stundenplan.
   * Wenn dies fehlschlägt wird this.state.loaded = false gesetzt.
   * Ist dies erfolgreich wird this.state.loaded = true gesetzt und in this.state.data ist der Stundenplan.
   * Siehe fetchStundenplan() fuer den Aufbau von this.state.data.
   *
   * @ToDo (optional) Cache-Algorithmus wie im NewsFetcher.
   */
  async fetchData() {
    this.setState({
      data: await this.fetchStundenplan()
    });
    if (this.state.data === false) {
      this.setState({
        loaded: "false"
      });
    } else {
      this.setState({
        loaded: "true",
        dateLoaded: Date.now()
      });
    }
  }

  /**
   * Lade Daten.
   * Da die Lade-Funktionen den State benutzen, kann einfach im render() mit
   * this.state.loaded und this.state.data gearbeitet werden.
   */
  componentDidMount() {
    this.fetchData();
  }

  // Lade neu, wenn sich die ausgewählte Seminargruppe ändert
  componentDidUpdate(prevProps) {
    if (prevProps.semgrp !== this.props.semgrp) {
      this.fetchData();
    }
  }

  render() {
    // Festlegen welcher Tag "heute" ist
    let dayNumber = (new Date().getDay() + 7) % 8; // Mo=0, Di=1...

    const dateLoaded = new Date(this.state.dateLoaded)
      .toLocaleDateString()
      .replace(/\//g, ".");

    const timeLoaded = new Date(this.state.dateLoaded).toLocaleTimeString([], {
      hour: "2-digit",
      minute: "2-digit"
    });

    if (this.state.loaded === "true") {
      return (
        <div className="containerAusgabe">
          <Tabs defaultIndex={dayNumber}>
            <div className="tabLinks">
              <TabList>
                {//mappen der Wochentage auf Tabs
                this.wochentage.map((tag, index) => {
                  return <Tab key={tag}>{tag}</Tab>;
                })}
              </TabList>
            </div>

            <div className="tabContent">
              {//mappen der Wochentage auf Tabinhalt
              this.wochentage.map(tag => {
                return (
                  <TabPanel key={tag}>
                    <table className="tabTable">
                      <thead className="tabThead">
                        <tr>
                          <td>Planungswochen</td>
                          <td>Beginn</td>
                          <td>Ende</td>
                          <td>Raum</td>
                          <td>Veranstaltung</td>
                          <td>Art</td>
                          <td>Dozent</td>
                        </tr>
                      </thead>
                      <tbody>
                        {//mappen der Daten des Tages
                        this.state.data[this.wochentage.indexOf(tag)].map(
                          item => {
                            return (
                              <tr
                                key={
                                  item.beginn + item.raum + item.veranstaltung
                                }
                              >
                                <td>{item.planungswochen}</td>
                                <td>{item.beginn}</td>
                                <td>{item.ende}</td>
                                <td>{item.raum}</td>
                                <td>{item.veranstaltung}</td>
                                <td>{item.art}</td>
                                <td>{item.dozent}</td>
                              </tr>
                            );
                          }
                        )}
                      </tbody>
                    </table>
                  </TabPanel>
                );
              })}
            </div>
          </Tabs>
          <p className="tabText">
            Zuletzt geladen am {dateLoaded} um {timeLoaded}.
          </p>
        </div>
      );
    } else if (this.state.loaded === "pending") {
      return <p className="tabText">Lädt...</p>;
    } else if (this.state.loaded === "false") {
      return (
        <p className="tabText">Stundenplan kann nicht angezeigt werden.</p>
      );
    }
  }
}
export default StundenplanAusgabe;
