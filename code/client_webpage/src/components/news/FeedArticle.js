import React, { Component } from "react";
import ReactHtmlParser from "react-html-parser";
import { convertNodeToElement } from "react-html-parser";
import { getFetcher } from "./NewsFetcher.js";
import "./FeedArticle.css";
import backupImage from "./geutebrueckbau.jpg";

var QRCode = require("qrcode-react");

const options = { decodeEntities: true, transform };

function transform(node, index) {
  if (node.type === "tag" && node.name === "a") {
    node.name = "span";
    return convertNodeToElement(node, index, transform);
  }
}

class FeedArticle extends Component {
  constructor(props) {
    super(props);
    this.state = {
      data: {},
      loaded: false
    };
  }

  //Laden der Artikel-Daten
  async fetchData() {
    this.setState({
      data: await getFetcher(this.props.alias).getNewsArticle(this.props.guid),
      loaded: true
    });
  }

  componentDidMount() {
    this.fetchData();
  }

  render() {
    if (this.state.loaded) {
      console.log(this.state.data);
      //Vorbereitung der Datumsanzeige
      var date;
      const datePart = new Date(this.state.data.date)
        .toLocaleDateString()
        .replace(/\//g, ".");

      const timePart = new Date(this.state.data.date).toLocaleTimeString([], {
        hour: "2-digit",
        minute: "2-digit"
      });

      var qrText;
      //Anzeigeaenderung je nach Widgetart
      if (this.props.alias === "Veranstaltungen") {
        date = "Findet statt am " + datePart + " ab " + timePart;
        qrText = "Scanne den QR-Code, um die Veranstaltungsseite anzusehen.";
      } else {
        date = "Veröffentlicht am " + datePart;
        qrText = "Scanne den QR-Code, um den ganzen Artikel zu lesen.";
      }
      console.log("THIS IS STATE IMG " + this.state.data.image);
      console.log(this.state.data);
      //Fallunterscheidung fuer Layout je nach (nicht) vorhandenem Bild
      if (this.state.data.image !== backupImage) {
        return (
          <div className="article">
            <h2>{this.state.data.title}</h2>
            <p className="date">{date}</p>
            <h3>{this.state.data.teaser}</h3>
            <div className="QRAndTextWrapper">
              <QRCode id="qr" value={this.state.data.link} />
              <p>{qrText}</p>
            </div>
            <div>{ReactHtmlParser(this.state.data.content, options)}</div>
            <hr />
          </div>
        );
      } else {
        return (
          <div className="article">
            <h2>{this.state.data.title}</h2>
            <p className="date">{date}</p>
            <h3>{this.state.data.teaser}</h3>
            <div className="ImageQRWrapper">
              <img src={this.state.data.image} alt="Ladefehler" />
              <div className="QRAndTextWrapper">
                <QRCode id="qr" value={this.state.data.link} />
                <p>{qrText}</p>
              </div>
            </div>
            <div>{ReactHtmlParser(this.state.data.content, options)}</div>
            <hr />
          </div>
        );
      }
    } else {
      return <p>Lädt...</p>;
    }
  }
}

export default FeedArticle;
