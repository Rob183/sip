import React, { Component } from "react";
import FeedListItem from "./FeedListItem";
import "./FeedList.css";
import { getFetcher } from "./NewsFetcher.js";

class FeedList extends Component {
  constructor(props) {
    super(props);
    this.state = { data: { date: new Date(2019, 1, 1) }, loaded: "pending" };
  }

  //ruft den NewsFetcher auf und lädt Artikel in den State
  async fetchData() {
    this.setState({
      data: await getFetcher(this.props.alias, this.props.url).getNewsList()
    });
    if (this.state.data.articles === false) {
      this.setState({
        loaded: "false"
      });
    } else {
      this.setState({
        loaded: "true"
      });
    }
  }

  componentDidMount() {
    this.fetchData();
  }

  render() {
    const date = new Date(this.state.data.date)
      .toLocaleDateString()
      .replace(/\//g, ".");

    const time = new Date(this.state.data.date).toLocaleTimeString([], {
      hour: "2-digit",
      minute: "2-digit"
    });

    if (this.state.loaded === "true") {
      return (
        <>
          <div className="widgetkapselung2">
            <div className="widgetheadline">{this.props.title}</div>
            <div className="Scrollkapselung">
              <div className="widget">
                {/*Mappen der einzelnen Artikel in die Liste*/}
                {this.state.data.articles &&
                  this.state.data.articles.map(item => {
                    console.log(this.state.data);
                    return (
                      <FeedListItem
                        key={item.guid}
                        alias={this.props.alias}
                        item={item}
                      />
                    );
                  })}
                <div>
                  Zuletzt aktualisiert am {date} um {time}
                </div>
              </div>
              <div className="fade" />
            </div>
          </div>
        </>
      );
    } else {
      if (this.state.loaded === "pending") {
        return (
          <div className="widgetkapselung2">
            <div className="widgetheadline">{this.props.title}</div>
            <div className="widget">
              <div>Lädt...</div>
            </div>
          </div>
        );
      } else {
        return (
          <div className="widgetkapselung2">
            <div className="widgetheadline">{this.props.title}</div>
            <div className="widget">
              <div>Laden der Daten fehlgeschlagen.</div>
            </div>
          </div>
        );
      }
    }
  }
}

export default FeedList;
