import React, { Component } from "react";
import { navigate } from "@reach/router";

import "./FeedListItem.css";

export default class FeedListItem extends Component {
  render() {
    //Vorbereitung der Datumsanzeige
    var date;
    const datePart = new Date(this.props.item.date)
      .toLocaleDateString()
      .replace(/\//g, ".");

    const timePart = new Date(this.props.item.date).toLocaleTimeString([], {
      hour: "2-digit",
      minute: "2-digit"
    });

    //Anzeigeaenderung je nach Widgetart
    if (this.props.alias === "Veranstaltungen") {
      date = datePart + " ab " + timePart;
    } else {
      date = datePart;
    }

    return (
      <div
        onClick={() =>
          navigate("aktuelles/" + this.props.alias + "/" + this.props.item.guid)
        }
      >
        <div className="FeedListItem">
          <div className="FeedListItemdate">{date}</div>
          <div className="FeedListItemdtitle">{this.props.item.title}</div>
          <div className="FeedListItempreview">{this.props.item.teaser}</div>
        </div>
      </div>
    );
  }
}
