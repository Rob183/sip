import { getFetcher, NewsFetcher } from "./NewsFetcher.js";

describe("NewsFetcher", () => {
  test("lädt alle Artikel im richtigen Format", async () => {
    const testNews = await getFetcher(
      "News",
      "http://localhost:5001/api/fb/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
    ).getNewsList();

    expect(testNews.articles.length).not.toBe(0);
  });

  test("Datum aktualiserung", async () => {
    await getFetcher(
      "News",
      "http://localhost:5001/api/fb/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
    ).refreshNewsList();
    var testDate = await getFetcher(
      "News",
      "http://localhost:5001/api/fb/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
    ).cache.date;

    expect(Math.abs(Date.now() - testDate)).toBeLessThan(10000);
  });

  test("getNewsList", async () => {
    const NewCache = await getFetcher(
      "News",
      "http://localhost:5001/api/fb/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
    ).getNewsList();

    expect(NewCache).not.toBeNull();
  });
});
