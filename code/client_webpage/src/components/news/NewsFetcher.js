class NewsFetcher {
  constructor(url) {
    this.url = url;
    this.cache.date = Date.now();
  }

  cache = {
    date: new Date(),
    articles: [],
  };

  _populateNewsObject(item, d) {
    item.childNodes.forEach((child) => {
      switch (child.nodeName) {
        case "guid":
          d.guid = child.textContent;
          break;
        case "pubDate":
          d.date = Date.parse(child.textContent);
          break;
        case "title":
          d.title = child.textContent;
          break;
        case "description":
          d.teaser = child.textContent;
          break;
        case "content:encoded":
          d.content = child.textContent;
          break;
        case "link":
          d.link = child.textContent;
          break;
        case "enclosure":
          d.image = child.getAttribute("url");
          break;
        default:
          break;
      }
    });
  }

  fetchArticles() {
    return new Promise((resolve) => {
      fetch(this.url)
        .catch(function (err) {
          var newData = false;
          resolve(newData);
        })
        .then((response) => response.text())
        .catch((err) => {
          console.log(err);
        })
        .then((contents) => {
          var newData = [];
          const parser = new DOMParser();
          const doc = parser.parseFromString(contents, "application/xml");
          const items = doc.getElementsByTagName("item");
          for (let item of items) {
            var d = {
              guid: 1,
              date: new Date(2019, 1, 1),
              title: "Eine News der FB",
              teaser: "Hier lädt eine News",
              link: "https://fb.htwk-leipzig.de",
              content: "",
              image: "backupImage",
            };
            this._populateNewsObject(item, d);
            newData.push(d);
          }
          resolve(newData);
        });
    });
  }

  async refreshNewsList() {
    this.cache.articles = await this.fetchArticles();
    // Datum der Aktualisierung wird auch gespeichert
    this.cache.date = Date.now();
  }

  async getNewsList() {
    if (this.cache.articles.length === 0) {
      await this.refreshNewsList();
    }
    return this.cache;
  }

  async getNewsArticle(guid) {
    if (!this.cache) {
      return { title: "Fehler, keine gültige GUID!" };
    } else {
      return (await this.getNewsList()).articles.find(
        (item) => item.guid === guid
      );
    }
  }
}

const fetchers = {};
export function getFetcher(alias, url) {
  if (!fetchers[alias]) {
    // Erstelle neue NewsFetcher Instanz und speichere sie
    const newFetcher = new NewsFetcher(url);
    fetchers[alias] = newFetcher;
  }

  return fetchers[alias];
}
