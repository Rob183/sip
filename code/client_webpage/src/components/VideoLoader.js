import React, { Component } from "react";
//Einbinden Videos
import { Player, ControlBar, BigPlayButton, LoadingSpinner } from "video-react";
import "../../node_modules/video-react/dist/video-react.css";

class VideoLoader extends Component {
  render() {
    return (
      <Player src={this.props.src} fullwidth={true}>
        <ControlBar autohide={false} disableCompletely={false} />
        <BigPlayButton position="center" />
        <LoadingSpinner />
      </Player>
    );
  }
}

export default VideoLoader;
