import React, { Component } from "react";
import "./UnlockDialog.css";
import bcrypt from "bcryptjs";
import { PIN } from "./PIN.js";

/**
 * Valid PIN hash.
 */
const validPIN = PIN;

/**
 * Unlock URL.
 */
const unlockURL = "/quit";

/* Texts */
const TEXT_HEADLINE = "Bitte PIN eingeben:";
const TEXT_BUTTON_DELETE = "Korrektur";
const TEXT_BUTTON_BACK = "Zurück";
const TEXT_BUTTON_SUBMIT = "OK";

/**
 * PIN based unlock dialog to exit SEB.
 */
export default class UnlockDialog extends Component {
  constructor(props) {
    super(props);
    this.state = {
      enteredPin: "",
      wrongPinEntered: false,
    };
    this.addNumber = this.addNumber.bind(this);
    this.deleteNumber = this.deleteNumber.bind(this);
    this.handleSubmit = this.handleSubmit.bind(this);
  }

  addNumber(event) {
    this.setState({
      enteredPin: this.state.enteredPin + event.target.value,
      wrongPinEntered: false,
    });
  }

  deleteNumber(event) {
    this.setState({
      enteredPin: this.state.enteredPin.slice(0, -1),
      wrongPinEntered: false,
    });
  }

  handleSubmit(event) {
    if (bcrypt.compareSync(this.state.enteredPin, validPIN) === true) {
      this.setState({
        wrongPinEntered: false,
      });
      window.location.replace(unlockURL);
    } else {
      this.setState({
        wrongPinEntered: true,
      });
    }
    event.preventDefault();
  }

  render() {
    return (
      <div>
        <div className="unlockOverlay" />
        <form className="unlockDialog" onSubmit={this.handleSubmit}>
          <span>{TEXT_HEADLINE}</span>
          <input
            type="password"
            name="unlockPin"
            id="unlockPin"
            disabled="disabled"
            value={this.state.enteredPin}
            className={this.state.wrongPinEntered ? "wrong" : ""}
          />
          <p>
            <input
              type="button"
              name="number"
              value="7"
              onClick={this.addNumber}
            />
            <input
              type="button"
              name="number"
              value="8"
              onClick={this.addNumber}
            />
            <input
              type="button"
              name="number"
              value="9"
              onClick={this.addNumber}
            />
          </p>
          <p>
            <input
              type="button"
              name="number"
              value="4"
              onClick={this.addNumber}
            />
            <input
              type="button"
              name="number"
              value="5"
              onClick={this.addNumber}
            />
            <input
              type="button"
              name="number"
              value="6"
              onClick={this.addNumber}
            />
          </p>
          <p>
            <input
              type="button"
              name="number"
              value="1"
              onClick={this.addNumber}
            />
            <input
              type="button"
              name="number"
              value="2"
              onClick={this.addNumber}
            />
            <input
              type="button"
              name="number"
              value="3"
              onClick={this.addNumber}
            />
          </p>
          <p>
            {this.state.enteredPin === "" ? (
              <input
                type="button"
                name="back"
                value={TEXT_BUTTON_BACK}
                onClick={this.props.getBackAction}
              />
            ) : (
              <input
                type="button"
                name="delete"
                value={TEXT_BUTTON_DELETE}
                onClick={this.deleteNumber}
              />
            )}
            <input
              type="button"
              name="number"
              value="0"
              onClick={this.addNumber}
            />
            <input type="submit" value={TEXT_BUTTON_SUBMIT} />
          </p>
        </form>
      </div>
    );
  }
}
