import React from "react";
import Enzyme, { shallow } from "enzyme";
import Adapter from "enzyme-adapter-react-16";
import UnlockDialog from "./UnlockDialog";

Enzyme.configure({ adapter: new Adapter() });

describe("UnlockDialog", () => {
  it("Soll alle Buttons clicken", () => {
    const wrapper = shallow(<UnlockDialog />);
    for (let i = 0; i < 10; i++) {
      let button = wrapper.find({ value: i.toString() });
      button.simulate("click", { target: { value: i.toString() } });
    }
    expect(wrapper.state("enteredPin")).toBe("0123456789");
  });

  it("Eine Eingabe loeschen", () => {
    const wrapper = shallow(<UnlockDialog />);
    let button = wrapper.find({ value: "7" });
    button.simulate("click", { target: { value: "7" } });
    expect(wrapper.state("enteredPin")).toBe("7");

    let dButton = wrapper.find({ name: "delete" });
    dButton.simulate("click", { target: { name: "delete" } });
    expect(wrapper.state("enteredPin")).toBe("");
  });

  it("Submiting", () => {
    const wrapper = shallow(<UnlockDialog />);
    const submitButton = wrapper.find("form");
    submitButton.simulate("submit", { preventDefault: jest.fn() });
    expect(wrapper.state("wrongPinEntered")).toBeTruthy();
  });
});
