import React, { Component } from "react";
import * as GoogleMaps from "google-maps";
import MarkerClusterer from "node-js-marker-clusterer";
import locations from "./locations.json";
import "./lageplan.css";
//import {MarkerClusterer} from "./markerclusterer";

//Import Konfigurationsvariablen
import { LageplanStartGebaeude } from "./Lageplan.Config.js";

class Lageplan extends Component {
  loader;
  loaderPromise;
  apiKey = "AIzaSyDqQ7q3NC4RnrnbWX5CjWhxbY6yEhSqhh0";
  version = "weekly"; // Damit keine "Google Maps JavaScript API warning: RetiredVersion" Warnings geworfen werden

  constructor(props) {
    super(props);

    // Verhindern, dass Google Maps geladen wird, wenn es bereits vorhanden ist
    if (!window.google) {
      this.loader = new GoogleMaps.Loader(this.apiKey, {
        version: this.version,
      });
      this.loaderPromise = this.loader.load();
    } else {
      this.loaderPromise = Promise.resolve(window.google);
    }
  }

  componentDidMount() {
    this.loaderPromise.then((google) => {
      // Hier der ganze Map Code, da es hier "google" gibt.
      // Vermutlich auch hier erst das laden der markerclusterer.js, via:
      const mapOptions = {
        zoom: 17,
        center: new google.maps.LatLng(51.3138, 12.374034), // center ist die Mitte der Karte, hier etwa die Mitte des HTWK Campus
        zoomControlOptions: {
          position: google.maps.ControlPosition.RIGHT_BOTTOM,
        },
        streetViewControlOptions: {
          position: google.maps.ControlPosition.LEFT_BOTTOM,
        },

        draggable: true,
        scrollwheel: true,

        styles: [
          { featureType: "poi", stylers: [{ visibility: "off" }] },
          {
            featureType: "road.highway",
            stylers: [{ hue: "#f0ad42" }],
          },
          {
            featureType: "water",
            stylers: [{ hue: "#A0C4FC" }],
          },
          {
            featureType: "landscape.natural",
            stylers: [{ hue: "#c5e193" }],
          },
        ],
      };
      this.map = new google.maps.Map(this.refs.map, mapOptions);
      let map = this.map; // Musste hinzugefügt werden damit die Buttons reibungslos funktionieren

      /* ***** ERKLÄRUNG JSON *****
      Die JSON-Datei "locations" mit dem Array markers[object1, object2, .., object17]
      let object = {
        icon=png,
        latitude=float, longitude=float,
        title=String,
        content=html,
        important=boolean
      }
      locations.markers.length ==>  17
      locations.markers[0].title  ==> Automatik-Museum Daueraustellung  */

      let markers = []; // Speicher für die Marker aus der JSON
      let markerInfoWindowMapping = []; // Speichere die InfoWindows der Marker

      // Gehe die JSON durch
      for (let i = 0; i < locations.markers.length; i++) {
        // Erstellt die Marker
        markers[i] = new google.maps.Marker({
          position: new google.maps.LatLng(
            locations.markers[i].latitude,
            locations.markers[i].longitude
          ),
          title: locations.markers[i].title,
          map: this.map,
          bounds: true,
          icon: locations.markers[i].icon,
          content: locations.markers[i].content,
        });

        // Wenn HTML-Content vorhanden erstelle daraus ein InfoWindow
        let infoWindow = null;
        if (locations.markers[i].content) {
          infoWindow = new google.maps.InfoWindow({
            content: locations.markers[i].content,
          });
        }
        markerInfoWindowMapping[i] = infoWindow;

        /* Fügt den Markern einen OnCLickListener-Funktion hinzu
           Welcher das Infofenster öffnet */
        google.maps.event.addListener(markers[i], "click", function () {
          for (let j = 0; j < markerInfoWindowMapping.length; j++) {
            markerInfoWindowMapping[j].close();
          }
          infoWindow.open(this.map, markers[i]);
        });
      }

      // MarkerCluster sind Wolken von Markern, wenn man herau zoomt und sich die Marker sonst ueberlagern
      // @ToDo die height/width muss angepasst werden, so dass die Zahlen lesbar sind
      // eslint-disable-next-line no-unused-vars
      var mc = new MarkerClusterer(map, markers, {
        gridSize: 50,
        maxZoom: 15,
        styles: [
          {
            height: 59,
            url: "map-icons/marker-pin.png",
            width: 45,
            textColor: "#FFFFFF",
            textSize: 20,
          },
          {
            height: 59,
            url: "map-icons/marker-pin.png",
            width: 45,
            textColor: "#FFFFFF",
            textSize: 20,
          },
          {
            height: 59,
            url: "map-icons/marker-pin.png",
            width: 45,
            textColor: "#FFFFFF",
            textSize: 20,
          },
          {
            height: 59,
            url: "map-icons/marker-pin.png",
            width: 45,
            textColor: "#FFFFFF",
            textSize: 20,
          },
          {
            height: 59,
            url: "map-icons/marker-pin.png",
            width: 45,
            textColor: "#FFFFFF",
            textSize: 20,
          },
        ],
      });

      // OnCLickListener zum Springen der Locations per Buttonclick
      // lädt ALLE Buttons in ein Array
      let locationButtons = document.querySelectorAll(".button");

      if (
        locationButtons &&
        locationButtons instanceof NodeList &&
        locationButtons.length > 0
      ) {
        // Geht alle Buttons durch
        for (let i = 0; i < locationButtons.length; i++) {
          // Gibt jedem Button ein EventListener
          locationButtons[i].addEventListener("click", function (event) {
            let buttonTitle = locationButtons[i].getAttribute("title"); // Holt sich das title Attribut
            // Geht alle Marker durch
            for (let j = 0; j < markers.length; j++) {
              // Wenn der Titel des Markers mit dem des Buttons übereinstimmen
              if (markers[j].title === buttonTitle) {
                // Dann "schwenke" (pan to) die Karte auf diesen Marker
                map.panTo(markers[j].position);
                console.log(markers[j].position.lat());
                // Öffnen des Infofensters, falls vorhanden
                if (markers[j].content) {
                  // Schließe alle möglichen offenen Infofenster
                  for (let k = 0; k < markerInfoWindowMapping.length; k++) {
                    markerInfoWindowMapping[k].close();
                  }
                  // Öffne das gewünschte Infofenster
                  markerInfoWindowMapping[j].open(map, markers[j]);
                }
                return;
              }
            }
          });
        }
      }

      /* Beim Ersten Aufruf der Karte wird auf das in der Konfigdatei "Lageplan.Config.js" angegebene Gebäude
          zentriert. Und das dazugehörige Infofenster wird geöffnet.*/
      // Gehe alle Marker durch
      for (let i = 0; i < markers.length; i++) {
        // Wenn Geutebrück-Bau-Marker gefunden
        if (markers[i].title === LageplanStartGebaeude) {
          // Dann "schwenke" (pan to) die Karte auf diesen Marker
          map.panTo(markers[i].position);
          console.log(markers[i].position.lat());
          // Öffnen des Infofensters, falls vorhanden
          if (markers[i].content) {
            // Schließe alle möglichen offenen Infofenster
            for (let k = 0; k < markerInfoWindowMapping.length; k++) {
              markerInfoWindowMapping[k].close();
            }
            // Öffne das gewünschte Infofenster
            markerInfoWindowMapping[i].open(map, markers[i]);
          }
          return;
        }
      }
    });
  }

  // width: 900, height: 600  .. erstmal geändert damit auch Buttons einfach sichtbar sind
  render() {
    return (
      <div className="site">
        <div>
          <h1>Lageplan</h1>
        </div>
        {/* DÃœRFTE auf der Stele die perfekte weite und höhe haben.. aber wer weiß.. sieht man erst beim ausprobieren */}
        <div id="map" ref="map" style={{ width: "90vw", height: "35vh" }}>
          Hier sollte eine Karte sein!
        </div>

        <div id="wrapper">
          {
            // Loop zum Erzeugen der Buttons aus der JSON
            // mschalln: Laut Anforderung soll es nicht für alle Gebäude einen Button geben, sondern nur für relevante Gebäude
            // der Fakultät Bau. Diese haben in der locations.json das Attribut important=true
            locations.markers.map(function (marker, i) {
              //Abfrage ob Content vorhanden UND ob Marker (Gebäude) auch important ist
              if (marker.content && marker.important) {
                // Filtern der Straße mit RegExp
                let regExpStraße = new RegExp("<br /><div>.*<br />");
                let resultStraße = regExpStraße.exec(marker.content); // "<br /><div>Straße<br />"
                resultStraße[0] = resultStraße[0].replace("<br /><div>", ""); // "<br /><div>" aus dem String löschen
                resultStraße[0] = resultStraße[0].replace("<br />", ""); // "<br />" aus dem String löschen

                // Erstelle den jeweiligen Button mit den gefilterten Informationen
                return (
                  <button className="button" title={marker.title}>
                    <strong>{marker.title}</strong>
                    <p>{resultStraße[0]}</p>
                  </button>
                );
              } else {
                return null;
              }
            })
          }
        </div>
      </div>
    );
  }
}

export default Lageplan;
