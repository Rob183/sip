import React, { Component } from "react";
import { Page } from "react-pdf";
import { Document } from "react-pdf/dist/entry.webpack";
import { Swipeable } from "react-swipeable";

import "./PdfPage.css";

class PdfLoader extends Component {
  state = {
    numPages: null,
    pageNumber: 1,
  };

  constructor(props) {
    super(props);
    this.documentClasses = [];
    this.pageScale = 1.0;
    if (typeof this.props.classList !== "undefined")
      this.documentClasses.push(...this.props.classList);
    if (typeof this.props.scale !== "undefined")
      this.pageScale = this.props.scale;
  }

  onDocumentLoadSuccess = ({ numPages }) => {
    this.setState({ numPages });
  };

  nextPage = () => {
    if (this.state.pageNumber < this.state.numPages) {
      this.setState({ pageNumber: this.state.pageNumber + 1 });
    }
  };
  previousPage = () => {
    if (this.state.pageNumber > 1)
      this.setState({ pageNumber: this.state.pageNumber - 1 });
  };

  config = {
    onSwipedLeft: () => this.nextPage(),
    onSwipedRight: () => this.previousPage(),
    preventDefaultTouchmoveEvent: true,
    trackMouse: true,
  };

  render() {
    const { pageNumber, numPages } = this.state;
    return (
      <Swipeable {...this.config}>
        <div className={this.documentClasses.join(" ")}>
          <Document
            file={this.props.file}
            onLoadSuccess={this.onDocumentLoadSuccess}
          >
            <Page
              pageNumber={pageNumber}
              className="PDFPage"
              scale={this.pageScale}
            />
          </Document>
          {numPages > 1 && (
            <div className="PDFNav">
              <button
                type="button"
                disabled={pageNumber <= 1}
                onClick={this.previousPage}
              >
                &lt;
              </button>
              <span>
                Seite {pageNumber || (numPages ? 1 : "--")} von{" "}
                {numPages || "--"}
              </span>
              <button
                type="button"
                disabled={pageNumber >= numPages}
                onClick={this.nextPage}
              >
                &gt;
              </button>
            </div>
          )}
        </div>
      </Swipeable>
    );
  }
}

export default PdfLoader;
