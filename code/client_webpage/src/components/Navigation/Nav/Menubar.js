import React from "react";
import "./Menubar.css";

const Menubar = () => {
  return (
    <div>
      <ul>
        <li>
          <a href="/">Lageplan</a>
        </li>
        <li>
          <a href="/">News</a>
        </li>
        <li className="dropdown">
          <a href="/" className="dropbtn">
            BIM
          </a>
          <div className="dropdown-content">
            <a href="#">Link 1</a>
            <a href="#">Link 2</a>
            <a href="#">Link 3</a>
          </div>
        </li>
        <li className="dropdown">
          <a href="/" className="dropbtn">
            Abschlussarbeiten
          </a>
          <div className="dropdown-content">
            <a href="#">Link 1</a>
            <a href="#">Link 2</a>
            <a href="#">Link 3</a>
          </div>
        </li>
        <li className="dropdown">
          <a href="/" className="dropbtn">
            Stundenplan
          </a>
          <div className="dropdown-content">
            <a href="#">Link 1</a>
            <a href="#">Link 2</a>
            <a href="#">Link 3</a>
          </div>
        </li>
        <li className="dropdown">
          <a href="/" className="dropbtn">
            Kalender
          </a>
          <div className="dropdown-content">
            <a href="#">Link 1</a>
            <a href="#">Link 2</a>
            <a href="#">Link 3</a>
          </div>
        </li>
        <li>
          <a href="/">Hilfe</a>
        </li>
      </ul>
    </div>
  );
};

export default Menubar;
