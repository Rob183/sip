import styled from "styled-components";

export const StyledMenu = styled.nav`
  color: black;
  display: flex;
  flex-direction: column;
  justify-content: center;
  background: rgba(0, 70, 180, 1);
  height: 100%;
  width: 75%;
  text-align: left;
  padding: 2.5em;
  position: absolute;
  top: 0;
  left: 0;
  transition: transform 0.3s ease-in-out;
  transform: ${({ open }) => (open ? "translateX(0)" : "translateX(-100%)")};
  z-index: 2;

  .swipemenu {
    width: 10%;
    height: 100%;
    background: rgba(60, 60, 60, 0);
    position: absolute;
    top: 0;
    left: 95%;
  }
  a {
    font-family: source-sans-pro, sans-serif;
    font-size: 1.8em;
    text-transform: uppercase;
    padding: 1em 0;
    font-weight: normal;
    letter-spacing: 0.5rem;
    color: white;
    text-decoration: none;
    transition: color 0.3s linear;
  }

  .host {
    display: flex;
    flex-direction: column;
  }

  .host a {
    font-size: 1.7em;
    padding: 10px 0 10px 36px;
  }
`;
