import React from "react";
import { bool, func } from "prop-types";
import { StyledMenu } from "./Menu.styled";
import { useSwipeable, Swipeable } from "react-swipeable";
const Menu = ({ open, setOpen }) => {
  return (
    <StyledMenu open={open}>
      <a href="../../../">HOME</a>
      <a href="/Informationen/">INFORMATIONEN</a>
      <div className="host">
        <a href="/Informationen/Alumni"> ALUMNI</a>
        <a href="/Informationen/Exkursionen"> EXKURSIONEN</a>
        <a href="/Informationen/Fakultät"> FAKULTÄT</a>
        <a href="/Informationen/Forschung"> FORSCHUNG</a>
        <a href="/Informationen/Hochschule"> HOCHSCHULE</a>
        <a href="/Informationen/artikel/Institute/Institute.md"> INSTITUTE</a>
        <a href="/Informationen/International"> INTERNATIONAL</a>
        <a href="/Informationen/Kooperationen"> KOOPERATIONEN</a>
        <a href="/Informationen/Lehrbereiche"> LEHRBEREICH</a>
        <a href="/Informationen/Studiengänge"> STUDIENGÄNGE</a>
        <a href="/Informationen/Veranstaltungen"> VERANSTALTUNGEN</a>
        <a href="/Informationen/Wettbewerbe"> WETTBEWERBE</a>
      </div>
      <a href="../../../BIM/">BIM</a>
      <a href="../../../Stundenplan/">Stundenplan</a>
      <a href="/Lageplan">Lageplan</a>
      <a href="../../../">News</a>
      <a href="../../../Hilfe/">Hilfe</a>
      <a href="/Besondere_Abschlussarbeiten/">Besondere Abschlussarbeiten</a>

      <Swipeable
        className="swipemenu"
        onSwipedRight={() => setOpen(true)}
        onSwipedLeft={() => setOpen(false)}
      ></Swipeable>
    </StyledMenu>
  );
};

Menu.propTypes = {
  open: bool.isRequired,
  setOpen: func.isRequired,
};
export default Menu;
