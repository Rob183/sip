import React from "react";
import "./karte.css";
import mapboxgl from "mapbox-gl";
import { Token } from "./Token.js";
//Access Token fuer den Mapbox Account
mapboxgl.accessToken = Token;

class Karte extends React.Component {
  constructor(props) {
    super(props);
    //Initial Zoom und Position der Karte
    this.state = {
      lng: 55,
      lat: 0,
      zoom: 1,
    };
  }
  componentDidMount() {
    try {
      let geojson = JSON.parse(this.props.data);
      const map = new mapboxgl.Map({
        container: this.mapContainer,
        center: [this.state.lng, this.state.lat],
        zoom: this.state.zoom,
      });
      //Setzen des Kartenstyles
      map.setStyle("mapbox://styles/mapbox/light-v10");

      // add markers to map
      geojson.features.forEach(function (marker) {
        // create a HTML element for each feature
        var el = document.createElement("div");
        el.className = "marker";

        // make a marker for each feature and add to the map
        new mapboxgl.Marker(el)
          .setLngLat(marker.geometry.coordinates)
          .setPopup(
            new mapboxgl.Popup({ offset: 25 }) // add popups
              .setHTML(
                //Texte fuer die Popups
                marker.properties.description
              )
          )
          .addTo(map);
      });
    } catch (err) {
      console.log(err);
    }
  }

  render() {
    return (
      <div>
        <div id={"map"} ref={(el) => (this.mapContainer = el)}></div>
      </div>
    );
  }
}

export default Karte;
