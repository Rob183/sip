import React, { Component } from "react";
import "./Clock.css";

class Clock extends Component {
  state = {
    time: new Date()
  };

  componentDidMount() {
    setInterval(() => {
      this.setState({ time: new Date() });
    }, 1000);
  }

  render() {
    const { time } = this.state;

    return (
      <p className="clock">
        {time
          .getHours()
          .toString()
          .padStart(2, "0")}
        :
        {time
          .getMinutes()
          .toString()
          .padStart(2, "0")}
      </p>
    );
  }
}

export default Clock;
