import React, { Component } from "react";
import { Link } from "@reach/router";
import "./Button.css";

class Button extends Component {
  render() {
    if (!this.props.disabled) {
      // Abfrage zur Sichtbarkeit
      return (
        <Link to={this.props.name}>
          <button className="button">
            {this.props.icon}
            {this.props.name}
          </button>
        </Link>
      );
    } else return null;
  }
}
export default Button;
