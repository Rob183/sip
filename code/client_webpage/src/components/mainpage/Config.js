/*  Konfiguration von Komponenten
 *
 *   Zum Verbergen von Komponenten die Variable mit dem passenden Namen auf "true" setzen
 *
 *   Beispiel, Verbergen des Buttons Informationen:
 *       export var Informationen=true;
 *
 *   Um den Button wieder anzuzeigen:
 *       export var Informationen=false;
 *
 */

//  Hauptseite
export var InformationenVersteckt = false;
export var StundenplanVersteckt = false;
export var AbschlussarbeitenVersteckt = false;
export var LageplanVersteckt = false;
export var BIMVersteckt = false;
export var HilfeVersteckt = false;
export var HITVersteckt = true;

export var NewsVersteckt = false;
export var VeranstaltungenVersteckt = false;
