import React, { Component } from "react";
//Import fuer Ueberschrift
import "../text.css";

//Import der Grundbestandteile der Buttons
import { Link } from "@reach/router";
import "./Dashboard.css";
import Button from "@material-ui/core/Button";

// import ButtonNeu from "./Button";

// Import der Icons von https://www.materialui.co/icons
import InformationenIcon from "@material-ui/icons/Info";
import StundenplanIcon from "@material-ui/icons/DateRange";
import AbschlussarbeitenIcon from "@material-ui/icons/Class";
import LageplanIcon from "@material-ui/icons/Explore";
import BIMIcon from "@material-ui/icons/AccountBalance";
import HelpIcon from "@material-ui/icons/Help";
import CodeIcon from "@material-ui/icons/Code";

//Import Konfigurationsvariablen
import { InformationenVersteckt } from "./Config";
import { StundenplanVersteckt } from "./Config";
import { AbschlussarbeitenVersteckt } from "./Config";
import { LageplanVersteckt } from "./Config";
import { BIMVersteckt } from "./Config";
import { HilfeVersteckt } from "./Config";
import { NewsVersteckt } from "./Config";
import { VeranstaltungenVersteckt } from "./Config";
import { HITVersteckt } from "./Config";

//Import FeedList
import FeedList from "../news/FeedList.js";

class Dashboard extends Component {
  //Anzeigeeigenschaften der Buttons + Icons + Parameter disabled
  apps = [
    {
      name: "Informationen",
      icon: <InformationenIcon className="icon" fontSize="large" />,
      disabled: InformationenVersteckt
    },
    {
      name: "Stundenplan",
      icon: <StundenplanIcon className="icon" fontSize="large" />,
      disabled: StundenplanVersteckt
    },
    {
      name: "Besondere_Abschlussarbeiten",
      icon: <AbschlussarbeitenIcon className="icon" fontSize="large" />,
      disabled: AbschlussarbeitenVersteckt
    },
    {
      name: "Lageplan",
      icon: <LageplanIcon className="icon" fontSize="large" />,
      disabled: LageplanVersteckt
    },
    {
      name: "BIM",
      icon: <BIMIcon className="icon" fontSize="large" />,
      disabled: BIMVersteckt
    },
    {
      name: "Hilfe",
      icon: <HelpIcon className="icon" fontSize="large" />,
      disabled: HilfeVersteckt
    },
    {
      name: "HIT",
      icon: <CodeIcon className="icon" fontSize="large" />,
      disabled: HITVersteckt
    }
  ];

  setName(name) {
    name = name.replace("_", " ");
    return name;
  }

  render() {
    return (
      <div>
        <div id="dbueberschrift">Willkommen an der Fakultät Bauwesen!</div>
        <div className="AppButton">
          {//Erzeugung der Buttons mit den Eigenschaften aus vorherigem Array
          this.apps.map((item, i) => {
            if (!item.disabled) {
              //Abfrage zur Sichtbarkeit
              return (
                <Link to={item.name} key={i}>
                  <Button
                    style={{
                      textDecoration: "none",
                      fontSize: "100%",
                      width: "100%",
                      height: "10vh"
                    }}
                    color="primary"
                    variant="contained"
                    fullWidth={true}
                    disableRipple={false}
                  >
                    {item.icon}
                    {this.setName(item.name)}
                  </Button>
                </Link>
              );
            }

            return <div />;
          })}

          {/*zeitweise deaktiviert*/
          /* neuer Button Dummy - Bsp. für 'Informationen'* und 'Stundenplan'
          <ButtonNeu
            name="Informationen"
            icon={<InformationenIcon className="icon" />}
            disabled={InformationenVersteckt}
          />
          <ButtonNeu
            name="Stundenplan"
            icon={<StundenplanIcon className="icon" />}
            disabled={StundenplanVersteckt}
          />
          */}
        </div>
        <div className="widgetkapselung">
          {!NewsVersteckt && (
            <FeedList
              title="News"
              url="http://localhost:5001/api/fb/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
              alias="News"
            />
          )}
          {!VeranstaltungenVersteckt && (
            <FeedList
              title="Veranstaltungen"
              url="http://localhost:5001/api/fb/fakultaet/?type=787&action=rss&tx_htwkenhancements%5Bfeed%5D=48895"
              alias="Veranstaltungen"
            />
          )}
        </div>
      </div>
    );
  }
}
export default Dashboard;
