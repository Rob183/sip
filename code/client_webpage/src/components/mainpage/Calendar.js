import React, { Component } from "react";
import "./Calendar.css";

class Calendar extends Component {
  constructor() {
    super();

    /* Tag - Monat - Jahr */
    var today = new Date(),
      date =
        today.getDate() +
        "." +
        (today.getMonth() + 1) +
        "." +
        today.getFullYear();

    this.state = {
      date: date
    };
  }
  /* render date mit CSS Klasse "calendar" */
  render() {
    return <div className="calendar">{this.state.date}</div>;
  }
}
export default Calendar;
