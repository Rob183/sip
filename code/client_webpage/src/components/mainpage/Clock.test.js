import React from "react";
import { shallow } from "enzyme";
import Clock from "./Clock";

it("renders without crashing", () => {
  shallow(<Clock />);
});

it("has CSS class clock", () => {
	const wrapper = shallow(<Clock />);
	expect(wrapper).toHaveClassName('clock');
});

it("displays time according to state ", () => {
	const wrapper = shallow(<Clock />);
	wrapper.setState({ time : new Date('1995-12-17T03:24:00') });
	expect(wrapper.find("p")).toIncludeText("03:24");
});
