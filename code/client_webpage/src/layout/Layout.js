import React, { useEffect, useState, Component } from "react";
import { navigate, Location } from "@reach/router";

import Breadcrumbs from "../components/Breadcrumbs";

import "./Layout.css";

import { useSwipeable, Swipeable } from "react-swipeable";

// Import der benötigten Assets
import logo from "../assets/HTWK Logo.svg";
import home from "../assets/home_icon.svg";
import back from "../assets/back_icon.svg";

// import Menubar from "../components/Navigation/Nav/Menubar";
import Sidebar from "../components/Navigation/Sidebar/Sidebar";

import WifiIcon from "@material-ui/icons/Wifi";
import WifiOffIcon from "@material-ui/icons/WifiOff";

/*react-interactive-tutorials wird für den Tutorial benoetigt*/

import interactiveTutorials from "react-interactive-tutorials";
import {
  paragraphs,
  registerTutorials,
  startTutorial,
} from "react-interactive-tutorials";

//Tutorial-button
import HelpOutlineIcon from "@material-ui/icons/HelpOutline";

import demo from "./Tutorial.js";

//Import für CheckForInternet
import { Offline, Online } from "react-detect-offline";

import PopUp from "./PopUp.js";
import Popup from "reactjs-popup";

// Import zusätzlicher Komponenten
import UnlockDialog from "../components/unlock/UnlockDialog.js";

function Calendar() {
  const [uhrzeit, setUhrzeit] = useState("00:00");
  const [datum, setDatum] = useState("01.01.1970");

  useEffect(() => {
    const inveralID = setInterval(() => {
      setUhrzeit(new Date().toLocaleTimeString().substring(0, 5));
    }, 1000);

    return () => {
      clearInterval(inveralID);
    };
  }, []);

  useEffect(() => {
    setDatum(new Date().toLocaleDateString().replace(/\//g, "."));
  }, [uhrzeit]);

  return (
    <div>
      <div className="calendar">
        <div>{uhrzeit}</div>
        <div>{datum}</div>
      </div>
    </div>
  );
}

{
  /* Die Funktion für Zeit und Wifi, wenn online */
}

function RenderIfOnline() {
  return (
    <div className="wifi">
      <WifiIcon style={{ fontSize: 55 }} />
    </div>
  );
}

{
  /* Die Funktion für Zeit und Wifi,wenn offline*/
}

function RenderIfOffline() {
  return (
    <Popup
      modal
      trigger={
        <div className="wifioff" width="800px">
          <WifiOffIcon style={{ fontSize: 55 }} />
        </div>
      }
    >
      {(close) => <PopUp close={close} />}
    </Popup>
  );
}

function CheckForInternet() {
  return (
    <div className="wifiwrapper">
      <Online>
        <RenderIfOnline />
      </Online>
      <Offline>
        <RenderIfOffline />
      </Offline>
    </div>
  );
}

function BackButton() {
  const [active, setActive] = useState(false);

  return (
    <>
      <Location>
        {({ location }) => {
          if (location.pathname !== "/") {
            setActive(true);
          } else {
            setActive(false);
          }
        }}
      </Location>
      <div class="back-button">
        <img
          className="back"
          alt="back"
          src={back}
          style={{ opacity: active ? "1" : "0.5" }}
          onClick={() => {
            if (active) window.history.back();
          }}
        />
      </div>
    </>
  );
}

export default class Layout extends Component {
  constructor(props) {
    super(props);
    /* Needed to toggle unlocking dialog */
    this.state = { isUnlocking: false };
    this.viewUnlockDialog = this.viewUnlockDialog.bind(this);
    this.hideUnlockDialog = this.hideUnlockDialog.bind(this);
  }

  viewUnlockDialog() {
    this.setState({ isUnlocking: true });
  }

  hideUnlockDialog() {
    this.setState({ isUnlocking: false });
  }

  render() {
    return (
      <div className="wrapper">
        <nav>
          <Sidebar />
          <BackButton />

          {/* <Clock /> */}
          {/* <div className="title">Hephaistos</div> */}
          <Breadcrumbs />

          <img
            className="home"
            alt="Home"
            src={home}
            onClick={() => navigate("/")}
          />
        </nav>
        <main>{this.props.children}</main>

        {/* Uhrzeit und Datum immer am Fußende der Seite */}
        <footer>
          {this.state.isUnlocking ? (
            <div>
              <img
                className="logo"
                src={logo}
                alt="Logo"
                onClick={this.hideUnlockDialog}
              />
              <UnlockDialog getBackAction={this.hideUnlockDialog} />
            </div>
          ) : (
            <img
              className="logo"
              src={logo}
              alt="Logo"
              onClick={this.viewUnlockDialog}
            />
          )}
          <Swipeable
            className="touch"
            onSwipedUp={() => this.viewUnlockDialog()}
          ></Swipeable>
          <div className="icons">
            <div class="tutorial">
              <HelpOutlineIcon
                style={{ fontSize: 55 }}
                onClick={() => startTutorial("demo")}
              >
                {" "}
                Tutorial
              </HelpOutlineIcon>
            </div>
            <CheckForInternet />
            <Calendar />
          </div>
        </footer>
      </div>
    );
  }
}
