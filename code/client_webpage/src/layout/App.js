import React, { Component } from "react";
import { Router } from "@reach/router";
import { MuiThemeProvider, createMuiTheme } from "@material-ui/core/styles";

import "./App.css";

//Import der Components mit zugeordneter JS Datei, in welcher Components definiert sind
import Layout from "./Layout";
import Dashboard from "../components/mainpage/Dashboard";
//remove: neue Kategorien hinzugefügt @richard
import ContentNav from "../components/ContentNav.js";
import Hilfe from "../components/Hilfe";
import HIT from "../components/HIT";
import Lageplan from "../components/Lageplan";
import BIM from "../components/BIM/BIM.js";
import Stundenplan from "../components/Stundenplan/Stundenplan.js";
import FeedArticle from "../components/news/FeedArticle";
import Abschlussarbeiten from "../components/abschlussarbeiten/Abschlussarbeiten.js"

//Eigenschaften des Themes
const theme = createMuiTheme({
  palette: {
    primary: { main: "#00338D" }
  }
});

class App extends Component {
  render() {
    return (
      <MuiThemeProvider theme={theme}>
        {/*vergibt das Theme an alle folgenden Komponenten*/}
        <div className="App">
          <Layout>
            {/*Using aller Seiten und Komponenten KEINE LEERZEICHEN :/*/}
            {/*Component erhaelt Dateipfad im Projekt*/}
            <Router>
              <Dashboard path="/" />
              <ContentNav path="Informationen/*" />
              <Hilfe path="Hilfe/" />
              <HIT path="HIT/" />
              <Lageplan path="/Lageplan/" />
              <BIM path="BIM/*" />
              <FeedArticle path="aktuelles/:alias/:guid" />
              <Stundenplan path="Stundenplan/" />
              <Abschlussarbeiten path="Besondere_Abschlussarbeiten/*" />
            </Router>
          </Layout>
        </div>
      </MuiThemeProvider>
    );
  }
}

export default App;
