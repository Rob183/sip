import React from "react";
import "./PopUp.css";

export default ({ close }) => (
  <div className="modal">
    <a className="close" onClick={close}>
      &times; {/* close-Zeichen */}
    </a>
    <div className="header"> &nbsp; </div>
    {/* &nbsp -Leerzeichen */}
    <div className="content">
      Kein Netzwerk vorhanden.
      <br />
      Bitte versuchen Sie es später nochmal...
    </div>
  </div>
);
