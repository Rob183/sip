//import "./Layout.css";
import "./Tutorial.css";

import interactiveTutorials from "react-interactive-tutorials";
import {
  paragraphs,
  registerTutorials,
  startTutorial,
} from "react-interactive-tutorials";

/* Das Tutorial ist dafür geeignet, um sich vertraut mit den wichtigesten Elementen der Stele zu machen. Um das Tutorial zu starten, muss der Benutzer auf "Fragezeichen-Knopf" klicken, der sich neben dem Wifi-Knopf unten rechts auf dem Bildschirm befindet.
Die meisten Funktionalitäten sind in Package "react-interactive-tutorials eingebaut.*/

const TUTS = {
  demo: {
    key: "demo",
    title: "Einführung",
    steps: [
      {
        key: "anywhere",
        announce: paragraphs`
            
             Kommen Sie nicht mit der Stele zurecht? Dann könnte dieses Tutorial nützlich für Sie sein!  
             Das Schritt-für-Schritt Tutorial macht Sie mit der Navigation und den wichtigsten Elementen der Stele vertraut.
  
             Die wichtigsten Icons und Elemente werden mit der weißen Hintergrundfarbe markiert.
  
             Sie können jederzeit das Tutorial beenden, indem Sie links unten auf das Icon "Exit Tutorial" klicken.
  
             Wenn Sie kurz die Information sowie den schwarzen Hintergrund ausblenden möchten, klicken Sie bitte auf "Hide Help".
             Möchten Sie wieder in das Tutorial gelangen, klicken Sie bitte auf "Show Help".
  
             Wollen Sie mit dem Tutorial anfangen, klicken Sie bitte auf "Tutorial starten".
          `,
        announceDismiss: "Tutorial starten",
        activeWhen: [],
      },
      {
        key: "home",
        highlight: ".home",
        highlightBack: "#fff",
        announce: paragraphs`
            
          Schauen wir uns zuerst das Home-Icon ganz oben in der rechen Ecke des Bildschirmes an.
          Egal wo Sie sich gerade befinden, beim Anklicken dieses Icons können Sie jederzeit auf die Homepage-Seite gelanden. 
  
            
          `,
        announceDismiss: "Weiter",
        activeWhen: [
          {
            compare: "checkpointComplete",
            checkpoint: "demo_anywhere",
          },
        ],
      },
      {
        key: "wifi",
        highlight: ".wifi",
        highlightBack: "#fff",
        announce: paragraphs`
            Das Icon "WiFi" oder WLAN zeigt immer, ob die Stele eine Netzwerkverbindung hat oder nicht. Das ist relevant bei dem Zugriff auf Online-Ressourcen. 
  
            Hat die Stele einen Zugang zum Internet, ist das Icon Schwarz-Weiß. Dazu kann es NICHT angeklickt werden.
  
            Ist bei der Stele kein Netzwerk vorhanden, wird das Icon in ROTER Farbe dargestellt und beim Anklicken erscheint eine Meldung mit genaueren Informationen. 
  
           
          `,
        announceDismiss: "Weiter",
        activeWhen: [
          {
            compare: "checkpointComplete",
            checkpoint: "demo_home",
          },
        ],
      },
      {
        key: "back",
        highlight: ".back-button",
        highlightBack: "#fff",
        announce: paragraphs`
             Das Back-Icon ganz oben links gibt Ihnen die Möglichkeit, die vorherige Seite zu laden. 
  
             Befinden Sie sich auf der Homepage-Seite, ist das Icon nicht anklickbar.
          `,
        announceDismiss: "Weiter",
        activeWhen: [
          {
            compare: "checkpointComplete",
            checkpoint: "demo_wifi",
          },
        ],
      },
      {
        key: "news",
        highlight: ".widgetkapselung2:first-child",
        highlightBack: "#fff",
        annotate: paragraphs`
            Hier finden Sie die aktuellen Neuigkeiten der Maschinenbau-Fakultät sowie der Hochschule im Allgemeinen.
  
            
          `,

        annotateAfter: "#dbueberschrift",
        annotateSkip: "Weiter",
        activeWhen: [
          {
            compare: "checkpointComplete",
            checkpoint: "demo_back",
          },
        ],
      },
      {
        key: "info",
        highlight: ".AppButton",
        highlightBack: "#fff",
        annotate: paragraphs`
            Neben den aktuellen Neuigkeiten und Veranstaltungen teilt sich die Stele in sechs Bereiche: Informationen, Stundenplan, Lageplan, BIM, besondere Abschlussarbeiten und Hilfe.
            Alle Bereiche sind anklickbar.
      
          `,

        annotateAfter: ".AppButton",
        annotateSkip: "Weiter",
        activeWhen: [
          {
            compare: "checkpointComplete",
            checkpoint: "demo_news",
          },
        ],
      },
    ],
    complete: {
      on: "checkpointReached",
      checkpoint: "info",
      title: "Das Tutorial ist beendet.",
      message: paragraphs`
          Danke für die Benutzung des Tutorials. Wir hoffen es war hilfreich.
  
          Falls Sie weitere Fragen haben, können Sie auf das Icon "Hilfe" klicken.
  
          BEMERKUNG: Um auf die Homepage-Seite zu gelangen, klicken Sie bitte auf "Complete". Die anderen Icons sind nicht anklickbar.
        `,
    },
  },
};

registerTutorials(TUTS);
export default "demo";
