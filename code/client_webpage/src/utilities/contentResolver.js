function resolve() {
  let cache = {};

  function importAll(r) {
    r.keys().forEach((key) => (cache[key] = r(key)));
  }

  // require.context nicht testbar
  importAll(require.context("../../public/content", true, /\.md$/i));
  importAll(require.context("../../public/content", true, /\.geojson$/i));
  return cache;
}

// Regex bezieht den Titel des Markdowndokuments
function parseTitle(text, url) {
  const regex = /^#{1,6}\s[^\n]+/m;
  const regexResult = text.match(regex);

  const regexTitle = regexResult && regexResult.length ? regexResult[0] : "";

  if (regexTitle.length > 0) {
    return regexTitle.slice(regexTitle.lastIndexOf("#") + 1).trim();
  } else if (url.includes(".geojson")) {
    return url.slice(14, url.indexOf("."));
  } else {
    return url + " :: FEHLER DER MARKDOWNDATEI";
  }
}

async function fetchContent(url) {
  const content = {};
  const text = await (await fetch(url)).text();
  // const titelRegex = text.match(/(?<=#{1,6}\s)[^\n]+/);
  content.titel = parseTitle(text, url);
  content.text = text;
  return content;
}

// Inspiriert durch http://brandonclapp.com/arranging-an-array-of-flat-paths-into-a-json-tree-like-structure/
// Findet first occurrence value in array
function findWhere(array, key, value) {
  let t = 0;
  while (t < array.length && array[t][key] !== value) {
    t++;
  }

  if (t < array.length) {
    return array[t];
  } else {
    return false;
  }
}

async function buildTree(pathContent) {
  let tree = [];
  const paths = Object.keys(pathContent);

  for (const el of paths) {
    let path = el.split("/");
    let level = tree;
    for (const part of path) {
      let existingPath = findWhere(level, "name", part);

      if (existingPath) {
        level = existingPath.children;
      } else {
        // Check if file or directory
        if (path[path.length - 1] === part) {
          const tempContent = await fetchContent(pathContent[el]);
          const node = {
            name: part,
            url: pathContent[el],
            titel: tempContent.titel,
            text: tempContent.text,
          };

          level.push(node);
        } else {
          const node = {
            name: part,
            children: [],
          };

          level.push(node);
          level = node.children;
        }
      }
    }
  }
  return tree;
}

let cacheTree;
export default async function getContentMarkdown() {
  // Use cached response if available
  if (!cacheTree) {
    const contentFlat = resolve();
    cacheTree = await buildTree(contentFlat);
  }
  return cacheTree;
}

export { buildTree, findWhere, fetchContent };
