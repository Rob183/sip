import { findWhere, buildTree, fetchContent } from "./contentResolver";

describe("findWhere", () => {
  const array = [
    {
      name: "a"
    },
    {
      name: "b"
    },
    {
      name: "c"
    },
    {
      name: "d"
    }
  ];

  test("returns false when not in array", () => {
    // f is not in array
    expect(findWhere(array, "name", "f")).toBeFalsy();
  });

  test("returns object to query", () => {
    expect(findWhere(array, "name", "a")).toEqual({ name: "a" });
  });
});

const testPaths = {
  "./Informationen/Alumni/absolventenkarriere.md":
    "/static/media/absolventenkarriere.5dd38f5e.md",
  "./Informationen/Alumni/absolventenmeinungen.md":
    "/static/media/absolventenmeinungen.8f0d6b0f.md",
  "./Informationen/Alumni/alumni.md": "/static/media/alumni.e24c838b.md",
  "./Informationen/Exkursionen/exkursionen1.md":
    "/static/media/exkursionen1.99c8c922.md",
  "./Informationen/Kooperationen/praxisprtnerIFBT.md":
    "/static/media/praxisprtnerIFBT.8aae8396.md",
  "./Informationen/Kooperationen/praxisprtnerIFBT2.md":
    "/static/media/praxisprtnerIFBT2.66ed36da.md",
  "./Informationen/Kooperationen/sahlmann&PartnerGbR.md":
    "/static/media/sahlmann&PartnerGbR.528c6624.md",
  "./Informationen/Kooperationen/sahlmannGebÑudetechnikGmbH.md":
    "/static/media/sahlmannGebÑudetechnikGmbH.7051d9cf.md",
  "./Informationen/Kooperationen/sarahKnechtges.md":
    "/static/media/sarahKnechtges.e8cfc4a8.md",
  "./Informationen/Kooperationen/thomasCebulla.md":
    "/static/media/thomasCebulla.455de3ac.md",
  "./Informationen/Kooperationen/thomasKuehnert.md":
    "/static/media/thomasKuehnert.ab23b1d3.md"
};

// TODO: fetch needs to be mocked.
// test("buildTree retruns tree", async () => {
//   expect(JSON.stringify(await buildTree(testPaths))).toMatchSnapshot();
// });

describe("getContentMarkdown", () => {
  test("fetches content and gets title from markdown", async () => {
    // Mock fetch API
    // eslint-disable-next-line no-native-reassign
    fetch = jest.fn().mockImplementation(() =>
      Promise.resolve({
        text: () =>
          Promise.resolve(`
![bild44](ifbt.jpg)

## Praxispartner - IFBT GmbH

• Jobs und Praktika
• Thomas Kühnert
• Marcel Wolter`)
      })
    );

    expect(
      await fetchContent("http://localhost:3000/markdowntest")
    ).toMatchSnapshot();

    expect(fetch).toHaveBeenCalledWith("http://localhost:3000/markdowntest");
  });
});
