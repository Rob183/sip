### Building Information Modeling

Arbeitsmethode für die Erstellung und Verwaltung von Informationen in Bauprojekten auf der Grundlage klar definierter Arbeitsaufgaben, Datenmodelle und Kommunikationsschnittstellen. Digitale Datenmodelle, welche die jeweiligen Planungsstände der einzelnen Disziplinen zusammenführen, dienen hierbei als ein Werkzeug für die Koordination der unterschiedlichen Projektinformationen.

### Open BIM

Open BIM ist ein universeller Kooperationsansatz im Bereich Entwurf, Bau und Betrieb von Gebäuden, basierend auf offenen Standards und Arbeitsabläufen. Open BIM ist eine Initiative von mehreren führenden Softwareanbietern unter Verwendung des offenen buildingSMART-Datenmodells.

Das einzige Ziel der Open-BIM-Bewegung ist es, offene Kooperationsabläufe für besser koordinierte Projekte zu fördern. Dieses Ziel wird in erster Linie durch ein weltweit gemeinsames, öffentlich realisierbares Open-Bim-Branding erreicht, das durch klare Definitionen, spezifische Anforderungen und Best Practices zur Unterstützung der Umsetzung gefördert wird.

Der mit OpenBIM verfolgte Ansatz der modellbasierten Zusammenarbeit fördert offene Kooperationsprozesse, bei denen komplexe Firmen- und Projektstrukturen in vollem Umfang anerkannt werden. Dieser Ansatz hebt das Thema von der Datenebene auf die Workflowebene und macht Daten zu dem, was sie sind: ein Medium oder Instrument für hochwertige Informationen. Dieser Ansatz erlaubt es, die Projektbeteiligten aufgrund ihrer Fachkompetenz auszuwählen und nicht deshalb, weil sie eine bestimmte Software benutzen.

### Möglichkeitsraum

Der „Möglichkeitsraum“ ist angelehnt an den Roman von Robert Musil „Der Mann ohne Eigenschaften“ und seinem darin formulierten Gedanken zum „Möglichkeitssinn“:

„Wenn es Wirklichkeitssinn gibt, muss es auch Möglichkeitssinn geben. ... Wer ihn besitzt, sagt beispielsweise nicht: Hier ist dies oder das geschehen, wird geschehen, muss geschehen; sondern er erfindet: Hier könnte, sollte, müsste geschehen; und wenn man ihm von irgendetwas erklärt, dass es so sei, wie es sei, dann denkt er: Nun es könnte wahrscheinlich auch anders sein. So ließe sich der Möglichkeitssinn geradezu als die Fähigkeit definieren, alles, was ebenso gut sein könnte, zu denken und das, was ist, nicht wichtiger zu nehmen, als das, was nicht ist.“

Diesen Gedanken aufgreifend, findet sich aus Überzeugung des Autors im „Möglichkeitsraum“ eine ureigene Qualität von Forschung und Wissenschaft wieder.

Unter Pädagogen wird der „Möglichkeitsraum“ zudem häufig verwendet, beispielsweise auch im Zusammenhang mit Paradigmen und Paradigmenwechsel.

_Timo Kretschmer, 2015_
