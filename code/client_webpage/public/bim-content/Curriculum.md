## BIM Curriculum

### BIM-Qualifikation Basiskenntnisse

1. **BIM Genese** \[Entstehung, Bedeutung, Terminologie, Ausprägung]  
   Ähnlich einer Einführung werden in diesem Kapitel die Entstehungsgeschichte, die Protagonisten, die Motivation und Begrifflichkeiten gelehrt. Den Studierenden wird BIM als konsequente Weiterentwicklung hin zum integrierten Planen vorgestellt. Die Faktoren, die zur Entwicklung von BIM motiviert haben, werden dabei eingehend beschrieben. Dabei ist es erforderlich, den Studierenden die BIM-Terminologie kenntlich zu machen. BIM wird definiert und Begrifflichkeiten aus dem BIM-Kontext werden hergeleitet und erläutert.

2. **BIM Vision** \[Chancen der Digitalisierung, Interoperabilität, Rahmenbedingungen]  
   In diesem Kapitel werden die Vorteile, die man sich durch die BIM Methode gegenüber der bisherigen Projektabwicklung verspricht erörtert und vorgestellt. Zudem werden die Rahmenbedingungen im Zuge der Digitalisierung der Baubranche diskutiert und politische Absichten und internationale Tendenzen vermittelt. Kollaboration, Datenmanagement und Standardisierung bilden hierbei die Hauptpfeiler.

3. **BIM Data** \[Wer, Wann, Was, Wie, Wo]  
   Dieser große Themenkomplex beinhaltet alle wesentlichen Elemente, die durch Einführung der BIM Methode und deren erfolgreiche Anwendung erforderlich sind. Hier werden die einzelnen Rollen beschrieben und resultierende Mehrwerte gekennzeichnet. Prozesse und Prozessanforderungen werden dargestellt. Die Implementierung der BIM Methode wird erläutert und Anwendungsfälle beschrieben.

4. **BIM Standards** \[bS, IFC, openBIM, MVD, IDM, bSDD, BCF]  
   Die BIM Standards werden umfassend vorgestellt, da sie sowohl den Arbeitsprozess, als auch die Qualität der Planung und Fortschreibung der Modelldaten wesentlich beeinflussen. Die Studierenden müssen ein grundlegendes Verständnis für die internationalen Standards wie IFC und die Abhängigkeiten und Möglichkeiten von MVD, IDM, AIA und dgl. entwickeln.

5. **BIM Change-Management** \[Hemmnisse, Chancen, Risiken]  
   Auf Basis der zuvor erworbenen Kenntnisse werden der Strukturwandel und das Potenzial einer Prozessänderung diskutiert. Die Studierenden werden befähigt, vorhandene Strukturen zu bewerten und Wege zur Einführung von BIM aufzuzeigen. Hier werden neben den technischen Voraussetzungen insbesondere unternehmerische Ziele hinterfragt und bewertet. Die Studierenden sollen den Ist-Zustand der Marktteilnahme und den Soll-Zustand beschreiben und erforderliche Schritte qualifiziert aufzeigen können.

6. **BIM International** \[Normen, nationale Standards, Vertragssysteme, int. Institutionen]  
   Dieser Themenblock beschäftigt sich mit den geltenden Normen, nationalen Besonderheiten und vertraglichen Regelungen als Basis für bim-konforme Arbeitsprozesse. Die Abgrenzung zwischen internationalen und nationalen Begebenheiten und Abhängigkeiten, vertragliche Regelungen hinsichtlich Qualitätssicherung, Honorierung und Rechtssicherheit (Urheberschaft, Zielvereinbarungen) stehen hierbei im Mittelpunkt. Zudem werden internationale Institutionen vorgestellt, die länderübergreifend das BIM-Geschehen beeinflussen.

7. **BIM "hands on"** \[Aus CAD wird BIM-konformes Modellieren. Anwendungsbeispiele]  
   Für eine Hochschule für angewandte Wissenschaften ist es unabdingbar, die Studierenden über die praktische Anwendung das erworbene Wissen vertiefen zu lassen. In diesen Anwendungsbeispielen wird das Theoretische mit dem Praktischen vereint und das Lernen durch Handeln ermöglicht. Dabei werden sämtliche Inhalte der vorherigen Kapitel eingebunden und am Fallbeispiel hinsichtlich Einfluss und Auswirkung hinterfragt.

8. **BIM "hands on"** \[Aus CAD wird BIM-konformes Modellieren. Anwendungsbeispiele]\*  
   \* Lern-Studio \[Fragen/Antworten während der Projektbearbeitung]
