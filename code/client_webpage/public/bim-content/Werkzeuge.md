## BIM-Werkzeuge

Im Rahmen unserer Lehrveranstaltungen kommen unterschiedlichste Werkzeuge zum Einsatz, die wir Ihnen kurz vorstellen möchten:

| Name                    | Einsatzbereich                 | Einsatzziel                                             | Link                                                                                    |
| ----------------------- | ------------------------------ | ------------------------------------------------------- | --------------------------------------------------------------------------------------- |
| ArchiCAD inkl. BIMcloud | (Architektur-)Planung          | Architekturmodell, Koordinationsmodell                  | [Website](https://www.graphisoft.de/)                                                   |
| Allplan                 | (Tragwerks-)Planung            | Tragwerksmodell                                         | [Website](https://www.allplan.com/de/)                                                  |
| bimcollab               | BCF-Plattform                  | Issue-Management                                        | [Website](https://www.bimcollab.com/de/default)                                         |
| BECHMANN BIM/AVA        | Kalkulation                    | Kosten- und Mengenermittlung                            | [Website](https://bechmann-software.de/products/bechmann-bim/)                          |
| ETU-Planer              | Energetische Gebäudesimulation | Berechnung und Simulation                               | [Website](https://www.hottgenroth.de/M/SOFTWARE/ETU-Planer/Seite.html,73257,80409)      |
| Orca Ava                | Kalkulation                    | Kosten- und Mengenermittlung                            | [Website](https://www.orca-software.com/orca-ava/)                                      |
| RFEM                    | Tragwerk                       | FEM-Statik                                              | [Website](https://www.dlubal.com/de/produkte/fem-statik-software-rfem/was-ist-rfem)     |
| Revit MEP               | Haustechnik                    | TGA-Modell                                              | [Website](https://www.autodesk.de/products/revit/mep)                                   |
| SolarComputer           | Haustechnik                    | Gebäudeberechnungen                                     | [Website](https://www.solar-computer.de/index.php?seite=BIM&sub=SOLAR-COMPUTER-BIM-TGA) |
| SimpleBIM               | IFC-Datenaustausch             | Qualitätsprüfung und Modelloptimierung                  | [Website](http://www.datacubist.com/benefits/)                                          |
| Solibri Model Checker   | IFC-Datenaustausch             | Qualitätsprüfung und Modelloptimierung und Regelprüfung | [Website](https://www.solibri.com/de/)                                                  |
