## Zertifizierung BIM-Basis

BuildingSMART International bietet mit dem Professional Certification Program einen weltweit gültigen Qualitätsmaßstab für die Bewertung und Vergleichbarkeit von Kenntnissen und Kompetenzen im Bereich Building Information Modeling.

Die HTWK Leipzig wurde als erste Fachhochschule Deutschlands (16.10.2018) als zertifizierter Weiterbildungsanbieter gelistet. Im Unterschied zu externen Weiterbildungsanbietern bietet die HTWK Leipzig Studierenden in bestimmten Modulen an den Fakultäten Architektur und Sozialwissenschaften, Bauingenieurwesen und Maschinen und Energietechnik die Möglichkeit, im Rahmen der Lehre die erforderlichen Kenntnisse für die BIM Basis Qualifikation zu erwerben.

In einer externen Prüfung über buildingSMART International kann das international anerkannte **buildingSMART/VDI Zertifikat BIM-Qualifikationen - Basiskenntnisse** gegen Entrichtung einer Prüfungsgebühr erlangt werden.

Einzelheiten zum Procedere werden in den entsprechenden Modulen innerhalb der Lehrveranstaltungen der Fakultäten bekanntgegeben.

Am 23.10.2018 wurde den Verantwortlichen der HTWK Leipzig (Prof. Monica Rossi, Prof. Henning Rambow, Timo Kretschmer M.A.) im Rahmen der Mitgliederversammlung der buildingSMART Deutschland die Listungsurkunde feierlich überreicht.

Weitere Informationen erhalten Sie über die genannten Ansprechpartner.

![image1](/bim-content/zertifizierungBuildingSmart.png#z#g)
