# Dashboard

    import React, { Component } from "react";
    // import der Grundbestandteile der Buttons
    import { Link } from "@reach/router";
    import "./AppButton.css";
    import Button from "@material-ui/core/Button";
    // import ButtonNeu from "./Button";
    // import der Icons von https://www.materialui.co/icons
    import InformationenIcon from "@material-ui/icons/Info";
    import StundenplanIcon from "@material-ui/icons/DateRange";
    import AbschlussarbeitenIcon from "@material-ui/icons/Class";
    import LageplanIcon from "@material-ui/icons/Explore";
    import BIMIcon from "@material-ui/icons/AccountBalance";
    import HelpIcon from "@material-ui/icons/Help";
    import News from "./News";
    // import Konfigurationsvariablen
    import { InformationenVersteckt } from "./Config";
    import { StundenplanVersteckt } from "./Config";
    import { AbschlussarbeitenVersteckt } from "./Config";
    import { LageplanVersteckt } from "./Config";
    import { BIMVersteckt } from "./Config";
    import { HilfeVersteckt } from "./Config";
    import { NewsVersteckt } from "./Config";
    import { VeranstaltungenVersteckt } from "./Config";
    class Dashboard extends Component {
      //Anzeigeeigenschaften der Buttons + Icons + Parameter disabled
      apps = [
        {
          name: "Informationen",
          icon: <InformationenIcon className="icon" fontSize="large" />,
          disabled: InformationenVersteckt
        },
        {
          name: "Stundenplan",
          icon: <StundenplanIcon className="icon" fontSize="large" />,
          disabled: StundenplanVersteckt
        },
        {
          name: "Abschlussarbeiten",
          icon: <AbschlussarbeitenIcon className="icon" fontSize="large" />,
          disabled: AbschlussarbeitenVersteckt
        },
        {
          name: "Lageplan",
          icon: <LageplanIcon className="icon" fontSize="large" />,
          disabled: LageplanVersteckt
        },
        {
          name: "BIM",
          icon: <BIMIcon className="icon" fontSize="large" />,
          disabled: BIMVersteckt
        },
        {
          name: "Hilfe",
          icon: <HelpIcon className="icon" fontSize="large" />,
          disabled: HilfeVersteckt
        }
      ];
      render() {
        return (
          <div className="AppButton">
            {//Erzeugung der Buttons mit den Eigenschaften aus vorherigem Array
            this.apps.map((item, i) => {
              if (!item.disabled) {
                //Abfrage zur Sichtbarkeit
                return (
                  <Link to={item.name} key={i}>
                    <Button
                      style={{ fontSize: "100%", width: "100%", height: "10vh" }}
                      color="primary"
                      variant="contained"
                      fullWidth={true}
                      disableRipple={false}>
                      {item.icon}
                      {item.name}
                    </Button>
                  </Link>
                );
              }
              return <div />;
            })}
          {/*zeitweise deaktiviert*
            /* neuer Button Dummy - Bsp. für 'Informationen'* und 'Stundenplan'
            <ButtonNeu
              name="Informationen"
              icon={<InformationenIcon className="icon" />}
              disabled={InformationenVersteckt}
            />
            <ButtonNeu
              name="Stundenplan"
              icon={<StundenplanIcon className="icon" />}
              disabled={StundenplanVersteckt}
            />
          */}
            <div className="widget" aps="News" hidden={NewsVersteckt}>
              {/* Dummy News - Links von fb.htwk-leipzig.de/fakultaet/ */}
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Am Puls der Zeit"
                date="3. April 2019"
                preview="Vor wenigen Monaten wurde die HTWK Leipzig als eine der ersten Hochschulen..."
              />
              <br />
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Tagung Betonbauteile - erneut ein Erfolg"
                date="25. März 2019"
                preview="13. Tagung Betonbauteile..."
              />
              <br />
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Neue Entwicklung im Betonbau"
                date="18. März 2019"
                preview="Betonexperten diskutieren am 21. März an der HTWK Leipzig über Innovationen..."
              />
              <br />
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Feierliche Graduierung an der Fakultät Bauwesen"
                date="8. Februar 2019"
                preview="22 Absolventen der Studiengänge des Bauingenieurwesens erhielten Mitte Januar..."
              />
              <br />

              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Am Puls der Zeit"
                date="3. April 2019"
                preview="Vor wenigen Monaten wurden die HTWK Leipzig als eine der ersten Hochschulen..."
              />

              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Am Puls der Zeit"
                date="3. April 2019"
                preview="Vor wenigen Monaten wurden die HTWK Leipzig als eine der ersten Hochschulen..."
              />
            </div>
            <!-- {/* Dummy Veranstaltungen - Links von fb.htwk-leipzig.de/fakultaet/ */} -->
            <div className="widget" aps="Veranstaltungen" hidden={VeranstaltungenVersteckt}>
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&action=rss&tx_htwkenhancements%5Bfeed%5D=48895"
                name="Am Puls der Zeit"
                date="3. April 2019"
                preview="Vor wenigen Monaten wurde die HTWK Leipzig als eine der ersten Hochschulen..."
              />
              <br />
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Tagung Betonbauteile - erneut ein Erfolg"
                date="25. März 2019"
                preview="13. Tagung Betonbauteile..."
              />
              <br />
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Neue Entwicklung im Betonbau"
                date="18. März 2019"
                preview="Betonexperten diskutieren am 21. März an der HTWK Leipzig über Innovationen..."
              />
              <br />
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Feierliche Graduierung an der Fakultät Bauwesen"
                date="8. Februar 2019"
                preview="22 Absolventen der Studiengänge des Bauingenieurwesens erhielten Mitte Januar..."
              />
              <br />
              <News
                link="https://fb.htwk-leipzig.de/fakultaet/?type=787&tx_news_pi1%5Bversion%5D=rss&tx_htwkenhancements%5Bfeed%5D=48894"
                name="Am Puls der Zeit"
                date="3. April 2019"
                preview="Vor wenigen Monaten wurden die HTWK Leipzig als eine der ersten Hochschulen..."
              />
            </div>
            <div className="fade" />
          </div>
        );
      }
    }

export default Dashboard;
