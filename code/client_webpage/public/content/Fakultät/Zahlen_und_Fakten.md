## Zahlen und Fakten

Personal:

- ca. 26 Professuren

- ca. 22 Mitarbeiter

- ca. 50 Drittmittelbeschäftigte

Studierende:

- ca. 1.350 Studierende gesamt

- ca. 350 Immatrikulationen

- ca. 250 Graduierungen

Haushalt:

- ca. 145 T. Euro gesamt

- ca. 72,5 T. Euro Haushalt

- ca. 72,5 T. Euro leistungsbezogene Mittel

- ca. 2 Mio. Euro Drittmittel

![image](/content/Fakultät/zahlen.png#z#g)
