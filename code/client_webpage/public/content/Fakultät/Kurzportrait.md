## Kurzportrait

![image](/content/Fakultät/gebaeude.png#z#g)

Basierend auf einer langjährigen Tradition versteht sich die heutige Fakultät Bauwesen als in der Lehre anerkannte und zugleich forschungsstarke Einrichtung, die die zentrale Bildungsstätte für eine anwendungsorientierte wissenschaftliche Bauausbildung in Mitteldeutschland darstellt.

Die Ausrichtung des Studiengänge im Bauingenieurwesen ist berufsqualifizierend, praxisorientiert und anwendungsbezogen. In ihnen spiegeln sich darüber hinaus die Forschungsschwerpunkte der Fakultät und das Humboldt’sche Ideal der Einheit von Lehre und Forschung wider.
