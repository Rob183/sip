## Zahlen und Fakten

### Fakultäten:

![image](/content/Hochschule/zahlenFakten.png#z#g)

### Personal:

ca. 180 Professuren
ca. 210 Mitarbeiter
ca. 250 Drittmittelbeschäftigte

### Studierende:

ca. 6.100 Studierende gesamt
ca. 1.650 Immatrikulationen
ca. 1.300 Graduierungen

### Haushalt:

ca. 44 Mio. Euro gesamt
ca. 34 Mio. Euro Landesmittel
ca. 10 Mio. Euro Drittmittel
