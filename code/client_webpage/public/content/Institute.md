## Institute

- IBB Institut für Bauwirtschaft und Baubetriebswesen/Institute for Construction Management and Construction Operations

- IFBFM Institut für Bio-Fluidmechanik

- IFEM Institut für experimentelle Mechanik

- IGV Institut für Grundbau und Verkehrsbau

- IHBB Institut für Hochbau, Baukonstruktion und Bauphysik

- IWS Institut für Wasserbau und Siedlungswasserwirtschaft

- IfB Institut für Betonbau

- G² Gruppe Geotechnik

- I4S Institut für Statik, Strukturdynamik, Systemidentifikation und Simulation
