## Forum Bau

Mit bis zu vier Fachvorträgen pro Semester - in der Regel mittwochs ab 18 Uhr - lädt das FORUM BAU an der HTWK Leipzig Interessenten ein. Meist steht regionale Bau- oder Sanierungstätigkeit im Fokus: „Wenn es sich um bekannte Leipziger Bauwerke handelt, verzeichnen wir ein überdurchschnittliches Interesse beim Publikum“, begründet der verantwortliche Professor Thomas Jahn die Schwerpunktsetzung der Reihe. Doch auch ungewöhnliche Themen (bspw. Wiederaufbauprojekte in Nepal) als auch Berichte zu allgegenwärtigen Baumaßnahmen (bspw. Rückbau von Kernkraftwerken) stehen auf der Agenda.

_Beim Besuch von fünf Veranstaltungen wird die Teilnahme durch die Architektenkammer Sachsen als Weiterbildung anerkannt. Der Eintritt ist zu allen Vorträgen frei._

_Mit freundlicher Unterstützung durch den Förderverein der HTWK Leipzig_
