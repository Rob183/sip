## Tagungen und Konferenzen

![image](/content/Veranstaltungen/Beton_2019.png#r#k)

- Fachtagung Betonbauteile

- Leipziger Kolloquium für Wasserwirtschaft, Wasserbau und Siedlungswasserwirtschaft

- Erdbaufachtagung

- Deponiefachtagung

- Mitteldeutsches Asphaltseminar

- Fachtagung Energie- und Gebäudetechnik
  (in Verantwortung der Fakultät Maschinenbau
  und Energietechnik)

- Leipziger Fassadentag

&nbsp;

&nbsp;

![image](/content/Veranstaltungen/Fassadentag_2019.png#r#g)
