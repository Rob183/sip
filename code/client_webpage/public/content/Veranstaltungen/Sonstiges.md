## Sonstiges

### Geotechnikseminar

Das Geotechnikseminar der G² Gruppe Geotechnik an der HTWK Leipzig soll den Austausch in Forschung und Lehre zwischen der HTWK Leipzig und der Baupraxis fördern. Deshalb bilden interessante Bauvorhaben, Entwicklungen und Innovationen sowie aktuelle Forschungsergebnisse den Schwerpunkt. Es dient dem Kontakt der Studierenden der Vertiefung Geotechnik, Verkehrs- und Wasserwesen mit der Baupraxis.
