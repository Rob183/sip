## Betonkanuregatta

![image](/content/Wettbewerbe/Wettbewerb3.jpeg#z#g)

Seit 1986 findet als Highlight der Zement- und Betonindustrie aller 2 Jahre die Deutsche
Betonkanu-Regatta statt. Die Grundidee dieser Veranstaltung ist es, Studierende internationaler
Ausbildungsstätten, Hochschulen und Universitäten rund um die Materie Beton in einen
wissenschaftlichen, kreativen und sportlichen Wettbewerb zu stellen. Die HTWK Leipzig ist mit dem
vom IfB unterstützten Team seit Jahren erfolgreich dabei.
