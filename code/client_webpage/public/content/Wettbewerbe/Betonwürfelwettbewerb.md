## Betonwürfelwettbewerb

![image](/content/Wettbewerbe/Wettbewerb1.jpeg#z#g)


Kurz vor Weihnachten findet jährlich im Laborgebäude der Fakultät Bauwesen der Betonwürfelwettbewerb
statt. Unter jährlich wechselnden Vorgaben sin im Betonlabor Betonwürfel herzustellen, die dann im
Rahmen des Wettbewerbs einer Druckfestigkeitsprüfung unterzogen werden.
