## Brückenmodellwettbewerb

![image](/content/Wettbewerbe/Wettbewerb2.jpeg#z#g)

Die Studierenden der baubezogenen Studiengänge an der HTWK Leipzig werden jährlich unter
verschiedenen Vorgaben zum Bau von Brückenmodellen aufgefordert. Kurz vor Ende des Sommersemesters
findet dann in der Versuchshalle der Fakultät Bauwesen unter Wettbewerbsbedingungen eine
Belastungsprüfung statt. Zusätzlich wird immer ein Preis für die beste Gestaltung vergeben.
