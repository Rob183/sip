![bild17](/content/Kooperationen/mfpa.jpg#k)

&nbsp;

## Jobs

Wir suchen Unterstützung für unser Team: Wir bieten spannende und vielseitige Tätigkeiten in den Bereichen Bauingenieurwesen und Maschinenbau.

Bewirb dich gerne als Absolventin/Absolvent für einen Direkteinstieg oder für ein Praktikum bzw. eine Abschlussarbeit. **info@mfpa.de**

![bild18](/content/Kooperationen/jobs.png#z#g)
