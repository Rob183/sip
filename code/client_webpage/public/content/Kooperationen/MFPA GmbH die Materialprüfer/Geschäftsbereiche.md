![bild11](/content/Kooperationen/mfpa.jpg#k)

![bild12](/content/Kooperationen/geschaeftsbereichOben.png#m#r#u)
![bild13](/content/Kooperationen/geschaeftsbereichMitte.png#m#r#u)
![bild14](/content/Kooperationen/geschaeftsbereichUnten.png#m#r#u)

## Unsere Geschäftsbereiche

- Werkstoffe im Bauwesen
- Tragwerke und Konstruktionen
- Baulicher Brandschutz
- Bauphysik
- Tiefbau
- Zentrum für Innovation und Berechnung
