![bild36](/content/Kooperationen/mfpa.jpg#k)

## Praxispartner - MFPA

- Was tun wir? Klick auf unsere Geschäftsbereiche!
- Wie sind wir aufgestellt? Unser Organigramm…
- Wie kannst Du mitwirken? Finde einen Job bei uns!

MFPA Leipzig GmbH - Die Materialprüfer:
Mit Sicherheit geprüfte Qualität

![bild37](/content/Kooperationen/baustellenbildMfpa.png#z#g)
