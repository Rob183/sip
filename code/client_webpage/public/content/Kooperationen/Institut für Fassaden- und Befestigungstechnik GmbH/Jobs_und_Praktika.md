![bild45](/content/Kooperationen/ifbt.jpg#m)

## Jobs und Praktika

Du hast Interesse an experimentellen Untersuchungen und suchst einen Praxispartner in Leipzig im Bereich Forschung, Entwicklung und Materialprüfung?
Dann komm zu uns! Wir sind eine anerkannte Prüfstelle im Bereich von Verankerungslösungen und neuen Fassadensystemen, bei der neue Produkte aus der Industrie im Rahmen von Zulassungsverfahren untersucht und zertifiziert werden.

Wir bieten:

- Jobs und Praktika im Bereich experimenteller Prüfungen
- Themen für Bachelor- und Masterarbeiten

Kontakt: **t.kuehnert@fassade-und-befestigung.de**

weitere Infos: **www.fassade-und-befestigung.de**
