![bild53](/content/Kooperationen/exkursionenReUnten.png#m)

&nbsp;

## Thomas Kühnert

![bild54](/content/Kooperationen/thomasKuehnert.png#r#k)
Hallo! Mein Name ist Thomas Kühnert. Nach dem Abitur hatte ich den Wunsch Ingenieur zu werden und bin dem Ruf an die BA-HU gefolgt. Hier habe ich von 1996 – 2000 Bauingenieurwesen studiert, wobei ich das dritte Studienjahr an der Partneruniversität in Paisley (Schottland) verbracht habe – ein Erlebnis das ich nicht vergessen werde!

Über eine Hilfsassistentenstelle bei Prof. Sahlmann bin ich auf das Ingenieurbüro S&P aufmerksam geworden, wo ich 2000 als Projektingenieur im Bereich Fassadenstatik und thermische Bauphysik starten durfte. Dieses Arbeitsfeld wurde dann um die gutachterliche Betreuung von Zulassungsverfahren für neue Fassadensysteme / -produkte ergänzt, womit ich erste Berührungspunkte mit dem IFBT GmbH – Institut für Fassaden- und Befestigungstechnik hatte. Nach langjähriger intensiver Zusammenarbeit mit dem IFBT auf diesem Gebiet durfte ich im Jahr 2018 zum IFBT wechseln und habe ab Juli 2018 die Geschäftsführung übernommen.

Somit habe ich nun die Verantwortung für knapp 20 festangestellte Mitarbeiter und die 3 Tätigkeitsfelder Wärmedämmverbundsysteme (WDVS), vorgehängte hinterlüftete Fassaden (VHF) und Befestigungstechnik (Dübel, Anker, etc.), in denen wir experimentelle Prüfungen als Grundlage für nationale und europäische Zulassungen und objektspezifische Fragestellungen durchführen. Zur Verstärkung unseres Teams, in dem viele ehemalige BA-HU’s Verantwortung tragen, suchen wir immer pfiffigen Nachwuchs!
