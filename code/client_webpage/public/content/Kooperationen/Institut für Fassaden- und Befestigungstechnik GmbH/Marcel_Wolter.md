![bild28](/content/Kooperationen/ifbt.jpg#m)

&nbsp;

## Marcel Wolter

![bild29](/content/Kooperationen/marcelWolter.png#r#k)
Noch während meines Bauingenieur-Studiums an der HTWK Leipzig bin ich durch den Brückenmodellwettbewerb auf die IFBT GmbH – Institut für Fassaden- und Befestigungstechnik aufmerksam geworden.

Zunächst arbeitete ich als studentische Hilfskraft und konnte dabei einen Einblick in die vielfältigen Tätigkeitsfelder und das Team erlangen. Nach meiner Abschlussarbeit entschied ich mich für die Stelle des Projektleiters „Befestigungssysteme“ der IFBT GmbH.

Nun, nach mittlerweile sieben Jahren, leite ich mit großer Freude das Geschäftsfeld „Befestigungssysteme“ und beschäftige mich, neben den alltäglichen Herausforderungen, auch mit Studentinnen und Studenten und ihren Praktikums- und Abschlussarbeiten.
