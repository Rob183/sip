![bild57](/content/Kooperationen/exkursionenReOben.png#m)

&nbsp;

## Tobias Hoyer

![bild58](/content/Kooperationen/tobiasHoyer.jpg#k#r)
Wenn man sich mit der Stadt Leipzig beschäftigt, kommt man an dem Gewandhaus, als kulturelles Zentrum, nicht vorbei. Ein Teil dieses Projektes zu sein, welches das Ingenieurwesen mit der Kunst verbindet, ist etwas Besonderes. Dass wir mit dieser Arbeit ein völlig neues Gebiet betreten, macht sie umso spannender. Ich bin froh, einen Beitrag dazu leisten zu können.
