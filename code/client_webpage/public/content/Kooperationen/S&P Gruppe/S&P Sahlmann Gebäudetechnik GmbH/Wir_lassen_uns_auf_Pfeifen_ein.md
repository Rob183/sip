![Bild61](/content/Kooperationen/exkursionenReOben.png#m)

&nbsp;

## Projektbericht "Wir lassen uns auf Pfeifen ein"

Das Gewandhaus zu Leipzig ist für seine herausragende Akustik weltweit berühmt. Zentraler Blickfang im großen Saal ist die mächtige Schuke-Orgel.  
Über 6.000 Pfeifen verschiedener Längen und Bauarten ermöglichen einen beeindruckenden Frequenzumfang und einen Reichtum an Klangfarben. Wahrnehmbar jedoch nur bei den entsprechenden raumklimatischen Bedingungen, die durch technische Anlagen im gesamten Großen Saal bereitgestellt werden.

Das Forschungs- und Entwicklungsprojekt “Gewandhaus - Orgelklimatisierung” untersucht, ob und wie eine Mikroklimatisierung der Orgel die Temperierung des Instruments flexibler und zugleich energieeffizienter erreicht kann. Die HTWK ist, neben den Partnern Gewandhaus zu Leipzig, der S&P-Gruppe und MFPA, fest im Projektteam verankert.

![Bild62](/content/Kooperationen/gewandhaus.jpg#z#m)
