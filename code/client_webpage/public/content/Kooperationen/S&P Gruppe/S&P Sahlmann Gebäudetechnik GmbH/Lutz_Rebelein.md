![bild26](/content/Kooperationen/exkursionenReOben.png#m)

&nbsp;

## Lutz Rebelein

![bild27](/content/Kooperationen/lutzRebelein.png#r#k)
Seit meiner Jugend begeistern mich technische Anlagen und Energieflüsse aller Art. Mit der Zeit ist daraus Leidenschaft geworden. Der rasante Fortschritt in der Entwicklung von technischen Anlagen, die stetig stärker vernetzten Systeme beeindrucken mich immer wieder. Dies eröffnet mir jedes Mal aufregende Möglichkeiten, wie ich Anlagentechnik neu gestalten kann, dabei Menschen und Umwelt im Fokus behalten und Konzepte designen, die auch kritischen Augen aus verschieden Blickrichtungen standhalten…

Mich fasziniert das Gewandhaus zu Leipzig mit seiner Konzertkunst im Zusammenspiel mit der Architektur und den technischen Anlagen. Besonders sticht hier die Gewandhausorgel mit ihren 6.000 Pfeifen hervor. Es ist wunderbar, dass wir hier in Leipzig ein solches Haus mit diesem überwältigenden Instrument ansässig haben. Mit unserem Forschungs- und Entwicklungsprojekt "Gewandhaus - Orgelklimatisierung" wirke ich bei der Suche mit, die technischen Aufwendungen für den Betrieb der Orgel zu senken. Damit sollen möglichst zusätzliche Freiräume für das Team des Gewandhauses geschaffen werden, womit die künstlerischen und musikalischen Aspekte gefördert werden können.

Wie und mit welcher technischen Anlagenvariante das Ziel erreicht werden kann, ist eine besondere Herausforderung, da keine fertigen Lösungen bekannt sind und die Akustik nicht beeinflusst werden darf.

Vielleicht entsteht sogar eine interessante Produktlösung, die auf andere Orgeln übertragen werden kann.
