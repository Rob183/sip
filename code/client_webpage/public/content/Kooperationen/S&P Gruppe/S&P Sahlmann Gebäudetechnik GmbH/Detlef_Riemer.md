![bild8](/content/Kooperationen/exkursionenReOben.png#m)

&nbsp;

## Prof. Dr.-Ing. Detlef Riemer

![bild9](/content/Kooperationen/detlefRiemer.jpg#r#m)
Orgel trifft Mechatronik und Energie…

Für die Analyse der klimatischen
Bedingungen wurde gemeinsam mit
Studenten, der MFPA Leipzig und der
S&P Sahlmann Gebäudetechnik
GmbH ein vernetztes digitales Mess-
system mit über 50 kalibrierten
Miniatur-Sensoren entwickelt und in
die große Orgel des Gewandhauses
Leipzig dauerhaft integriert. Das Ziel: Den Klang der Orgel in Konzerten zu stabilisieren und gleichzeitig die notwendige Raumklimatisierung energetisch stark zu minimieren.

Das ich selbst musikalisch die Orgeltasten drücke - ist das für mich ein Forschungsthema welches Ohr, Herz und Geist berührt.
