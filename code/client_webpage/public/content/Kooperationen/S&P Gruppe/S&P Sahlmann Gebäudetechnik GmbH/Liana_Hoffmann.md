![bild24](/content/Kooperationen/exkursionenReOben.png#m)

&nbsp;

## Liana Hoffmann

![bild25](/content/Kooperationen/lianaHoffmann.png#r#k)
Zahlreiche interessante, kleine und große Projekte konnte ich im Rahmen meiner langjährigen Tätigkeit als Energieberater schon bearbeiten, jedes speziell und besonders. Das Projekt der Orgelklimatisierung, an dem ich nun bereits seit 2015 mitwirke, sticht durch seine Individualität besonders hervor. Dabei ist es ja nicht nur toll, regional tätig zu sein. In Erwartung des bekanntermaßen ausgedehnten Applauses der Besucher nach einem Konzert im Gewandhaus macht es ebenso stolz, unterstützen zu können, die Erwärmung der imposanten Schuke- Orgel im Großen Saal flexibler und energieeffizienter zu gestalten.
