![bild32](/content/Kooperationen/exkursionenLiOben.png#m)

## Martin Dvořák

![bild33](/content/Kooperationen/martindovrak.png#r#k)
Hi, ich bin Martin Dvořák.

Ich habe von 2008 bis 2014 mein Bachelor- und Masterstudium an der HTWK Leipzig absolviert.

Bereits während des Studiums war ich bei der S&P Planungsgesellschaft mbH Leipzig aktiv und bin seit Abschluss meines Studiums dort tätig.

Nachfolgend eine Auswahl an realisierten Projekten, an welchen ich mitgewirkt habe:

- Erweiterung Kernwerk Porsche AG, Leipzig
- Sanierung und Modernisierung Bahnhofsgebäude, Sangerhausen
- Servicewohnanlage Kregelstraße, Leipzig
- Umnutzung Bürogebäude ehem. Brühlpelz in Hotel- und Geschäftshaus, Leipzig

Bei S&P erwächst aus der Zusammenarbeit von Architekten und Ingenieuren eine kraftvolle Mischung, womit wir eine umfangreiche Planung von der ersten Konzeptskizze bis zum fertigen Bauwerk gewährleisten.
