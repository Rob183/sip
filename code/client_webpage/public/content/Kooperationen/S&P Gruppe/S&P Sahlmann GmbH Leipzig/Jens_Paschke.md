![bild15](/content/Kooperationen/exkursionenLiOben.png#m)

&nbsp;

## Jens Paschke

![bild16](/content/Kooperationen/jensPaschke.png#r#k)
Mein Name ist Jens Paschke. Von 2002 bis 2006 habe ich an der HTWK Architektur studiert. Seit 2007 bin ich bei der S&P Planungsgesellschaft für Bauwesen mbH Leipzig angestellt.

Angefangen habe ich als Sachbearbeiter für eine hausinterne Datenbanksoftware. Inzwischen arbeite ich im Bereich Controlling und erarbeite für die Geschäftsführung unsere Projekt- und Unternehmenszahlen. Hilfreich hierfür war sicherlich meine vor dem Studium abgeschlossene Lehre zum Industriekaufmann. S&P hat mir die Möglichkeit gegeben in der Zeit mit den Aufgaben zu wachsen.

Auch wenn ich mein im Studium erlerntes Wissen bei meiner täglichen Arbeit nicht anwenden kann, habe ich dennoch einen anderen Blick auf die geleistete Arbeit der Kollegen fern ab der Zahlen. Besonders hervorheben möchte ich den gelebten Begriff der Familie innerhalb von S&P.
