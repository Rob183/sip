﻿![bild15](/content/Kooperationen/exkursionenLiOben.png#m)

&nbsp;

## Yvonne Krug

![yvonneKrug](/content/Kooperationen/yvonneKrug.png#r#k)
Hallo! Mein Name ist Yvonne Krug.
Zum Studium der Architektur bin ich über einenkleinen Umweg, nämlich über eine Lehre als Bauzeichnerin gekommen. Nachdem ich drei Jahre in einem Ingenieurbüro in Leipzig gelernt und gezeichnet habe und währenddessen das Berufs-bild des Architekten kennengelernt habe, habe ich im Anschluss von 1999 bis 2004 an der HTWKeipzig Architektur studiert. Ich wollte auch Architekt werden!

Über einen Aushang in der HTWK wurde ich auf die S&P Planungsgesellschaft aufmerksam und habe dort in den Semesterferien angefangen. Das lief dann so gut, dass ich meine Diplomarbeit auch in Zusammenarbeit mit S&P machen durfte, mit dem Vorteil ein ganz individuelles und vor allem reales Thema zu haben: „Wohnen und Arbeiten am Aurelienbogen“. Hierbei wurde ich sehr gut von S&P betreut und konnte im Frühjahr 2004 nach bestandener Diplomarbeit mit einer Anstellung bei S&P mein gesamtes Können unter Beweis stellen. Zur damaligen Zeit ein großer Glücksfall und für mich eine riesige Auszeichnung hier im Raum Leipzig eine Anstellung zu finden.

Das Thema des Aurelienbogen haben wir später im Büro noch weiter untersucht und geplant. Das hat mich sehr stolz gemacht. Nach nunmehr fast 15 Jahren bin immer noch bei S&P. Ich habe in dieser Zeit viele verschiedene Projekte bearbeitet und wachsen sehen. Nebenbei habe ich eine Familie gegründet und beides, Job und Familie, mit S&P gemeinsam unter einen Hut bringen können. Und wie ich finde unter einen sehr guten.
