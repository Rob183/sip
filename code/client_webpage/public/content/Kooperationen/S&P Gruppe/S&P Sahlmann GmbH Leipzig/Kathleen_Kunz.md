![bild18](/content/Kooperationen/exkursionenLiOben.png#m)

&nbsp;

## Kathleen Kunz

![bild19](/content/Kooperationen/kathleenKunz.png#r#k)
Bereits bei der Eintragung in Poesiealben der 1. Klasse
meiner Mitschüler stand mein Berufswunsch, Architektin werden zu wollen für mich schon völlig fest. So startete ich nach dem Abi direkt mein Studium an der Htwk Leipzig. Nach dem Diplom 2009 entschied ich mich mein Wissen in Richtung Innenarchitektur zu vertiefen und hing noch einen Masterabschluss hinter das Diplom. 2010 startete ich dann also als Dipl.-Ing. (MA) :-) ins Berufsleben.

Nach 2 lehrreichen Jahren in einem sehr bekannten Leipziger Büro, welche durch interessante Wettbewerbe und stadtprägende Projekte begleitet waren, fehlte mir trotzdem irgendetwas. Auf der Suche nach einem Unternehmen was mir die Möglichkeit geben würde meine Kreativität auch über die Entwurfsphase hinaus zu entwickeln und eigenverantwortlich Projekte zu begleiten, habe ich mir die Unternehmensstrukturen verschiedenster Büros genau angesehen. Meine Wahl ist damals bewusst auf S&P gefallen. Seit 2013 kann ich mich durch die Eintragung in die Architektenkammer Sachsen auch endlich als vollwertige Architektin bezeichnen.

Meine damalige bewusste Entscheidung das Büro zu wechseln habe ich bis heute zu keinem einzigen Tag bereut. Seit nunmehr sieben Jahren gehöre ich zur S&P Familie- in welcher ich bereits zahlreiche interessante Projekte Eigenverantwortlich & in toller Arbeitsatmosphäre bearbeitet habe.
