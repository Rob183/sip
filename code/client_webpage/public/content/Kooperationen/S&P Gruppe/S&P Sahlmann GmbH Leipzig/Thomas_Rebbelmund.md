![bild55](/content/Kooperationen/exkursionenLiOben.png#m)

&nbsp;

## Thomas Rebbelmund

![bild56](/content/Kooperationen/thomasRebbelmund.png#r#k)
Hallo, mein Name ist Thomas Rebbelmund. Ich habe im Zeitraum 1993 bis 1997 hier an der HTWK Bauingenieurwesen mit der Vertiefungsrichtung „Bausanierung“ studiert. Unmittelbar nach meinem Studium begann ich meine Ingenieursarbeit bei der S&P Planungsgesellschaft mbH Leipzig, bei der ich nunmehr schon seit 20 Jahren als Projektingenieur im Bereich Hoch- und Industriebau tätig bin. In dieser langen Zeit habe ich unzählige Projekte aller Art bearbeitet und betreut, wie:

- die Sanierung und Modernisierung ganzer denkmalgeschützter Straßenzüge und einer Vielzahl von Einzelobjekten in Leipzig und Chemnitz sowie unzählige Plattenbauten
- den Neubau von technischen und logistischen Bauwerken im Uniklinikum Leipzig, Geschäftshäusern, Kitas, Schulerweiterungen etc.
- die Erweiterung der Endmontagehalle des Leipziger Porschewerks im laufenden Produktionsprozess,
- umfassende Sanierungsarbeiten im Bereich Spezialtiefbau zur Stabilisierung des Süd- und Osthangs des Burgbergs in Eilenburg
- Abbrucharbeiten u.A. des ehemaligen Bettenhauses des Uniklinikum Leipzig

und weitere größere und kleinere Projekte vorrangig im Raum Sachsen.

Warum ich bei S&P bin? Weil ich hier als Berufseinsteiger eine Möglichkeit bekommen habe mich selbst vielfältig entwickeln zu können. Auch war es mir über die Jahre möglich aktiv an der Entwicklung des Unternehmens S&P mitzuwirken und so mein Arbeitsumfeld und meinen eigenen Arbeitsplatz mitgestalten zu können.
