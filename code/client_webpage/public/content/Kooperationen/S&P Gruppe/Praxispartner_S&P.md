## Über den Praxispartner S&P

Du suchst einen Praxispartner in Leipzig, Potsdam, Dresden oder Zwickau?
Dann komm zu S&P, mit über 450 Mitarbeitern einem der
größten Ingenieur- und Architekturbüros in den neuen Bundesländern!

Wir bieten:

- Jobs und Praktika im Bereich Architektur, Hochbauplanung, Tragwerksplanung, TGA-Planung, Softwareentwicklung und Gutachterwesen

- Themen für Bachelor- und Masterarbeiten

Gern kannst Du Dich auch als „Werkstudent“ für ein Stipendium bewerben.

weitere Infos: **www.sup-gruppe.com**

![bild39](/content/Kooperationen/exkursionenLiOben.png#m#l)

![bild40](/content/Kooperationen/exkursionenReOben.png#m#r)

![bild41](/content/Kooperationen/exkursionenLiUnten.png#m#l)

![bild42](/content/Kooperationen/exkursionenReUnten.png#m#r)
