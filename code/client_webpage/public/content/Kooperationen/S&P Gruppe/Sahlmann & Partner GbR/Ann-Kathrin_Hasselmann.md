![bild3](/content/Kooperationen/exkursionenLiUnten.png#m)

&nbsp;

## Ann-Kathrin Hasselmann

![bild4](/content/Kooperationen/ann-kathrinHasselmann.png#r#k)

Hallo, ich bin Ann-Kathrin Hasselmann. Ich habe im Zeitraum von 2002 bis 2007 an der HTWK Architektur studiert. Während des Studiums habe ich als Werkstudent bei S&P 2,5 Jahre gearbeitet und konnte dabei in verschiedenen Arbeitsbereichen reinschauen.

Nach dem Studium durchforschte ich weitere Möglichkeiten als Architekt tätig zu sein und machte eine Weiterbildung als Energieberater für Wohngebäude in Leipzig und den Energieberater für Nichtwohngebäude in Berlin. Auf der Suche nach meiner Berufung war ich 6 Jahre lang als Projektsteuerer und als Stadt- und Regionalplaner in einem kleinen Büro außerhalb von Leipzig unterwegs. Aber es fehlte mir immer noch der Einstieg als Energieberater.

So entschied ich mich 2015 wieder nach Leipzig beruflich zurückzukehren, um näher bei der Familie in Leutzsch zu sein. Da lag es nahe eine Initiativbewerbung bei S&P abzugeben.

Nach kurzer Zeit bekam ich wirklich ein Angebot als Projektingenieur im Fachbereich Bauphysik. Trotz der Tatsache dass ich keine Berufserfahrung in diesem Bereich hatte, gab man mir die Chance der Einarbeitung inkl. Mentor, so dass ich sofort in die Projektarbeit gehen konnte.

Die Suche nach meiner Berufung habe ich endlich gefunden und seit mittlerweile 3 Jahren bin ich als vollwertiges S&P-Mitglied im Fachbereich Bauphysik tätig. Fazit: Einmal S&P immer S&P.
