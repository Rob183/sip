![bild3](/content/Kooperationen/exkursionenLiUnten.png#m)

&nbsp;

## Antje Proft

![AntjePoft](/content/Kooperationen/AntjePoft.png#r#k)

Ausgestattet mit dem Vordiplom der TU Cottbus landete ich 1991 in Leipzig. Mein erster Weg nach der Anmeldung zur Fortsetzung des Studiums an der TH Leipzig führte mich in das Zimmer von Professor Sahlmann mit der Frage nach einer Stelle als Hilfsassistent… die Entscheidung damals an diese Tür zu klopfen war wohl eine der Besten meines Lebens, denn nach dem Job als Hilfsassistent und einer Diplomarbeit zum Thema „Bemessung von hinterlüfteten Fassaden mit Holz-Unterkonstruktion“ bin ich letztendlich bei S&P gelandet und nach nunmehr fast 24 Jahren noch immer mit wachsender Begeisterung dabei!

Angefangen mit der Planung der Sanierung von Plattenbauten und statischen Berechnungen von Fassaden durfte ich mich unter dem Dach von S&P zu einer Fachfrau für Wärmedämmverbundsysteme entwickeln. In Zusammenarbeit mit den Prüfanstalten IFBT und MFPA betreuen wir in einem kleinen Team von 5 Leuten innerhalb der Sahlmann & Partner GbR Hersteller von Fassadenprodukten im Rahmen von deren Produktentwicklung und Zertifizierung der Fassadensysteme im Zuge von Zulassungsverfahren. Das ist zugegebener Maßen ein sehr kleines Feld des Bauens, aber jeden Tag fordernd, spannend und abwechslungsreich - was auch ich am Anfang meines Berufslebens nie so erwartet hätte. Insofern gilt – zumindest für mich – einmal S&P – immer S&P!
