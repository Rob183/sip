![bild30](/content/Kooperationen/exkursionenReUnten.png#m)

&nbsp;

## Markus Sperling

![bild31](/content/Kooperationen/markusSperling.png#r#k)
Nach meiner Ausbildung zum Wirtschaftsassistent in der Informatik und einem anschließenden Fachabitur begann ich im Jahr 2008 mein Studium zum Bachelor im Bauingenieurwesen an der HTWK Leipzig. Verschiedene Tätigkeiten, u.a. in einem Ingenieurbüro für Bausanierung und einer Anstellung als Projektingenieur bei der Gesellschaft für Geo- und Baumesstechnik mbH begleiteten mein Studium über den Masterstudiengang hinweg, welches ich im Jahr 2016 erfolgreich beendete.

Neben meiner zweijährigen Tätigkeit als Projektingenieur in der Geo- und Baumesstechnik beschäftigte ich mich in meiner Freizeit mit Programmierung. Meine zunehmende Begeisterung für dieses Interessengebiet sorgte Anfang 2018 für die Entscheidung mich neu zu orientieren und beruflich weiterzuentwickeln. Auf der Suche nach einer entsprechenden Tätigkeit bot sich bei der S&P Software Consulting and Solutions GmbH diese Gelegenheit, da diese zu diesem Zeitpunkt auf der Suche nach einen Bauingenieur war.

Seit Mai 2018 bin ich als Teil des Teams mit dem Ergebnis meiner Neuorientierung, der daraus resultierenden Tätigkeit bei S&P sehr zufrieden. Die vielfältigen Aufgabengebiete und das angenehme Betriebsklima geben mir viele Möglichkeiten mein im Studium angeeignetes Wissen und meine bisherige Berufserfahrung einzubringen und erweitern zu können.
