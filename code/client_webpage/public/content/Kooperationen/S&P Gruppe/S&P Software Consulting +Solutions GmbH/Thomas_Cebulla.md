![bild51](/content/Kooperationen/exkursionenReUnten.png#m)

&nbsp;

## Thomas Cebulla

![bild52](/content/Kooperationen/thomasCebulla.jpg#r#k)

Mein Name ist Thomas Cebulla. Ich habe 1990 an der TH Leipzig / HTWK Leipzig mein Studium in Bauingenieurwesen aufgenommen.  
In der Vertiefungsrichtung habe ich Konstruktiven Ingenieurbau und in dem Zusammenhang auch unter Anleitung von zwei S&P Firmengründern studiert, Herrn Professor Sahlmann und Herrn Dr. Wolf. Meine Diplomarbeit über die Befestigung von Fassadendübeln wurde durch den heutigen Geschäftsführer von S&P Potsdam, Dr. Jacob, betreut.

Bevor mich mein Weg zu S&P geführt hat, durfte ich in einem Leipziger Büro für Prüfstatik intensive Erfahrungen aus verschiedensten Richtungen sammeln.Im Jahr 2000 bot sich die Möglichkeit, meine Leidenschaft für Computer und Programmierung und meine Erfahrungen aus Studium und Berufstätigkeit in einer neuen Form zu kombinieren. In der neu gegründeten S&P Software Consulting + Solutions GmbH konnte ich dazu beitragen ein neues Geschäftsmodell mit aufzubauen, in dem die S&P Software Consulting + Solutions heute national und international sehr erfolgreich agiert.

Die Befestigungstechnik als eines der aktuellen Kerngeschäfte begleitet mich seit meiner Diplomarbeit vor nunmehr 23 Jahren. Diese Erfahrungen hoffe ich zukünftig auch im Sinne der S&P und ihrer Partner in der Task Group 2.9 des fib (Fédération internationale du béton) einbringen zu können.
