![bild38](/content/Kooperationen/exkursionenReUnten.png#m)

## Über S&P Software Consulting + Solutions GmbH

Professioneller Ansprechpartner der weltweit tätigen Bauprodukteindustrie. Als zertifizierter Spezialist entwickeln und implementieren wir in Marktführerschaft individuelle Bemessungssoftware und unterstützen deren nachhaltigen Betrieb.
