## Partnerhochschulen

**Insgesamt rund 60 Internationale Partnerhochschule.**

Auswahl:

- Arab International University / Syrien

- Federal University of Santa Maria / Brasilien

- Ho Chi Minh City University of Transport / Vietnam

- Istanbul Technical University / Türkei

- Mekelle University / Äthiopien

- Moscow State University of Civil Engineering / Russland

- Nagoya City University / Japan

- National University of Civil Engineering / Vietnam

- North China University of Water Resources and Electric Power / China

- Poltava National Technical Yuriy Kondratyuk University / Ukraine

- Saint Petersburg State University of Architecture and Civil Engineering / Russland

- Technical University of Civil Engineering Bucharest / Rumänien

- University of Brescia / Italien

- University of Jaén / Spanien

- University of Ljubljana / Slowenien

- University of the West of Scotland / Großbritannien

- Vilnius Gediminas Technical University / Litauen

- Wroclaw University of Science and Technology / Polen

- Xi'an University of Architecture and Technology / China
