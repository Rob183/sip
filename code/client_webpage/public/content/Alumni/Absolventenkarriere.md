## Absolventenkarriere

![bild1](/content/Alumni/absolventenkarriereOben.png#l#g)

Iris Wolke-Haupt, Geschäftsführerin der LWB und Alumna der TH Leipzig
(Studium Bauingenieurwesen 1985-1990)

![bild2](/content/Alumni/absolventenkarriereUnten.png#l#g)

Sebastian Wartenberg, Büroinhaber 'Wartenberg Ingenieure -Bauen
für Behinderte und Betagte' in Leipzig, Alumnus der HTWK Leipzig
(Studium Bauingenieurwesen 1998-2004)
