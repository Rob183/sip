## Absolventenfeier

Seit dem Jahr 2007 werden durch die Fakultät Bauwesen für ihre Absolventen jährliche Gradierungsfeiern durchgeführt.

Letztmalig erhielten am 20.01.2018 im Ring-Café Leipzig die Absolventen der Studiengänge des Bauingenieurwesens im Rahmen der Gradierungsfeier ihre Bachelor- und Masterurkunden.

Die letzte Graduierungsfeier fand am 18.01.2019 im Ring-Cafe Leipzig statt.

![image](/content/Alumni/absolventenfeier.png#z#g)
