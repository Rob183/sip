## Ausgewählte Forschungsthemen

**RegeFa - Entwicklung von Beurteilungskriterien für den Schlagregenschutz von Fassaden**

Geldgeber: Bundesministeriums für Bildung und Forschung (BMBF)

Projektleiter: Prof. Dr. -Ing Ulrich Möller

Partner: Institut für Bauklimatik der TU Dresden, hf sensor GmbH

Laufzeit: 11/2014 - 10/2017

![image](/content/Forschung/Forschung1.jpeg#z#g)
