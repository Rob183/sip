## Forschungsschwerpunkte

Einen hohen Stellenwert haben an der Fakultät die Erhaltung der Bausubstanz und das energie-und
ressourcenschonende Bauen. Dabei spielen die Nutzung erneuerbarer Energien, der Einsatz
umweltschonender und recycelbarer Baumaterialien und die Weiterentwicklung der Bauweisen eine große
Rolle. Die Forschung an der Fakultät ist daher auf folgende **Schwerpunkte** ausgerichtet:

&nbsp;

### Bausubstanzhaltung

### Innovative Baustoffe

### Energieeffizienz und Nachhaltigkeit

### Wasser- und Siedlungswasserwirtschaft Geotechnik
