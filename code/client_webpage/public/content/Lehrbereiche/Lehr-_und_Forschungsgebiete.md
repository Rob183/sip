## Lehr- und Forschungsgebiete

- Lehrbereich Baustoffe und Bausanierung

- Lehrbereich Stahlbau und Holzbau

- Lehr- und Forschungsbereich Mechanik/Dynamik

- Lehrbereich Verkehrs- und Infrastrukturplanung

- Lehrbereich Vermessung
