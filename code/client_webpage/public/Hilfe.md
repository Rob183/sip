## Hilfe

### Fakultät Bauwesen

Hochschule für Technik, Wirtschaft und Kultur Leipzig
Postfach 30 11 66
04251 Leipzig

https://fb.htwk-leipzig.de

#### Prüfungsamt BIB, BIK, BIM

Grundsätzliche Informationen zu prüfungsrelevanten Fragen geben Ihnen die Mitarbeiter(innen) des Prüfungsamtes und der Vorsitzende des Prüfungsausschusses Bauingenieurwesen.

##### Öffnungszeiten

- Dienstag 13:00 bis 15:00
- Donnerstag 09:30 bis 11:30
- und nach Vereinbarung

##### Ansprechpartner

Dipl.-Ing. Cindy Dräger
Raum: TC 0.79 | G215
Telefon: +49 (0) 341 3076-8453
Fax: +49 (0) 341 3076-6507
E-Mail: pruefungsamt.fb@htwk-leipzig.de

### IT-Servicezentrum

Das IT-Servicezentrum ist der zentrale IT-Dienstleister der HTWK Leipzig und erbringt oder koordiniert alle zentralen IT-Services. Dies umfasst beispielsweise Betrieb und Ausbau des Hochschulnetzes, E-Mail & Groupware, Systeme zum Datenaustausch u.v.m.

#### E-Mail

itsz@htwk-leipzig.de

#### Besucheranschrift

Eichendorffstraße 2  
04277 Leipzig

#### Postanschrift

HTWK Leipzig  
Postfach 301166  
04251 Leipzig
