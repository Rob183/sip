const fs = require("fs");
const bcrypt = require("bcryptjs");
const readline = require("readline");

const saltLength = 8;
const view = readline.createInterface({
  input: process.stdin,
  output: process.stdout,
});

view.question("Please enter new unlock password: ", (password) => {
  let newPassword = password;
  view.question("Please re-type new unlock password: ", (password) => {
    if (password == newPassword) {
      let newHash = bcrypt.hashSync(newPassword, saltLength);
      fs.writeFile(
        "./src/components/unlock/PIN.js",
        'export const PIN = "' + newHash + '";\n',
        function (error) {
          if (error) {
            return console.log("Failed to change password: " + error);
          }
          console.log("Password successfully changed.");
        }
      );
    } else {
      console.log("Passwords not identical, aborted.");
    }
    view.close();
  });
});
