var express = require("express");
var proxy = require("http-proxy-middleware");

var app = express();

// Stundenplan-Offline Dummy
app.use(express.static("public"));

// Proxy from localhost:5001/api/fb to https://fb.htwk-leipzig.de
app.use(
  "/api/fb",
  proxy({
    target: "https://fb.htwk-leipzig.de",
    changeOrigin: true,
    pathRewrite: {
      "^/api/fb": "",
    },
    onProxyRes(proxyRes, req, res) {
      proxyRes.headers["Access-Control-Allow-Origin"] = "*";
    },
  })
);
// Proxy from localhost:5001/api/bim to https://bim.htwk-leipzig.de
app.use(
  "/api/bim",
  proxy({
    target: "https://bim.htwk-leipzig.de",
    changeOrigin: true,
    pathRewrite: {
      "^/api/bim": "",
    },
    onProxyRes(proxyRes, req, res) {
      proxyRes.headers["Access-Control-Allow-Origin"] = "*";
    },
  })
);

// Proxy from localhost:5001/api/stundenplan to https://stundenplan.htwk-leipzig.de
// !!!Anfangsversion: https://stundenplan.htwk-leipzig.de aber  bitte dazu altes NodeJS benutzen, hier die Information:
// https://www.ssllabs.com/ssltest/analyze.html?d=stundenplan.htwk%2dleipzig.de&latest
// !!!geänderte Version: http://stundenplan.htwk-leipzig.de ist funktionsfähig

app.use(
  "/api/stundenplan",
  proxy({
    target: "http://stundenplan.htwk-leipzig.de",
    changeOrigin: true,
    pathRewrite: {
      "^/api/stundenplan": "",
    },
    onProxyRes(proxyRes, req, res) {
      proxyRes.headers["Access-Control-Allow-Origin"] = "*";
    },
  })
);

app.listen(5001);
