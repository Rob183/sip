#!/usr/bin/env python3
'''
Python-Script welches den Inhalt: "Institute an der FB" ausliest
und als MarkDown im content-Ordner speichert. Wenn die HTWK-Seite
nicht erreichbar sein sollte, wird die letzte vorhandene MarkDown-
Datei nicht ueberschrieben.
'''

import sys
import os
import codecs
from urllib import request
from urllib.error import URLError
from bs4.builder._htmlparser import HTMLParseError
from bs4 import BeautifulSoup

FILENAME = 'Institute.md'
PATHDIR = os.path.join('..', '..', 'client_webpage', 'public', 'content', FILENAME)

try:
    URL_REQUESTED = request.urlopen('https://fb.htwk-leipzig.de/forschung/institute/')

    HTML_CONTENT = str(URL_REQUESTED.read().decode('utf-8'))
    SOUP = BeautifulSoup(HTML_CONTENT, 'html.parser')
    INSTITUTE = [line.text for line in SOUP.find_all("span", attrs={"class": "accordion-title"})]

    if not INSTITUTE:
        print("Error: List of institutes is empty.", file=sys.stderr)
        sys.exit(13) #ERROR_INVALID_DATA

    FILE = codecs.open(PATHDIR, 'w', 'utf-8')
    FILE.write('## Institute\n\n')

    for (_, line) in enumerate(INSTITUTE):
        FILE.write('* ' + line + '\n\n')

    FILE.close()

    print("Sucessfully created " + FILENAME)
except URLError:
    print("Error: Failed to load URL.", file=sys.stderr)
    sys.exit(58) #ERROR_BAD_NET_RESP
except HTMLParseError:
    print("Error: Failed to parse HTML.", file=sys.stderr)
    sys.exit(1)
except IOError:
    print("Error: Failed to write " + FILENAME, file=sys.stderr)
    sys.exit(1)
