import sys
import json
from urllib.parse import urlsplit
import requests
from bs4 import BeautifulSoup


# Translated the mail crypto code in the JS code
# This might not work in the future. Could be generated?

def link_to_uncryptmailto(obfuscated_str):
    return decrypt_string(obfuscated_str, -2)


def decrypt_string(enc, offset):
    dec = ""
    for char in enc:
        ascii_char = ord(char)
        if 0x2B <= ascii_char <= 0x3A:
            dec += decrypt_charcode(ascii_char, 0x2B, 0x3A, offset)
        elif 0x40 <= ascii_char <= 0x5A:
            dec += decrypt_charcode(ascii_char, 0x40, 0x5A, offset)
        elif 0x61 <= ascii_char <= 0x7A:
            dec += decrypt_charcode(ascii_char, 0x61, 0x7A, offset)
        else:
            dec += char
    return dec


def decrypt_charcode(ascii_char, start, end, offset):
    ascii_char = ascii_char+offset
    if offset > 0 and ascii_char > end:
        ascii_char = start+(ascii_char-end-1)
    elif offset < 0 and ascii_char < start:
        ascii_char = end-(start-ascii_char-1)
    return chr(ascii_char)

# End of mail decryption stuff


def link_fix(link):
    try:
        parsed = urlsplit(link)
        if not parsed.scheme and not parsed.netloc:
            return 'https://www.htwk-leipzig.de' + \
                link
        return link
    except:
        return 'https://www.htwk-leipzig.de' + \
            link


def get_additional_data(contact_page_url):
    image = ""
    mail = ""
    if contact_page_url is not None:
        contact_page = BeautifulSoup(
            requests.get(contact_page_url).text, "lxml")

        image = contact_page.find(
            lambda tag: tag.name == 'img' and tag.has_attr(
                'src') and ".jpg" in tag['src']
        )
        if image is not None:
            image = link_fix(image['src'].strip())
        else:
            image = ""

        # Some contacts don't give a fuck and just display their mail in plaintext
        mail = contact_page.find(
            lambda tag: tag.name == 'a' and "@htwk-leipzig.de" in tag.text
        )
        if mail is not None:
            mail = mail.text.strip()
        else:
            # Mail seems to be obfuscated, let's try our luck…
            try:
                mail_enc = contact_page.find(
                    lambda tag: tag.name == 'a' and tag.has_attr(
                        'href') and "javascript:linkTo_UnCryptMailto" in tag['href']
                )['href'].split('\'')[1]
                mail_dec = link_to_uncryptmailto(
                    mail_enc).replace(r'\-', '-', 1)[7:]
                if "@htwk-leipzig.de" in mail_dec:
                    mail = mail_dec.strip()
                else:
                    mail = ""
            except:
                mail = ""
    return tuple([image, mail])


def parse_contact_row(contact_row, matching_prename, matching_surname):
    contact_tds = contact_row.findAll('td')
    link_field = contact_tds[0].a
    if link_field is not None:
        name_split = link_field.text.strip().split(', ', 1)
        link = link_field['href'].strip()
    else:
        name_split = contact_tds[0].text.strip().split(', ', 1)
        link = ""
    if matching_prename in name_split[1] and matching_surname in name_split[0]:
        name = name_split[1] + ' ' + name_split[0]
        title = contact_tds[1].text.strip()
        bereich = contact_tds[2].text.strip()
        room = contact_tds[3].text.strip()
        # There may be multiple phone numbers
        fon = contact_tds[4].text.strip().split('\n', 1)[0]

        return tuple([name, title, bereich, room, fon, link_fix(link)])
    return None


if len(sys.argv) > 1:
    prename = sys.argv[1]
    surname = sys.argv[2]
else:
    prename = input("Vorname: ").capitalize()
    surname = input("Name: ").capitalize()

# Get all contacts whose name starts with the first letter of the surname
url = 'https://www.htwk-leipzig.de/hochschule/kontakt/htwk-telefonverzeichnis/buchstabe/' + \
    surname[0]

contact_list = BeautifulSoup(requests.get(url).text, "lxml").find(
    'table', attrs={"class": "tableKomplex grey"}).findAll('tr')[1:]

contacts = [c for c in [parse_contact_row(contact_row, prename, surname)
                        for contact_row in contact_list] if c is not None]

LIST_DATA = None

if len(contacts) == 0:
    print("Contact not found.")
    sys.exit(1)
elif len(contacts) > 1:
    # Example: Uwe Jung
    print("Multiple contacts found. Which do you want?")
    for i in range(1, len(contacts)+1):
        print(f'%d: %s' % (i, contacts[i-1]))
    try:
        NUMBER = int(input("Number: "))
    except:
        NUMBER = None
    if NUMBER is None or NUMBER > len(contacts):
        NUMBER = 1
    LIST_DATA = contacts[NUMBER - 1]
else:
    print("Contact found.")
    LIST_DATA = contacts[0]

additional_data = get_additional_data(LIST_DATA[5])

json_object = json.dumps({
    "image": additional_data[0],
    "name": LIST_DATA[0],
    "title": LIST_DATA[1],
    "link": LIST_DATA[5],
    "bereich": LIST_DATA[2],
    "raum": LIST_DATA[3],
    "telefon": LIST_DATA[4],
    "email": additional_data[1]
}, indent=4, ensure_ascii=False)

try:
    with open('info.json', 'w') as outfile:
        outfile.write(json_object)
        print("Contact information wrote into `info.json`.")
except IOError:
    print("Failed to write data.")

print()
print(json_object)
