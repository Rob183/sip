# Das Protokoll zu dem Treffen von 06.07.20

Weekly Meeting

(Das Besprechen der Tickets von Sprint 8,
die aufgetauchten Probleme und die möglichen Lösungen)

Abwesend: Mathias Telling

## SIP-126 Doku Softwaremesse

(Zuständige: Marc Zenker)
**Was bisher gemacht wurde:** Dokumentation fertig aber noch kein Merge Request
**Das Problem:** Frage nach dem Poster
**Die Lösung:** Präsentationsfolien einbinden

## SIP-124 Fix des Lageplans

(Zuständige: Marc Zenker)
**Was bisher gemacht wurde:** ist fertig und Merge Request erstellt

## SIP-102 Testen des BIM-Bereichs

(Zuständige:Hanna Markava )
**Was bisher gemacht wurde:** ist fertig und Merge Request erstellt,
es ist aber noch nicht in master gemerged

## SIP-127 Ergänzung der Emails aller Projektbeteiligten

(Zuständige: Martin Diecke)
**Was bisher gemacht wurde:** ist fertig, bleibt noch in Git zu pushen und in master

## SIP-125 Doku Videos

(Zuständige: Max Vierling )
**Was bisher gemacht wurde:** fertig, nur noch ein bisschen Arbeit am Layout

## SIP-105 Testen des News Bereich

(Zuständige:Max Vierling )
**Was bisher gemacht wurde:** ist fertig und in master gemerged

(falls noch nicht protokolliert)

## SIP-104 Testen der PIN-Eingabe

(Zuständige: Nimrod Mugzach )
**Was bisher gemacht wurde:** ist fertig und in master gemerged
