# Das Protokoll zu dem Treffen von 9.12.2019

## Tickets von Sprint 1, aufgetauchte Probleme und die möglichen Lösungen

### Verlassen des Kiosk-Modus

(Zuständige: Vivien Richter, Mathias Telling)

**Was bisher gemacht wurde:** die Recherche zur Umsetzung und möglichen Lösungen
zum Ticket, die Idee die Umsetzung als Pin-Eingabe oder als Passwort
vorzustellen (mit Bildschirmtastatur)

**Das Problem:** die genaue Umsetzung

**Die Lösung:** Mockup (für Tastatur und Buttons)

### PDF-Darstellung

(Zuständige: Hanna Markava, Gwen Schulz )

**Was bisher gemacht wurde:** Skalierung der PDF-Dateien umgesetzt,
die Recherche für den Einbau der Knöpfe;

**Das Problem:** PDF-s im Projekt enthalten nur eine Seite -> wozu die Funktion „Blättern“?

**Die Lösung:** Testen, was passiert beim Einfügen einer PDF-Datei
mit mehreren Seiten, falls der Kunde später welche einbauen will;
verschiedene Mockups zu „Seiten Blättern“-Funktionen

### Einbindung von Videos

(Zuständiger: Max Vierling)

**Was bis jetzt gemacht wurde:** die Recherche und die Umsetzung des Tickets

**Das Problem:** das Herausfinden der Stelle, wo die Buttons einfügt
werden müssen im Code

**Die Lösung:** es ist empfehlenswert einen Test zu machen, weitere Umsetzung

### Mockup Sitemap

(Zuständige: Robert Seidel, Nimrod Mugzach)

**Was bisher gemacht wurde:** ein Entwurf als Mockup (Robert), die Umsetzung (Nimrod)

**Das Problem:** die genaue Aufgabenverteilung; ob die Umsetzung
schon jetzt benötigt wird; Notwendigkeit mehrerer Entwürfe.

**Die Lösung:** erstmal mehrere Entwürfe als Mockups machen,
durch Abstimmen einen auswählen und umsetzen

### Aufräumen der NPM-Konfigurationen

(Zuständige: Marc Zenker, Martin Diecke)

**Was bisher gemacht wurde:** die Recherche und die Umsetzung des Tickets

**Das Problem:** die Aufgabenstellung, was wird genau gemacht,
wie ist es genau aufgebaut; zu viele Packages fürs Überprüfen.

**Die Lösung:** nur die obersten Packages nehmen, das Vergleichen
mit den benutzten Packages im Code und in vorhandenen package.json,
die nicht gebrauchten Packages löschen, sonst neue einfügen.

### Fernzugriff / Remote Desktop Verbindung

(Zuständiger: Leon Malte Meng)

**Was bisher gemacht wurde:** das Prüfen des Fernzugriffes (funktioniert)
und der Datenübertragung, die Ansätze zu den Problemen

**Das Problem:** mehrere Zugriffe von den verschiedenen Benutzern
in der gleichen Zeit nicht möglich; die Abmeldung des lokalen Benutzers

**Die Lösung:** Dateiübertragung durch Cloud (Dateisynchronisation),
FDP, SAMBA (besser als Remote-Desktop) -> ist praktikabel

_Zusätzlich besprochene Punkte: die Pläne für das nächste Treffen,
Abgabetermine und die Beantwortung der weiteren Fragen._
