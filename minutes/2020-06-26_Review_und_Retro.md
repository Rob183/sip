# **Protokoll 26.06.2020**

## Anwesenheit

**Anwesend:** Alle.

**Verhindert:** Keiner.

## Review

- **SIP-144:** Fertig und gemerged.
- **SIP-102:** In Bearbeitung.
- **SIP-80:** Fertig und gemerged.
- **SIP-121:** Fertig und gemerged.
- **SIP-112:** Fertig und gemerged.
- **SIP-78:** Wurde bereits im vorherigem Sprint erledigt und gemerged.
- **SIP-117:** Noch nicht angefangen. Muss nicht mehr erledigt werden.
- **SIP-119:** Fertig und gemerged.
- **SIP-100:** Fertig und gemerged.
- **SIP-65:** Fehlermeldung fehlt noch, ansonsten funktional und bereinigt.
- **SIP-103:** Noch nicht angefangen. Muss nicht mehr erledigt werden.
- **SIP-113:** Abhängig von SIP-65, somit noch nicht fertig.
- **SIP-105:** Merge-Request erstellt, noch zu verifizieren.
- **SIP-37:** Noch zu verifizieren.
- **SIP-104:** Fertig und gemerged.
- **SIP-115:** Fertig und gemerged.
- **SIP-99:** Fertig und gemerged.
- **SIP-76:** Fertig.
- **SIP-116:** Fertig und gemerged.
- **SIP-120:** Fertig und gemerged.
- **SIP-118:** In Bearbeitung.
- **SIP-109:** Fertig und Merge-Request erstellt.

## Retrospektive

**Anmerkungen:**

- **Martin Diecke:** CI/CD-Pipelines hätten eher umgesetzt werden müssen.
- **Vivien Richter:** Einsicht: Git-Tutorial am Anfang wäre nützlich gewesen.
- **Marc Zenker:** Möglichkeit des physischen Testens der Stele hat gefehlt.
  Außerdem wären mehr Coding-Sessions nützlich gewesen.

**Umfrage:**
Wie hilfreich fandet ihr Coding-Sessions (CS) und Pair-Programming (PP).

| Teilnehmer      |    CS     |    PP    | Begründung               |
| --------------- | :-------: | :------: | :----------------------- |
| Hanna Markava   | hilfreich |  nicht   |
| Leon Malte Meng | hilfreich | begrenzt | Manchmal unnötig.        |
| Martin Diecke   |   sehr    |   sehr   |
| Mathias Telling |   nicht   |  nicht   | Durchsetzung mangelhaft. |

![Visualisierung bereitgestellt von Minh](images/2020-06-12_2020-06-26_Retro.png)

**Fazit:**

- Ein Git-Sheetsheet sowie eine Checkliste wäre gut gewesen.
- Das Projekt war anspruchsvoll und mit hohem Aufwand verbunden.
- Der bestehende Code war schwierig, was zu erhöhter Einarbeitungszeit führte.
