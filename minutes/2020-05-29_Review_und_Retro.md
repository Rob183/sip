# **Protokoll 29.05.2020**

## Anwesenheit

**anwesend:** Justin, Tobias, Minh, Hanna, Malte, Marc, Martin,
Mathias, Vivien, Max, Robert, Nimrod

**verhindert:**

## Review

**Robert:**

- Ticket: SIP-52
- Thema: Suchleiste
- Status:
- abgeschlossen

- Ticket: SIP-95
- Thema: Touchgeste PDF
- Status:
- abgeschlossen

**Hannah:**

- Ticket: SIP-93
- Thema: Touchgeste Pineingabe
- Status:
- fertig

- Ticket: SIP-94
- Thema: Touchgeste Sidemap
- Status:
- WIP

- Ticket: SIP-102
- Thema: Testen BIM Bereich
- Status:
- WIP

**Leon:**

- Ticket: SIP-81
- Thema: Karte
- Status:
- erledigt

**Max:**

- Ticket: SIP-75
- Thema: Einbindung Videos Markdown
- Status:
- WIP
- muss ggf. nur noch mit CSS angepasst werden

**Mathias:**

- Ticket: SIP-55
- Thema: Dateiübertragung per FTP
- Status:
- WIP

- Ticket: SIP-38
- Thema: Dateiübertragung per FTP
- Status:
- erledigt

  **Marc:**

  - Ticket: SIP-60
  - Thema: Aufarbeitung der Daten der Partner Hochschulen
  - Status:
  - WIP

**Vivien:**

- Ticket: 53
- Status: Durchsuchen der statischen Dateien
  - WIP

**Nimrod:**

- Ticket: SIP-38
- Thema: Sidemap
- erledigt

**offene Tickets:**

- Testen der Pineingabe
- Testen des Bim Bereich
- Pipeline für das Testen der Anwendung
- Pipeline für das Bauen der Anwendung
- Touchgeste für Sidemap
- Prototyp 3D Modell Ansicht
- Neuer Flyer für die Studiengänge
- Vorarbeit Automatisches Deploymend
- Installation der Stele als Anwendung
- Einbindung PDF im Markdown
- Repository line ending misconfigured
- Verknüpfung Frontend/ Backend Suche
- Refactoring
- Möglichkeiten untersuchen zum Entfernen Stundenplan Proxy
- Feature Hinweise

## Retrospektive

Beibehalten der Codingsessions in Zweierteams

![Visualisierung bereitgestellt von Minh](images/2020-04-13_Retro.png)
