# Weekly Meeting vom 27.04.2020

## Anwesenheit

Martin, Malte, Vivien, Mark, Nimrod, Robert, Hanna, Max anwesend

## Staus

### SIP-54

Testen der Anwendung vorgestellt. Ticket beendet.

### SIP-33

Ticket beendet, war bereitslänger fertig und bereits vorgestellt.

### SIP-28,SIP-48,SIP-50,SIP-51

Pineingabe zum Verlassen des Kiosbrowser jetzt im Master. Tickets beendet.

### SIP-32

Ticket beendet, war bereitslänger fertig und bereits vorgestellt.

### SIP-91

Recherche abgeschlossen. Einige Formate zur Verwendung von 3d im Web.

### SIP-61

Ticket beendet, war bereitslänger fertig und bereits vorgestellt.

### SIP-79

Deployment nicht möglich nur npm install,etc. Eventuell für Deployment
für hash.works Seite.

## Planung

Hanna: SIP-93 Touch geste Pineingabe

Malte: SIP-81interaktive Karte

Martin: SIP-92 3d Model Prototyp

Nimrod: SIP-90 Untersuchung 3d Frameworks

Max: SIP-75 Videos in Markdown

Mark: SIP-63 pdf in markdown
