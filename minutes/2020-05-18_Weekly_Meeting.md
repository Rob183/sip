# 2020-05-18 Planning Meeting

## Anwesenheit = alle außer MAx

## Themen

- Was wurde gemacht?

### Vivien

- SIP-78: Handbuch erweitern um Installationsanleitung klar niederzuschreiben
- SIP-53: Suchfunktion soll implementiert werden Metadatensuche reicht

### Robert

- SIP-52: als neues Ticket
- SIP-64: als neues Ticket

### Nimrod

- SIP-89: als neues Ticket

### Mathias

- SIP-55, SIP-65, SIP-80
- SIP-65: Überarbeitung anhand Justins Checklist
- SIP-80: angefangen sich reinzuarbeiten

### Martin

- SIP-92: Modell wird nicht ordentlich angezeigt

### Leon

- SIP-60, SIP-81
- Karte für Partnerhochschulen
  → kann als Geo-JSON gedownloaded werden und implementiert werden

### Hanna

- SIP-44: ist fertig
- SIP-94: wird bearbeitet
- SIP-102: einarbeiten in das Schreiben von Tests
