# Weekly Meeting vom 04.05.2020

## Anwesenheit

Martin, Malte, Vivien, Mark, Nimrod, Hanna, Max, Mathias anwesend

## Status

### SIP-65

Abgeschlossen, aber es muss noch die yarn.lock verkleinert werden.

### SIP-90

Einige Möglichkeiten Gefunden(z.B.:Three.js, Babylon.js).
Es wurde sich auf Three.js festgelegt.

## Planung

Viven übernimmt SIP-46.

## Retro

Keine Änderungen nötig, außer ein Zwischenstandsmeeting.
