# 27.01.2020

## Protokoll Retrospektive

### weiter so

impediment board + Vivien + Mathias + Marc
coding sessions + Martin
Feedback Master + Anja + Max + Marc

### mehr

an der Stele arbeiten + Marc
automatisierte Updates der Stele + Justin
mehr Unit Tests + Vivien

-zufrieden Robert + Anja + Nimrod + Gwen + Max

## Protokoll Planning

### SSH Server

erste Abstimmung durchschnittlich 8 ausreißer bei 3 und 13
Mathias: Geht auf 8 Runter-
Vivien: bereit auf 5 zu erhöhen
Martin: geht auf 5 runter
Zweite Abstimmung: Festgelegt auf 5

### Dokumentation: Dateiübertragung per FTP

erste Abstimmung zwischen 2 bis 5
Robert: bereit auf 3 zu gehen
Festgelegt auf 3

### Webscraping des HTWK Telefonverzeichnis

zwischen 3 und 8
Martin: kann sich nichts drunter vorstellen
Festgelegt auf 5

### Touch Gesten für die Verwendung der Website

erste abstimmung zwischen 5 und 8
Max: Suche bibliothek - schwierigkeit häng
t von der Bibliothek ab
zweite Abstimmung mehrheitlich 5 Vivien
dagegen wegen vermischung von normalen wischgesten und
wartungsgesten
festgelegt auf 5

### installation als anwendung

erste abstimmung zwischen 5 und 8
Robert: schwer durch lange recherche
festgelegt auf 5 nach zweiter abstimmung

### umsetzung der Interaktiven Karte

erste abstimmung zwischen 8 und 5
festgelegt auf 8

### einbinden pdf in markdown

zwischen 5 und 8
zweite Abstimmung einstimmig 8

### einbinden videos im Markdown

einstimmig 8 bei erster abstimmung

### untersuchung einbindung einer Präsentation

zwischen 3 und 8
Max: recherche nicht aufwendig
Vivien: 8 ist gerechtfertigt, da pdf implementation
nur notlösung -> recherche aufwendig
zweite abstimmung: zwischen 8 und 5 keiner besteht auf 8
festgelegt auf 5

### aufarbeitung der daten der Partner Hochschulen

erste abstimmung zwischen 5 und 8
Mathias stellt es sich nicht schwer vor
Nimrod schätzt es sich zeitintensiv vor
marc besteht auf 8 wegen hohem aufwand
festgelegt auf 8 keiner besteht auf seine 5
