# **Protokoll 12.06.2020**

## Anwesenheit

**Anwesend:**

- Hanna Markava
- Leon Malte Meng
- Martin Diecke
- Mathias Telling
- Nimrod Mugzach
- Tobias Mäde
- Vivien Richter
- Marc Zenker
- Van Minh Truong
- Max Vierling

**Verhindert:**

- Robert Seidel

## Review

- **SIP-63:** Fertig, jedoch noch nicht gemerged.
- **SIP-60:** Fertig.
  Für jeden der 5 kooperativen Bauwesen-Studiengänge
  wird jeweils ein einzelnes Element angelegt,
  da manche Partnerhochschulen in mehreren Studiengängen kooperieren.
- **SIP-76:** Kann nicht vorgestellt werden, bleibt daher auf zu verifizieren.
- **SIP-75:** Funktioniert. CSS wurde korrigiert. Merge-Request fehlt noch.
- **SIP-55:** Fertig und bereinigt. Merge-Request vorhanden.
- **SIP-94:** Fast fertig. Zwei Links müssen noch korrigiert werden.
- **SIP-21:** Fertig und gemerged.
- **SIP-92:** Import-Probleme bestehen weiterhin, trotz intensiver Recherche.
  Wird daher nicht weiter fortgesetzt.
- **SIP-89:** Kann aufgrund SIP-92 nicht fortgesetzt werden.
  Wird daher ebenfalls nicht fortgesetzt.
- **SIP-81:** Unklar, wo das Token gespeichert werden soll.
  Rücksprache mit Justin nötig.
- **SIP-78:** In Bearbeitung. Wird im Laufe des Tages fertig gestellt werden.
- **SIP-65:** In Bearbeitung.
- **SIP-03:** Erneut bearbeitet und Merge-Request erstellt.
- **SIP-23:** Erneut bearbeitet und Merge-Request erstellt.
- **SIP-83:** Dieses Ticket wird vorerst nicht bearbeitet,
  da es nicht im Zuge eines Planning-Meeting entstanden ist.

## Retrospektive

**Anmerkungen:**

- **Vivien Richter:** Mehr Tags bei den Tickets und Unit-Tests.
  Ab sofort sollte die Formatierung der Dokumentation vereinheitlicht werden.
- **Hanna Markava:** Vor der Zwischenstandspräsentation nochmal
  Alles auf der Stele zusammen testen.

![Visualisierung bereitgestellt von Minh](images/2020-06-12_2020-06-26_Retro.png)
