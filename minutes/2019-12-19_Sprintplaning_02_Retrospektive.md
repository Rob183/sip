# **Protokoll 19.12.2019**

## Anwesenheit

**anwesend:** Justin, Tobias, Minh, Anja (Hanna), Malte, Marc, Martin,
Mathias, Max, Robert, Nimrod, Vivien

**verhindert:** Gwen

## Planing Sprint 2

- Ticket 27: Proxy entfernen

  - Stundenplan, News, Meldungen BIM, Veranstaltungen aus Proxy lösen
    und in Webclient einfügen
  - Keinerlei Vorkenntnisse, 13er Bewertung trotz Schwierigkeitsgrad
    nicht gegeben, da eine Aufteilung sinnvoll kaum möglich scheint
  - &rarr; **8**: Untersuchung für Möglichkeiten proxy zu entfernen
    und woanders einzubinden

- Ticket 47: SFTP Server für Dateiübertragung:

  - Kurze Diskussion Samba vs SFTP, Änderung zu mehreren Alternativen
  - Vorschlag Ticket zu teilen, Thema ändern zu "nur Recherge"
  - Testen über VL-freie Zeit schwierig, nicht vor Ort
  - &rarr; **13**: Recherge und nach Auswahl die Umsetzung einer der Möglichkeiten

- Ticket 46: Suche

  - Kurze Demonstration von Justin
  - Mit grafischer Umsetzung wesentlich schwieriger
  - &rarr; **8**: durch grafische Umsetzung Einigung von 5 auf 8

- Ticket 28: Verlassen kiosk modus

  - &rarr; **5**: relativ eindeutig, Vorabeit wurde geleistet

- Ticket 34: Fehlersuche

  - Um auf 5 Tickets für je 2 Personen zu kommen, wurde von Minh
    die Fehlersuche aufgegriffen
  - Vorschlag aus der Gruppe: Ticket soll in Fehlerbehandlung
    geändert werden, Vorschlag wurde abgelehnt, da zuerst Fehler
    bekannt sein müssen, um effektiv dagegen vorgehen zu können;
    Zeit-/Ticketumfang nicht abschätzbar
  - &rarr; Nicht neu bewertet

- Ticket: Offline-Modus
  - begrenzt sich auf dyn. Inhalte
  - Hilfestellung über _promises_
  - &rarr; **8** nach kurzer Diskusion

## Retrospektive

- Anmerkungen von Minh:

  - Fragen vor Review klären
  - Prüfen auf Korrektheit der eigenen Lösungsvorschläge
  - Kurzer Verweis auf Pairprogramming (Vor-/Nachteile)

- Probleme Max:

  - Verständnisschwierigkeiten mit dem vorliegenden Code
  - fehlende Erfahrung mit react
  - git funktioniert nicht mehr

- Probleme Robert:

  - Fragen über Fragen, überwältigt

- Probleme Marc:
  - _npm install_ nach Änderung vergessen
  - nach _npm audit fix_ waren Warnungen beseitigt, aber Stele ging gar nicht mehr
