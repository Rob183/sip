# Meeting 28.11.2019

## Ziel des Meeting

## Anleitung Projekteinrichtung

### Voraussetzungen

- [Git](https://git-scm.com) installieren ()/Git bash
- `git clone https://gitlab.imn.htwk-leipzig.de/weicker/sip.git`
- Login mit dem Benutzernamen und Passwort (Geht nur wenn keine 2 Faktor Auth. aktiviert)

- Wichtige Gitbefehle:

  - git status (zeigt aktuellen Branch und nötige commits)
  - git checkout -b "brachname" (Branch wechseln ohne -b/erstellen -b)
  - echo "foo" > name (Datei name erstellen)
  - git add name (fügt name in denB Branch)
  - git commit
  - git log (zeigt commits), Ausstieg mit q
    git config --global user.email "stud.mail" (ändert E-Mail)
  - git config --global user.name "name" (ändert Namen)
  - git branch (Auflistung Branches, mit -d und name löschen möglich)
  - npm install(wenn node und npm installiert sind, meist zusammen zu downloaden)

- yarn:

  - yarn (muss instaliert werden, npm install --global yarn.
    Eventuell wird ein Pfad angezeigt, diesen kopieren
    und vim ~/.bashrc eingeben starten, dann
    `PATH=$PATH:*einfügen dann C: löschen*)`
  - yarn start (startet Scripts)
  - yarn bulid (optimertes Rendering der Scripts)

- Anwendung starten:
  - in zwei bashs
  - npm run-script start oder node proxy.js für Proxy
  - yarn start für client_webpage

### Entwicklungsumgebung

- IntelliJ (mit HTWK Login anmelden für gratis Enterprise Version)
- Plug-ins für NodeJS, React, etc.
- client_webpage und proxy als einzelne Projekte öffnen
