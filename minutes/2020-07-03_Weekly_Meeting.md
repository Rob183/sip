# Meeting vom 03.07.2020

Neue Dokumentations Tickets vorgestellt:

- SIP-122 Doku statische Inhalte
- SIP-123 Bearbeiten von Inhalten
- SIP-125 Fix des Lageplan
- SIP-125 Doku Videos
- SIP-126 Doku Softwaremesse
- SIP-127 Ergänzung der E-Mails aller Projekt beteiligten
