# Weekly Meeting vom 13. Januar 2020

## Anwesenheit

Gwen Schulz fehlt.

## Status

### SIP-46 - Suche

Unklar, wie Suche ausgeführt wird.

### SIP-52 - Entwurf: Suchleiste

Button fehlt noch.

### SIP-62 - Inhalt: Institutionen mit mehr Informationen

Läuft.

### SIP-32 - PDF Darstellung

PDF lädt noch nicht zuverlässig.

### SIP-43 - Offline Modus

Meldung muss noch implementiert werden.

### SIP-59 - Entwurf 2: Interaktive Karte

Recherche abgeschlossen und passendes Framework gefunden.

### SIP-58 - Entwurf 1: Interaktive Karte

MockUp angefertigt.

### SIP-34 - Fehlersuche bestehende Anwendung

Noch nichts gemacht.

### SIP-48 - Implementierung: Code-Eingabefeld zum Verlassen des SEB

Fehlen noch kleine Änderungen.
