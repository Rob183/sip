# **Protokoll 13.04.2020**

## Anwesenheit

**anwesend:** Justin, Tobias, Minh, Hanna, Malte, Marc, Martin,
Mathias, Vivien

**verhindert:** Max, Robert, Nimrod

## Review

**Malte:**

- Ticket: SIP-81 + SIP-56
- Thema: Karte
- Status:
- Aufgrund von Krankheit nicht weiter bearbeitet

**Hannah:**

- Ticket: SIP-44
- Thema: Tutorial
- Status:
- Fast fertig, Ausformulierungen fehlen, Anpassung auf Content austehend
  - Präsentation in der Skype-Konferenz über geteilten Bildschirm für alle
    einsehbar
  - Eventuelle Probleme mit möglichen Problemen der Auflösung in Rücksprache
    mit Tobias (Productowner) vorerst ignorierbar und könnten bei Bedarf als
    eigenständiges Ticket bearbeitet werden

**Marc:**

- Ticket: SIP-63
- Thema: PDF in MD
- Status:
  - Bearbeitung des Themas wie Powerpoint-Präsentationen eingebunden werden
    können. Die Idee Powerpointpräsentationen mit einem Skript automatisch
    in PDF umzuwandeln und diese einzubinden, fand keinen Anklang.
  - Fortan bearbeiten des richtigen Tickets.

**Robert:**

- Ticket: SIP-76
- Thema: Einbindung einer Präsentation
- Eingereichter Bericht:

> Die erste Idee wäre ein online-document-viewer. Dort kann man auf einer
> Webseite Dokumente uploaden (zB. von lokal) und dort ansehen. Mit
> Powerpoint Files funktioniert das ganz gut. Es gibt aber leider keinen
> wirklichen Präsentations Modus.
> (<https://onlinedocumentviewer.com/Viewer/default.aspx>)
> Die andere Idee läuft über eine Cloud. Google oder Microsoft bieten einen
> Präsentations Editor + Viewer an. Dazu muss man sich mit einem Account
> registrieren. Dann kann man recht einfach eine Präsi erstellen oder eine
> auf der Cloud liegende Präsentation anschauen.
> Das beantwortet schon mal die ersten 2 Fragen :leichtes_lächeln:
> Frage 4 wird mit den beiden Möglichkeiten überflüssig.. da alles nur über
> Browser läuft.
> Die anderen Fragen, welche mit PDF Export zu tun haben schau ich mir dann
> noch an :leichtes_lächeln:

**Vivien:**

- Ticket: vorherige
- Status:
  - Ticket "Kiosk-Modus verlasssen" ist noch zu verifizieren, Handbuch
    ausstehend
  - Neueres noch nicht bearbeitet

**Martin:**

- Ticket: SIP-54
- Thema: Aktuelle Tests
- Status:
  - Einblick in die bereits verwendeten Tests (app render, content resolver,
    news fetcher, clock)
  - Der Frage: "Wie wird getestet?" wird nachgegangen
- Ziel: Nächste Woche einfache Beispiele präsentieren

**Mathias:**

- Ticket: SIP-65
- Thema: Webscraping des HTWK Telefonverzeichnis
- Als stummer Teilnehmer an der Konferenz, hier der Chatverlauf:

> Ich arbeite an SIP-65, ein Dialogfenster zum Eingeben des entsprechenden
> Professors funktioniert. Ausgabe im Terminal funktioniert. Ausgabe in eine
> JSON Datei funktioniert ebenfalls. Wo ich noch keinen Erfolg erzielt
> habe, ist den IMG Link in die erste Zeile der JSON einzufügen.
> Meine Frage: soll die Ausgabe in einem Messagefenster oder im Terminal
> erfolgen?

- Antwort: Ausgabe als json --> Einigung: json wird nachgereicht

## Retrospektive

**Anmerkungen von Minh:**

- träges Voranschreiten
- **Coding-Sessions zur Pflicht erhoben** (für alle, auch Vivien), durchgeführt
  als Pair-Programming
- Maßnahme soll zur Erhöhung der Motivation und der Produktivität dienen

**Marc:**

- Arbeiten an der Stele verschieben zu "Stopp", da im Moment nicht möglich

**Vivien:**

- Wunsch nach mehr Coding-Tickets

**Anmerkung von Tobias dazu:**

- Abschließen der vorherigen Tickets notwendig um Coding-Tickets erstellen zu
  können
- Vgl. Anm. von Minh

![Visualisierung bereitgestellt von Minh](images/2020-04-13_Retro.png)
